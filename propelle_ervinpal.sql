-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost:3306
-- Létrehozás ideje: 2016. Feb 25. 19:14
-- Szerver verzió: 5.6.26-cll-lve
-- PHP verzió: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `propelle_ervinpal`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=378 ;

--
-- A tábla adatainak kiíratása `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(371, 1456418773, '193.6.194.10', '108745'),
(372, 1456418816, '193.6.194.10', '890612'),
(373, 1456418822, '193.6.194.10', '367049'),
(374, 1456418824, '193.6.194.10', '204378'),
(375, 1456418825, '193.6.194.10', '901374'),
(376, 1456418827, '193.6.194.10', '713092'),
(377, 1456418835, '193.6.194.10', '817563');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('770fdb30f2192db4e8bdce86aad915fca4a4adf1', '::1', 1456415531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431353334363b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('db768243dffb201722bb1d4c7489d8ad7476ed86', '::1', 1456415953, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431353637383b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('0cdddbf49ca1a1920e8b6c6e9f7f9e57e6404e6d', '::1', 1456416256, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431363031383b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('064606557903582507df731eaa8fa242e66ff7a6', '::1', 1456416360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431363335333b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('6b154c4fb5b53c5ca62104212c455d72697c0ced', '::1', 1456416940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431363637353b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('90cebc8076c913a8a371caadb6978ac4986f5747', '::1', 1456417303, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431373033303b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('90ff24bd8102748b133542a78f02e7e746921cfa', '::1', 1456417710, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435363431373334353b64656661756c745f6c616e677c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('a45009e3c63d1cd2116487cd4ecb6fd97838e9ba', '193.6.194.10', 1456418835, 0x4d6f3668684b7179327834576a4d71675a7a744c5f4d6a6b704333717a76636c6e4955336c4b49644356617464434950793846654479734a5a30503634416f78586766515a4f53516b62384f764538326a6a445459772e2e),
('56ec8d7af18f3f3c629d2e2b5b6461c467f9e84b', '84.0.130.43', 1456422817, 0x755f514978706b4772484e2d4c32794e4639447143524a544444645141396d6f51675f6b766c3676664f4e44747664636b6664456d2d5653746e6b70373444696f5763504c634a554a3437306272564b513230766c374c646a476769776c3967774a735554556c61414d363348396374717561646f552d3655656967424738593633706237376e4144726e383549456a566d65534b6c6a42554267754c447775316c5a3131376c38413144694b6a4a343475686f513170616c516c67335f5f374d6e7a6348344336516a7436354861575062426c5851324f656e486231366e6f6a44726a4d78654a53446f2e);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F70E6EB7C54C8C93` (`type_id`),
  KEY `IDX_F70E6EB712469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=56 ;

--
-- A tábla adatainak kiíratása `galleries`
--

INSERT INTO `galleries` (`id`, `type_id`, `create_date`, `category_id`, `status`, `weight`) VALUES
(49, 2, '2016-01-25 00:00:00', NULL, 1, NULL),
(50, 1, '2016-02-24 15:00:41', NULL, 1, NULL),
(51, 1, '2016-02-24 15:49:07', NULL, 1, NULL),
(52, 1, '2016-02-24 15:49:23', NULL, 1, NULL),
(53, 1, '2016-02-25 09:50:03', NULL, 1, NULL),
(54, 1, '2016-02-25 09:50:16', NULL, 1, NULL),
(55, 1, '2016-02-25 17:26:07', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries_tr`
--

CREATE TABLE IF NOT EXISTS `galleries_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B12E9F3182F1BAF4` (`language_id`),
  KEY `IDX_B12E9F314E7AF8F` (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- A tábla adatainak kiíratása `galleries_tr`
--

INSERT INTO `galleries_tr` (`id`, `language_id`, `gallery_id`, `name`, `description`) VALUES
(48, 1, 50, 'TestProject', NULL),
(49, 1, 51, 'TestProject2', NULL),
(50, 1, 52, 'TestProject3', NULL),
(51, 1, 53, 'Awesome Project', NULL),
(52, 1, 54, 'Super Project', NULL),
(53, 1, 55, 'Plus One', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories`
--

CREATE TABLE IF NOT EXISTS `gallery_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- A tábla adatainak kiíratása `gallery_categories`
--

INSERT INTO `gallery_categories` (`id`, `create_date`, `weight`) VALUES
(3, '2016-01-19 15:23:51', 1),
(4, '2016-01-19 15:28:05', 2),
(5, '2016-01-26 17:29:08', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories_tr`
--

CREATE TABLE IF NOT EXISTS `gallery_categories_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_93CCF8882F1BAF4` (`language_id`),
  KEY `IDX_93CCF8812469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `gallery_categories_tr`
--

INSERT INTO `gallery_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 3, 'test2'),
(2, 1, 4, 'élmények'),
(3, 1, 5, 'tánc6'),
(4, 5, 5, 'dance');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_types`
--

CREATE TABLE IF NOT EXISTS `gallery_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BDAD59138CDE5729` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `gallery_types`
--

INSERT INTO `gallery_types` (`id`, `type`) VALUES
(3, 'custom'),
(1, 'project'),
(2, 'slider');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- A tábla adatainak kiíratása `groups`
--

INSERT INTO `groups` (`id`, `group`) VALUES
(1, 'admin'),
(2, 'editor');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `cover` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E01FBE6A4E7AF8F` (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- A tábla adatainak kiíratása `images`
--

INSERT INTO `images` (`id`, `gallery_id`, `filename`, `ext`, `size`, `cover`, `weight`, `create_date`) VALUES
(39, 49, 'icon_gears.png', '.png', 15, 1, 3, '2016-01-25 14:28:09'),
(40, 49, 'kozbeszerzes.png', '.png', 65, 0, 1, '2016-01-25 14:29:19'),
(48, 49, 'elerhetoseg.png', '.png', 14, 0, 2, '2016-01-28 15:15:27'),
(54, 53, 'bor.jpg', '.jpg', 3567, 1, 2, '2016-02-25 16:05:46'),
(55, 53, 'daralo.jpg', '.jpg', 4349, 0, 1, '2016-02-25 16:05:46'),
(56, 53, 'tanc1.jpg', '.jpg', 5602, 0, 3, '2016-02-25 16:05:47'),
(57, 54, 'tanc3.jpg', '.jpg', 4678, 0, 7, '2016-02-25 16:07:43');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images_tr`
--

CREATE TABLE IF NOT EXISTS `images_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DFF7DCA882F1BAF4` (`language_id`),
  KEY `IDX_DFF7DCA83DA5256D` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=67 ;

--
-- A tábla adatainak kiíratása `images_tr`
--

INSERT INTO `images_tr` (`id`, `language_id`, `image_id`, `name`, `description`) VALUES
(42, 1, 39, 'magyar cím', NULL),
(43, 1, 40, NULL, NULL),
(51, 5, 39, 'english title', NULL),
(54, 1, 48, NULL, NULL),
(60, 1, 54, 'Bor', NULL),
(61, 1, 55, 'Daráló', NULL),
(62, 1, 56, 'Tánc', NULL),
(63, 1, 57, NULL, NULL),
(64, 5, 56, 'Dance', NULL),
(65, 5, 55, 'Grinder', NULL),
(66, 5, 54, 'Wine', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- A tábla adatainak kiíratása `languages`
--

INSERT INTO `languages` (`id`, `code`, `language`, `label`) VALUES
(1, 'hu', 'magyar', 'Hungarian'),
(2, 'en', 'English', 'English'),
(3, 'de', 'Deutsch', 'German'),
(4, 'it', 'Italiano', 'Italian'),
(5, 'sp', 'Espanol', 'Spanish'),
(6, 'cz', 'cestina', 'Czech');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata`
--

CREATE TABLE IF NOT EXISTS `metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `metadata`
--

INSERT INTO `metadata` (`id`, `logo`, `favicon`, `author`) VALUES
(1, NULL, NULL, 'Gerőcs Mátyás');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata_tr`
--

CREATE TABLE IF NOT EXISTS `metadata_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) DEFAULT NULL,
  `keywords` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D726301439FCA6F9` (`meta_id`),
  KEY `IDX_D726301482F1BAF4` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `metadata_tr`
--

INSERT INTO `metadata_tr` (`id`, `meta_id`, `keywords`, `description`, `title`, `language_id`) VALUES
(3, 1, 'Pál Ervin, ErvinPál, Pal Ervin, Ervin Pal, pál ervin, ervin pál', NULL, 'Pál Ervin', 1),
(4, 1, NULL, NULL, 'HOPP engines', 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5C93B3A44E7AF8F` (`gallery_id`),
  KEY `IDX_5C93B3A412469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- A tábla adatainak kiíratása `projects`
--

INSERT INTO `projects` (`id`, `category_id`, `gallery_id`, `status`, `weight`, `create_date`, `date`) VALUES
(40, 3, 50, 1, 3, '2016-02-24 15:00:41', '2016-02-24 00:00:00'),
(41, 1, 51, 1, 1, '2016-02-24 15:49:07', '2016-02-20 00:00:00'),
(42, 1, 52, 1, 2, '2016-02-24 15:49:23', '2017-02-18 00:00:00'),
(43, 2, 53, 1, 4, '2016-02-25 09:50:03', '2016-02-25 00:00:00'),
(44, 2, 54, 1, 5, '2016-02-25 09:50:16', '2016-02-25 00:00:00'),
(45, 4, 55, 1, 6, '2016-02-25 17:26:08', '2016-02-25 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects_tr`
--

CREATE TABLE IF NOT EXISTS `projects_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1566D61882F1BAF4` (`language_id`),
  KEY `IDX_1566D618166D1F9C` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;

--
-- A tábla adatainak kiíratása `projects_tr`
--

INSERT INTO `projects_tr` (`id`, `language_id`, `project_id`, `name`, `description`) VALUES
(56, 1, 40, 'TestProject', NULL),
(57, 1, 41, 'TestProject2', NULL),
(58, 1, 42, 'TestProject3', NULL),
(59, 1, 43, 'Awesome Project', 'cxvxcvxcvv'),
(60, 1, 44, 'Super Project', NULL),
(61, 1, 45, 'Plus One', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories`
--

CREATE TABLE IF NOT EXISTS `project_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `project_categories`
--

INSERT INTO `project_categories` (`id`, `create_date`, `weight`) VALUES
(1, '2016-02-25 09:50:28', 2),
(2, '2016-02-25 09:50:38', 1),
(3, '2016-02-25 09:50:46', 3),
(4, '2016-02-25 17:26:36', 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories_tr`
--

CREATE TABLE IF NOT EXISTS `project_categories_tr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7D3B5AFC82F1BAF4` (`language_id`),
  KEY `IDX_7D3B5AFC12469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `project_categories_tr`
--

INSERT INTO `project_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 1, 'design'),
(2, 1, 2, 'movie'),
(3, 1, 3, 'performance'),
(4, 1, 4, 'plus one');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `recovery`
--

CREATE TABLE IF NOT EXISTS `recovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- A tábla adatainak kiíratása `recovery`
--

INSERT INTO `recovery` (`id`, `email`, `ip_address`, `token`, `used`, `create_date`) VALUES
(1, 'mgerocs@gmail.com', '::1', '021e03c6f811156c516a866a57a2c35a', 1, '2016-01-29 10:53:28'),
(2, 'mgerocs@email.hu', '::1', '2bfa28140fece3255fe3f5155c63d8bd', 0, '2016-01-29 11:03:29'),
(3, 'elektromagnes@email.com', '::1', 'f979d592b9672fdc7010d624dbdd8ba8', 0, '2016-01-29 11:05:51'),
(4, 'mgerocs@gmail.com', '::1', '223d30bcfbd4af302403a273dae949d3', 1, '2016-01-29 11:54:55');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `site_languages`
--

CREATE TABLE IF NOT EXISTS `site_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `def` tinyint(1) NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C6DB250CB213FA4` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- A tábla adatainak kiíratása `site_languages`
--

INSERT INTO `site_languages` (`id`, `lang_id`, `def`, `weight`) VALUES
(1, 1, 1, 4),
(5, 2, 0, 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portrait` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`),
  KEY `IDX_1483A5E9FE54D947` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `group_id`, `lastname`, `firstname`, `password`, `email`, `city`, `postal_code`, `address`, `phone1`, `phone2`, `portrait`) VALUES
(1, 1, '', 'Mátyás', '662c03c272ff006f12c9f16fc4992460c57585ee744bb093e0f7279ec7df5c01', 'mgerocs@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `FK_F70E6EB712469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`),
  ADD CONSTRAINT `FK_F70E6EB7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `gallery_types` (`id`);

--
-- Megkötések a táblához `galleries_tr`
--
ALTER TABLE `galleries_tr`
  ADD CONSTRAINT `FK_B12E9F314E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B12E9F3182F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
  ADD CONSTRAINT `FK_93CCF8812469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_93CCF8882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_E01FBE6A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images_tr`
--
ALTER TABLE `images_tr`
  ADD CONSTRAINT `FK_DFF7DCA83DA5256D` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DFF7DCA882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `metadata_tr`
--
ALTER TABLE `metadata_tr`
  ADD CONSTRAINT `FK_D726301439FCA6F9` FOREIGN KEY (`meta_id`) REFERENCES `metadata` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_D726301482F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_5C93B3A412469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`),
  ADD CONSTRAINT `FK_5C93B3A44E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects_tr`
--
ALTER TABLE `projects_tr`
  ADD CONSTRAINT `FK_1566D618166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1566D61882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
  ADD CONSTRAINT `FK_7D3B5AFC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_7D3B5AFC82F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `site_languages`
--
ALTER TABLE `site_languages`
  ADD CONSTRAINT `FK_C6DB250CB213FA4` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`);

--
-- Megkötések a táblához `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_1483A5E9FE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
