-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Hoszt: 127.0.0.1
-- Létrehozás ideje: 2016. Már 05. 19:03
-- Szerver verzió: 5.6.21
-- PHP verzió: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `ervinpal`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
`captcha_id` bigint(13) unsigned NOT NULL,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=398 DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(396, 1457185350, '::1', '286950'),
(397, 1457187399, '::1', '927605');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('78d7adf1075144a5651331e574451bb7a5dbf277', '::1', 1457173966, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137333936363b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('bca326c76bab5b572a0d1d99f16094c504b6bab7', '::1', 1457174306, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137343330363b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('8b24bf9552623863fb432001538f22653c58697a', '::1', 1457176949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137363934393b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('4ab5905966d9f5180e058c8b91cc4f9aa107da35', '::1', 1457177264, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137373236343b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('d3059e003b1ad9a104466e00ee3ae45186854b28', '::1', 1457178424, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137383432343b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('b03b4ff2426fbee6a721477070800efa614fba85', '::1', 1457178889, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137383838393b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('1580f88720e35a77b4d2e06bd1ffc2a560c9cf69', '::1', 1457179198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137393139373b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('5a608229bd1ebe8660c667e0cf1347d38ab5f000', '::1', 1457179520, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137393532303b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('584ea09ed8294b478922a487773d130380ef756c', '::1', 1457179826, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373137393832363b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('1fe20937b8f8cb009467ab468fdeb1fc527beb5f', '::1', 1457180240, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138303234303b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('bf3ae5b729eb17cc6214d0b84e0f1df31e8d61fc', '::1', 1457180552, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138303535323b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('5bb2b0944300a299bb6e2eb0d3ff3eb0b9677433', '::1', 1457180876, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138303837363b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('b63ee22a2ec2239db92eecb325f21aa7c8c2e217', '::1', 1457181267, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138313236373b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('6f2ee97cd9a2757a0238cfa6274ad736fd57d2e5', '::1', 1457181585, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138313538353b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('d34a8208290b806a9c76e5e1c3648bd73d24f9e1', '::1', 1457182158, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138313839333b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('e115246fc32a6d8d8e268a2214a9f31588eac6dd', '::1', 1457182964, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138323936343b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('fed8cc3502a9b6d6520e01be033327dfe5cde2c3', '::1', 1457183321, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138333332313b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('fbaa3f4931b7500ef78ad2fd86c2a9ad4c56f6df', '::1', 1457183670, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138333637303b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('0ec3f96ed82cbfc0613aa00aceaccd4959d9121d', '::1', 1457184007, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138343030373b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('007e29347f1aba11b0c4f8ddc315d9850c09661f', '::1', 1457184567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138343338333b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d),
('46a94d15815fc39858749a6bcc51affda6938ccb', '::1', 1457185361, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138353139303b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('eeff8aaf99b4cdf3dfb45b1a03542223bf61cebc', '::1', 1457185550, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138353535303b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('0b6540483cb140200483355a9e3ca63664f00114', '::1', 1457186110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138353835313b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a353b733a383a226c616e6775616765223b733a373a22456e676c697368223b733a353a226c6162656c223b733a373a22456e676c697368223b733a343a22636f6465223b733a323a22656e223b7d),
('ff4f87c39ccf93055036c8762e4a5bbac8b2d1ab', '::1', 1457186397, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138363232323b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('aad767728dadf8cd502f9858d38fb1b1e1bb5063', '::1', 1457186655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138363631333b736974656c616e677c613a323a7b733a323a226964223b733a313a2231223b733a343a22636f6465223b733a323a226875223b7d69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('5b28fa467ebd3fd832fcbba7567596bb6127c495', '::1', 1457187399, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373138373339393b),
('adef71501a1b73660f8ea1830cdbac22d758e953', '::1', 1457200944, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373230303934343b736974656c616e677c613a323a7b733a323a226964223b693a313b733a343a22636f6465223b733a323a226875223b7d);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
`id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `galleries`
--

INSERT INTO `galleries` (`id`, `type_id`, `create_date`, `category_id`, `status`, `weight`) VALUES
(49, 2, '2016-01-25 00:00:00', NULL, 1, NULL),
(50, 1, '2016-02-24 15:00:41', NULL, 1, NULL),
(51, 1, '2016-02-24 15:49:07', NULL, 1, NULL),
(52, 1, '2016-02-24 15:49:23', NULL, 1, NULL),
(53, 1, '2016-02-25 09:50:03', NULL, 1, NULL),
(54, 1, '2016-02-25 09:50:16', NULL, 1, NULL),
(55, 1, '2016-02-25 17:26:07', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries_tr`
--

CREATE TABLE IF NOT EXISTS `galleries_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `galleries_tr`
--

INSERT INTO `galleries_tr` (`id`, `language_id`, `gallery_id`, `name`, `description`) VALUES
(48, 1, 50, 'TestProject', NULL),
(49, 1, 51, 'TestProject2', NULL),
(50, 1, 52, 'Teszt Project34', NULL),
(51, 1, 53, 'Awesome Project', NULL),
(52, 1, 54, 'Super Project', NULL),
(53, 1, 55, 'Plus One', NULL),
(54, 5, 52, 'English348', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories`
--

CREATE TABLE IF NOT EXISTS `gallery_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_categories`
--

INSERT INTO `gallery_categories` (`id`, `create_date`, `weight`) VALUES
(3, '2016-01-19 15:23:51', 1),
(4, '2016-01-19 15:28:05', 2),
(5, '2016-01-26 17:29:08', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories_tr`
--

CREATE TABLE IF NOT EXISTS `gallery_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_categories_tr`
--

INSERT INTO `gallery_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 3, 'test2'),
(2, 1, 4, 'élmények'),
(3, 1, 5, 'tánc6'),
(4, 5, 5, 'dance');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_types`
--

CREATE TABLE IF NOT EXISTS `gallery_types` (
`id` int(10) unsigned NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_types`
--

INSERT INTO `gallery_types` (`id`, `type`) VALUES
(3, 'custom'),
(1, 'project'),
(2, 'slider');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(10) unsigned NOT NULL,
  `group` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `groups`
--

INSERT INTO `groups` (`id`, `group`) VALUES
(1, 'admin'),
(2, 'editor');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(10) unsigned NOT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `cover` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `images`
--

INSERT INTO `images` (`id`, `gallery_id`, `filename`, `ext`, `size`, `cover`, `weight`, `create_date`) VALUES
(39, 49, 'icon_gears.png', '.png', 15, 1, 3, '2016-01-25 14:28:09'),
(40, 49, 'kozbeszerzes.png', '.png', 65, 0, 1, '2016-01-25 14:29:19'),
(48, 49, 'elerhetoseg.png', '.png', 14, 0, 2, '2016-01-28 15:15:27'),
(54, 53, 'bor.jpg', '.jpg', 3567, 1, 1, '2016-02-25 16:05:46'),
(55, 53, 'daralo.jpg', '.jpg', 4349, 0, 3, '2016-02-25 16:05:46'),
(56, 53, 'tanc1.jpg', '.jpg', 5602, 0, 2, '2016-02-25 16:05:47'),
(57, 54, 'tanc3.jpg', '.jpg', 4678, 0, 7, '2016-02-25 16:07:43');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images_tr`
--

CREATE TABLE IF NOT EXISTS `images_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `images_tr`
--

INSERT INTO `images_tr` (`id`, `language_id`, `image_id`, `name`, `description`) VALUES
(42, 1, 39, 'magyar cím', NULL),
(43, 1, 40, NULL, NULL),
(51, 5, 39, 'english title', NULL),
(54, 1, 48, NULL, NULL),
(60, 1, 54, 'Bor', NULL),
(61, 1, 55, 'Daráló', NULL),
(62, 1, 56, 'Tánc', NULL),
(63, 1, 57, NULL, NULL),
(64, 5, 56, 'Dance', NULL),
(65, 5, 55, 'Grinder', NULL),
(66, 5, 54, 'Wine', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `languages`
--

INSERT INTO `languages` (`id`, `code`, `language`, `label`) VALUES
(1, 'hu', 'magyar', 'Hungarian'),
(2, 'en', 'English', 'English'),
(3, 'de', 'Deutsch', 'German'),
(4, 'it', 'Italiano', 'Italian'),
(5, 'sp', 'Espanol', 'Spanish'),
(6, 'cz', 'cestina', 'Czech');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata`
--

CREATE TABLE IF NOT EXISTS `metadata` (
`id` int(11) NOT NULL,
  `logo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `metadata`
--

INSERT INTO `metadata` (`id`, `logo`, `favicon`, `author`) VALUES
(1, NULL, NULL, 'Gerőcs Mátyás');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata_tr`
--

CREATE TABLE IF NOT EXISTS `metadata_tr` (
`id` int(10) unsigned NOT NULL,
  `meta_id` int(11) DEFAULT NULL,
  `keywords` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `metadata_tr`
--

INSERT INTO `metadata_tr` (`id`, `meta_id`, `keywords`, `description`, `title`, `language_id`) VALUES
(3, 1, 'Pál Ervin, ErvinPál, Pal Ervin, Ervin Pal, pál ervin, ervin pál', NULL, 'Pál Ervin', 1),
(4, 1, 'Ervin Pál, Pál Ervin', NULL, 'Ervin Pál', 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `projects`
--

INSERT INTO `projects` (`id`, `category_id`, `gallery_id`, `status`, `weight`, `create_date`, `date`) VALUES
(40, 3, 50, 1, 3, '2016-02-24 15:00:41', '2016-02-24 00:00:00'),
(41, 1, 51, 1, 1, '2016-02-24 15:49:07', '2016-02-20 00:00:00'),
(42, 1, 52, 1, 2, '2016-02-24 15:49:23', '2017-02-18 00:00:00'),
(43, 2, 53, 1, 4, '2016-02-25 09:50:03', '2016-02-25 00:00:00'),
(44, 2, 54, 1, 5, '2016-02-25 09:50:16', '2016-02-25 00:00:00'),
(45, 4, 55, 1, 6, '2016-02-25 17:26:08', '2016-05-14 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects_tr`
--

CREATE TABLE IF NOT EXISTS `projects_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `projects_tr`
--

INSERT INTO `projects_tr` (`id`, `language_id`, `project_id`, `name`, `description`) VALUES
(56, 1, 40, 'Teszt Project', NULL),
(57, 1, 41, 'Teszt Project2', NULL),
(58, 1, 42, 'Teszt Project34', NULL),
(59, 1, 43, 'Nagyszerű Project', 'cxvxcvxcvv'),
(60, 1, 44, 'Szuper Project', NULL),
(61, 1, 45, 'Árvíztűrő Tükörfúrógép', NULL),
(62, 5, 41, 'English2', NULL),
(63, 5, 42, 'English348', NULL),
(64, 5, 40, 'Eng', NULL),
(65, 5, 43, 'Awesome Project', 'dsfsdfsdf'),
(66, 5, 44, 'Super Project', NULL),
(67, 5, 45, 'Plus One', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories`
--

CREATE TABLE IF NOT EXISTS `project_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `project_categories`
--

INSERT INTO `project_categories` (`id`, `create_date`, `weight`) VALUES
(1, '2016-02-25 09:50:28', 2),
(2, '2016-02-25 09:50:38', 1),
(3, '2016-02-25 09:50:46', 3),
(4, '2016-02-25 17:26:36', 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories_tr`
--

CREATE TABLE IF NOT EXISTS `project_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `project_categories_tr`
--

INSERT INTO `project_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 1, 'design'),
(2, 1, 2, 'movie'),
(3, 1, 3, 'performance'),
(4, 1, 4, 'plus one');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `recovery`
--

CREATE TABLE IF NOT EXISTS `recovery` (
`id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `recovery`
--

INSERT INTO `recovery` (`id`, `email`, `ip_address`, `token`, `used`, `create_date`) VALUES
(1, 'mgerocs@gmail.com', '::1', '021e03c6f811156c516a866a57a2c35a', 1, '2016-01-29 10:53:28'),
(2, 'mgerocs@email.hu', '::1', '2bfa28140fece3255fe3f5155c63d8bd', 0, '2016-01-29 11:03:29'),
(3, 'elektromagnes@email.com', '::1', 'f979d592b9672fdc7010d624dbdd8ba8', 0, '2016-01-29 11:05:51'),
(4, 'mgerocs@gmail.com', '::1', '223d30bcfbd4af302403a273dae949d3', 1, '2016-01-29 11:54:55');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `site_languages`
--

CREATE TABLE IF NOT EXISTS `site_languages` (
`id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `def` tinyint(1) NOT NULL,
  `weight` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `site_languages`
--

INSERT INTO `site_languages` (`id`, `lang_id`, `def`, `weight`) VALUES
(1, 1, 1, 3),
(5, 2, 0, 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portrait` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `group_id`, `lastname`, `firstname`, `password`, `email`, `city`, `postal_code`, `address`, `phone1`, `phone2`, `portrait`) VALUES
(1, 1, '', 'Mátyás', '662c03c272ff006f12c9f16fc4992460c57585ee744bb093e0f7279ec7df5c01', 'mgerocs@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
 ADD PRIMARY KEY (`captcha_id`), ADD KEY `word` (`word`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_F70E6EB7C54C8C93` (`type_id`), ADD KEY `IDX_F70E6EB712469DE2` (`category_id`);

--
-- Indexes for table `galleries_tr`
--
ALTER TABLE `galleries_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B12E9F3182F1BAF4` (`language_id`), ADD KEY `IDX_B12E9F314E7AF8F` (`gallery_id`);

--
-- Indexes for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_93CCF8882F1BAF4` (`language_id`), ADD KEY `IDX_93CCF8812469DE2` (`category_id`);

--
-- Indexes for table `gallery_types`
--
ALTER TABLE `gallery_types`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_BDAD59138CDE5729` (`type`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_E01FBE6A4E7AF8F` (`gallery_id`);

--
-- Indexes for table `images_tr`
--
ALTER TABLE `images_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_DFF7DCA882F1BAF4` (`language_id`), ADD KEY `IDX_DFF7DCA83DA5256D` (`image_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata`
--
ALTER TABLE `metadata`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata_tr`
--
ALTER TABLE `metadata_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D726301439FCA6F9` (`meta_id`), ADD KEY `IDX_D726301482F1BAF4` (`language_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_5C93B3A44E7AF8F` (`gallery_id`), ADD KEY `IDX_5C93B3A412469DE2` (`category_id`);

--
-- Indexes for table `projects_tr`
--
ALTER TABLE `projects_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_1566D61882F1BAF4` (`language_id`), ADD KEY `IDX_1566D618166D1F9C` (`project_id`);

--
-- Indexes for table `project_categories`
--
ALTER TABLE `project_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7D3B5AFC82F1BAF4` (`language_id`), ADD KEY `IDX_7D3B5AFC12469DE2` (`category_id`);

--
-- Indexes for table `recovery`
--
ALTER TABLE `recovery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_languages`
--
ALTER TABLE `site_languages`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_C6DB250CB213FA4` (`lang_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`), ADD KEY `IDX_1483A5E9FE54D947` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
MODIFY `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=398;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `galleries_tr`
--
ALTER TABLE `galleries_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallery_types`
--
ALTER TABLE `gallery_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `images_tr`
--
ALTER TABLE `images_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `metadata`
--
ALTER TABLE `metadata`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `metadata_tr`
--
ALTER TABLE `metadata_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `projects_tr`
--
ALTER TABLE `projects_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `project_categories`
--
ALTER TABLE `project_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `recovery`
--
ALTER TABLE `recovery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_languages`
--
ALTER TABLE `site_languages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `galleries`
--
ALTER TABLE `galleries`
ADD CONSTRAINT `FK_F70E6EB712469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`),
ADD CONSTRAINT `FK_F70E6EB7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `gallery_types` (`id`);

--
-- Megkötések a táblához `galleries_tr`
--
ALTER TABLE `galleries_tr`
ADD CONSTRAINT `FK_B12E9F314E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_B12E9F3182F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
ADD CONSTRAINT `FK_93CCF8812469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_93CCF8882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images`
--
ALTER TABLE `images`
ADD CONSTRAINT `FK_E01FBE6A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images_tr`
--
ALTER TABLE `images_tr`
ADD CONSTRAINT `FK_DFF7DCA83DA5256D` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_DFF7DCA882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `metadata_tr`
--
ALTER TABLE `metadata_tr`
ADD CONSTRAINT `FK_D726301439FCA6F9` FOREIGN KEY (`meta_id`) REFERENCES `metadata` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_D726301482F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects`
--
ALTER TABLE `projects`
ADD CONSTRAINT `FK_5C93B3A412469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`),
ADD CONSTRAINT `FK_5C93B3A44E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects_tr`
--
ALTER TABLE `projects_tr`
ADD CONSTRAINT `FK_1566D618166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_1566D61882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
ADD CONSTRAINT `FK_7D3B5AFC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_7D3B5AFC82F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `site_languages`
--
ALTER TABLE `site_languages`
ADD CONSTRAINT `FK_C6DB250CB213FA4` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`);

--
-- Megkötések a táblához `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `FK_1483A5E9FE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
