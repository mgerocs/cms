//DATATABLES DATE FILTER EXTENSION
var minDateFilter = "";
var maxDateFilter = "";
var datecolumn = "";
$.fn.dataTableExt.afnFiltering.push(
        function (oSettings, aData, iDataIndex) {
            if (typeof aData._date === 'undefined') {
                aData._date = new Date(aData[datecolumn]).getTime();
            }

            if (minDateFilter && !isNaN(minDateFilter)) {
                if (aData._date < minDateFilter) {
                    return false;
                }
            }

            if (maxDateFilter && !isNaN(maxDateFilter)) {
                if (aData._date > maxDateFilter) {
                    return false;
                }
            }

            return true;
        }
);

var manager;
var interactor;
var validator;
var tabulator;
//var oLanguage = {
//    oPaginate: {
//        sPrevious: "Előző",
//        sNext: "Következő"
//    },
//    sEmptyTable: "Nincs elérhető adat.",
//    sZeroRecords: "Nincs találat.",
//    sProcessing: "Folyamatban...",
//    sInfo: "Rekordok _START_ - _END_-ig / Összesen: _TOTAL_",
//    sInfoFiltered: "(szűrve _MAX_ rekordból)",
//    sLengthMenu: "_MENU_ rekord megjelenítése"
//};
var validator_tooltip_options = {
    trigger: 'custom', // default is 'hover' which is no good here
    onlyOne: false, // allow multiple tips to be open at a time
    position: 'top',
    theme: 'tooltipster-light'
};

default_defs = [
    {
        targets: "hidden",
        visible: false
    },
    {
        targets: "no-order",
        orderable: false
    },
    {
        targets: "op",
        className: "op"
    },
    {
        targets: 'handle',
        className: 'handle',
        render: function (data, type, full, meta) {
            return '<i class="fa fa-bars"></i>';
        }
    },
    {
        targets: 'coverImg',
        className: 'coverImg',
        render: function (data, type, full, meta) {
            if (full['filename'] !== null) {
                return '<img src="' + base_url + "../images/" + full['type'] + "/thumbs/" + full['filename'] + '" class="img img-responsive img-thumbnail coverimg"/>';
            } else {
                return "";
            }
        }
    },
    {
        targets: 'ymd',
        className: 'ymd',
        render: function (data, type, full, meta) {
            if (data !== null) {
                return data.date.substring(0, data.date.indexOf(' '));
            } else {
                return "";
            }
        }
    },
    {
        targets: 'ymdhis',
        className: 'ymdhis',
        render: function (data, type, full, meta) {
            return data.date;
        }
    },
    {
        targets: "edit",
        render: function (data, type, full, meta) {
            return '<a href="' + base_url + manager.routes.items + data + '" class="btn btn-sm btn-default edit_item" title="edit"><i class="fa fa-info"></i></a>';
        }
    },
    {
        targets: "setStatus",
        render: function (data, type, full, meta) {
            var cls1 = full['status'] == 1 ? "btn-success" : "btn-default";
            var cls2 = full['status'] == 1 ? "fa-check" : "fa-times";
            var status = full['status'] == 1 ? 0 : 1;
            return '<a href="' + base_url + manager.routes.status + data + '/' + status + '" class="btn btn-sm ' + cls1 + ' status" title="status"><i class="fa ' + cls2 + '"></i></a>';
        }
    },
    {
        targets: "delete",
        render: function (data, type, full, meta) {
            return '<a href="' + base_url + manager.routes.remove + data + '" class="btn btn-sm btn-danger delete_item" title="delete"><i class="fa fa-trash-o"></i></a>';
        }
    },
    {
        targets: "gallery",
        render: function (data, type, full, meta) {
            return '<a href="' + base_url + manager.routes.images + data + '" class="btn btn-sm btn-default images" title="images"><i class="fa fa-file-image-o"></i></a>';
        }
    }
];
validator_options = {
    ignore: ':hidden, [readonly=readonly]',
    onkeyup: function (element, event) {
        var $element = $(element);
        if (!$element.closest(".form-group").hasClass("has-error")) {
            return false;
        }
        $element.valid();
    },
    onfocusout: false,
    onclick: function (element) {
        var $element = $(element);
        $element.change(function () {
            $element.valid();
        });
    },
    rules: {
        ch_password: {pwdcheck: true},
        ch_password_conf: {equalTo: "#ch_password_"},
        new_password: {pwdcheck: true},
        new_password_conf: {equalTo: "#new_password_"},
        start: {year: true},
        end: {year: true},
        start_date: {fulldate: true},
        end_date: {fulldate: true},
        start_time: {time: true},
        end_time: {time: true}
    },
    errorPlacement: function (error, element) {
        var $element = $(element);
        var lastError = $element.data('lastError');
        var newError = $(error).text();
        $element.data('lastError', newError);
        if (newError !== '' && newError !== lastError) {
            $element.tooltipster('content', newError);
            $element.data('lastError', '');
            $element.tooltipster('show');
        }
    },
    success: function (label, element) {
        var $element = $(element);
        var $form = $element.closest("form");
        if (!$form.hasClass("showSuccess")) {
            return false;
        }
        $element.closest('.form-group').addClass('has-success');
        $element.closest('.form-group').find('.form-control-feedback').addClass('glyphicon-ok');
    },
    highlight: function (element) {
        var $element = $(element);
        $element.closest('.form-group').addClass('has-error');
        $element.closest('.form-group').find('.form-control-feedback').addClass('glyphicon-remove');
    },
    unhighlight: function (element) {
        var $element = $(element);
        $element.tooltipster('hide');
        $element.closest('.form-group').removeClass('has-error');
        $element.closest('.form-group').find('.form-control-feedback').removeClass('glyphicon-remove');
    },
    submitHandler: function (form) {
        //this is needed to move the content of the editor to the actual textarea input
        for (var instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var $form = $(form);
        var url = $form.attr('action');
        var data = new FormData($form[0]);
        performAjaxRequest(url, data, default_callback, true);
    }
};

/*******************************************************************************
 DEFAULT CALLBACK                        
 *******************************************************************************/
var default_callback = function (response) {
    if (response.status === true) {

        if ($("#dialog").is(':ui-dialog')) {
            $("#dialog").not(".no-close").html("").dialog('close');
        }
        if (response.redirect !== undefined) {
            window.location.href = response.redirect;
        }
        if (response.message !== undefined && response.context !== undefined) {
            interactor.dialog({
                type: 'message',
                message: response.message,
                context: response.context,
                title: ""});
        }
        if (response.partialUrl !== undefined && response.parent !== undefined) {
            var callback = function () {
                initLayout();
            };
            getPartial(response.partialUrl, response.parent, callback);
        }
        if (response.updateUI !== undefined) {
            $.each(response.updateUI, function (key, value) {
                $(key).html(value);
            });
        }

        if ($(".datatable").length > 0 && response.dtReload === undefined) {
            tabulator.reload();
        }

        $(".todisable").prop('disabled', false).removeClass("disabled");

    }

    if (response.status === false && response.error !== undefined) {
        if (response.recaptcha !== undefined) {
            $("#reload_captcha").trigger('click');
        }
        if (response.element !== undefined) {
            if ($(response.element).hasClass('tooltip-right')) {
                $(response.element).tooltipster('option', 'position', 'right');
            }
            $(response.element).tooltipster('content', response.error);
            $(response.element).closest('div.form-group').addClass('has-error');
            $(response.element).closest('div.form-group').find('span.form-control-feedback').addClass('glyphicon-remove');
            $(response.element).focus().tooltipster('show');
        } else {
            interactor.dialog({
                type: 'message',
                message: response.error,
                context: "error",
                title: ""});
        }
    }

};
var Interactor = function () {

    this.$element;
    this.defaultOptions = {
        autoOpen: false,
        draggable: false,
        resizable: false,
        modal: true,
        height: 'auto',
        width: 'auto',
        fluid: true,
        create: function (e, ui) {

        },
        open: function (e, ui) {
            $('.ui-dialog-buttonset').children('button').
                    removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus ui-corner-all").
                    mouseover(function () {
                        $(this).removeClass('ui-state-hover');
                    }).
                    mousedown(function () {
                        $(this).removeClass('ui-state-active');
                    }).
                    focus(function () {
                        $(this).removeClass('ui-state-focus');
                    });
            var $target = $(e.target);
            if ($target.hasClass("no-titlebar")) {
                $target.closest(".ui-dialog").find(".ui-dialog-titlebar").hide();
            }
            $target.find("button").blur();
            $target.find("input:enabled").first().focus();
        },
        beforeClose: function (e, ui) {
            hideValidationFeedback($(e.target).find("form").first());
        },
        close: function () {
            $(this).dialog('destroy').hide();
        }
    };
    this.dialog = function (options) {

        var self = this;
        switch (options.type) {
            case 'message':
                self.$element = $("#dialog");
                var icon = "";
                var content = "";
                switch (options.context) {
                    case "warning":
                        icon = '<i class="fa fa-warning warning"></i>';
                        break;
                    case "success":
                        icon = '<i class="fa fa-check success"></i>';
                        break;
                    case "error":
                        icon = '<i class="fa fa-times-circle error"></i>';
                        break;
                }

                content += '<div class="dialog-icon text-center">' + icon + '</div>';
                content += '<div class="message text-center">' + options.message + '</div>';
                content += '<div class="button_panel text-center"><button class="btn btn-default close-dialog">Ok</button></div>';
                self.$element.addClass('no-titlebar');
                self.$element.html(content);
                self.$element.dialog(self.defaultOptions);
                self.$element.dialog('open');
                self.$element.unbind('dialogopen');
                break;
            case 'form':
                self.$element = $("#dialog");
                self.$element.attr('title', options.title);
                if (options.cls) {
                    self.$element.addClass(options.cls);
                }
                var callback = function () {
                    var $form = self.$element.find('form').first();
                    var $fields = $form.find("input, textarea").not(':hidden');
                    $.each($fields, function (index, field) {
                        var $field = $(this);
                        if ($field.prop("type") !== "checkbox" && $field.prop('required') !== true && !$field.hasClass("no-disable")) {
                            $field.prev('.control-label').addClass('dim');
                            $field.prop('disabled', true);
                        }
                    });
                    $form.find('input, select, textarea').tooltipster(validator_tooltip_options);
                    $form.validate(validator_options);
                    if (options.populateUrl) {
                        $form.find("#op").val("mod");
                        $form.find("#id").val(options.id);
                        populateForm(options.populateUrl, $form);
                    } else {
                        $form.find("#op").val("new");
                    }

                    if (self.$element.find(".editor").length > 0) {
                        self.$element.find(".editor").ckeditor();
                    }
                    if (self.$element.find(".datepicker").length > 0) {
                        initDatePicker();
                    }
                    if (self.$element.find(".sortable").length > 0) {
                        initSortable();
                    }
                    if (self.$element.find(".autocomplete").length > 0) {
                        initAutoComplete();
                    }

                    self.$element.removeClass('no-titlebar');
                    self.$element.dialog(self.defaultOptions);
                    self.$element.dialog('open');
                    self.$element.unbind('dialogopen');
                    self.$element.find("form").css("visibility", "visible");
                    self.$element.find("input:enabled").first().focus();
                };
                getPartial(options.partialUrl, self.$element, callback);
                break;
            case 'confirm':
                self.$element = $("#confirm");
                var content = "";
                content += '<div class="dialog-icon text-center warning"><i class="fa fa-warning"></i></div>';
                content += '<div class="message text-center">' + options.message + '</div>';
                self.$element.addClass('no-titlebar');
                self.$element.html(content);
                self.$element.dialog(self.defaultOptions);
                self.$element.dialog("option", "buttons",
                        [
                            {
                                text: "Nem",
                                class: 'btn btn-default',
                                tabindex: -1,
                                click: function () {
                                    $(this).dialog("close");
                                    return false;
                                }
                            },
                            {
                                text: "Igen",
                                class: 'btn btn-warning',
                                tabindex: -1,
                                click: function () {
                                    if (options.callback && typeof (options.callback === "function")) {
                                        options.callback();
                                    }
                                    $(this).dialog("close");
                                }
                            }
                        ]
                        );
                self.$element.dialog('option', 'width', 'auto');
                self.$element.dialog('open');
                self.$element.unbind('dialogopen');
                break;
        }

    };
};
var Tabulator = function (source) {

    this.$element;
    this.instance;
    this.init = function (options) {
        var self = this;
        self.$element = $("#datatable");
        self.instance = self.$element.DataTable(options);
        self.$element.find("thead .filters input").on('keyup change', function () {
            self.instance
                    .column($(this).parent('td').index() + ':visible')
                    .search(this.value)
                    .draw();
        });
        self.$element.find("thead .filters select").on('change', function () {
            self.instance
                    .column($(this).parent('td').index() + ':visible')
                    .search(this.value)
                    .draw();
        });

        $("#from").datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            "onSelect": function (date) {
                minDateFilter = new Date(date).getTime();
                self.instance.draw();
            }
        }).keyup(function () {
            minDateFilter = new Date(this.value).getTime();
            self.instance.draw();
        });
        $("#to").datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            "onSelect": function (date) {
                maxDateFilter = new Date(date).getTime();
                self.instance.draw();
            }
        }).keyup(function () {
            maxDateFilter = new Date(this.value).getTime();
            self.instance.draw();
        });

        self.instance.on('row-reorder', function (e, diff, edit) {

            var ids = [];
            var weights = [];
            for (var i = 0, ien = diff.length; i < ien; i++) {
                ids.push(self.instance.row(diff[i].node).data().id);
                weights.push(self.instance.row(diff[i].node).data().weight);
            }

            if (ids.length < 1 || weights.length < 1) {
                return false;
            }

            var sorted_weights = weights.sort(function (a, b) {
                return a - b;
            });
            var callback = function () {
                self.instance.ajax.reload(null, false);
            };
            performAjaxRequest(base_url + manager.routes.dtReorder, {ids: ids, weights: sorted_weights}, callback);
        });
    };
    this.reload = function () {
        var self = this;
        var callback = function () {
            self.instance.search("").columns().search("").draw();
            self.createSelectFilter();
        };
        self.instance.ajax.reload(callback, false);
    };
    this.createSelectFilter = function () {
        var self = this;
        self.$element.find("thead .filters select").each(function (key, elem) {
            var $select = $(this);
            var column = self.instance.column($(this).parent('td').index() + ':visible');
            var options = "";
            options += "<option></option>";
            column.data().unique().sort().each(function (category, j) {
                var option = category !== null ? category : "";
                if (option !== "") {
                    options += '<option>' + option + '</option>';
                }
            });
            $select.html(options);
        });
    };
    this.destroy = function () {
        var self = this;
        self.instance.destroy();
    };
};
var Manager = function () {

    this.routes = {};
    this.defaultLang = 1;
    this.newItem = function () {
        var self = this;
        interactor.dialog({
            type: 'form',
            partialUrl: base_url + self.routes.partials.entityForm,
            title: self.routes.dialogTitle
        });
    };
    this.editItem = function (url) {
        var self = this;
        interactor.dialog({
            type: 'form',
            partialUrl: base_url + self.routes.partials.entityForm + "1",
            title: self.routes.dialogTitle,
            populateUrl: url
        });
    };
    this.deleteItem = function (url) {
        var callback = function () {
            performAjaxRequest(url, null, default_callback);
        };
        interactor.dialog({
            type: 'confirm',
            message: "Are you sure, you want to delete this item?",
            callback: callback
        });
    };
    this.setStatus = function (url) {
        performAjaxRequest(url, null, default_callback);
    };
    this.reorder = function (ids) {
        var weights = [];
        for (var i = 0; i < ids.length; i++) {
            weights.push(i + 1);
        }
        performAjaxRequest(base_url + manager.routes.reorder, {ids: ids, weights: weights});
    };
    this.categories = function () {
        var self = this;
        interactor.dialog({
            type: 'form',
            partialUrl: base_url + self.routes.partials.categoryForm + "1",
            title: self.routes.entity + " categories"
        });
    };
};

$(function () {
    $.validator.addMethod("pwdcheck", function (value, element) {
        return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/i.test(value);
    }, "Minimum 8 characters long, must contain small and capital letters and numbers.");
    $.validator.addMethod("year", function (value, element) {
        return this.optional(element) || /^[12][0-9][0-9][0-9]$/i.test(value);
    }, "Invalid year format.");
    $.validator.addMethod("fulldate", function (value, element) {
        return this.optional(element) || /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/.test(value);
    }, "Invalid date format.");
    $.validator.addMethod("time", function (value, element) {
        return this.optional(element) || /^(0[1-9]|1[0-9]|2[0-4]):(0[0-9]|[1-5][0-9])$/.test(value);
    }, "Invalid time format.");
    $('form.form').each(function () {
        validator = $(this).validate(validator_options);
    });
});

function initLayout(options) {

    if ($(".form").length > 0) {
        $("form.form").each(function () {
            $(this).validate(validator_options);
        });
        $('form.form :input').tooltipster(validator_tooltip_options);
    }

    if ($(".datepicker").length > 0) {
        initDatePicker();
    }

    if ($(".datatable").length > 0 && options) {
        tabulator.init(options);
    }

    if ($(".nestable").length > 0) {
        $(".nestable").nestable({
            maxDepth: 3
        });
        $('#main-content').off('change', '.nestable');
        $('#main-content').on('change', '.nestable', function () {
            var $element = $(this);
            var tree = $element.nestable('serialize');

            performAjaxRequest(base_url + "menu/updateMenu", {tree: tree}, default_callback);

        });
    }

    $("form input:visible:enabled").first().focus();

}

/*******************************/
/*****  DOCUMENT READY  *****   /
 /*******************************/

$(document).ready(function () {

    $(document).on('focusin', function (e) {
        e.stopImmediatePropagation();
    });

    Dropzone.autoDiscover = false;
    manager = new Manager();
    interactor = new Interactor();
    tabulator = new Tabulator();

    //options of initial page
    var options = projects();
    initLayout(options);

    //NAVIGATOR
    $("nav, .navbar").on('click', '.menuitem:not(.logout)', function (e) {
        e.preventDefault();
        var $menuitem = $(this);

        $(".menuitem").removeClass("active");
        $menuitem.addClass("active");

        if ($(".navbar-toggle").is(":visible")) {
            setTimeout(function () {
                $(".navbar-toggle").trigger("click");
            }, 100);
        }

        if (!$menuitem.hasClass("ajax")) {
            return false;
        }

        var url = $menuitem.attr('href');
        var action = $menuitem.data('action');
        var options;
        var callback = function () {
            initLayout(options);
        };
        switch (action) {
            case "events":
                options = events();
                break;
            case "projects":
                options = projects();
                break;
            case "galleries":
                options = galleries();
                break;
            case "biography":
                options = biography();
                break;
            case "menu":
                options = menu();
                break;
            case "users":
                options = users();
                break;
            case "settings":
                options = settings();
                break;
            default:

                break;
        }

        getPartial(url, "#main-content", callback);
    });

    //NEW ITEM
    $("#main-content").on('click', '.new_item', function () {
        manager.newItem();
    });
    //EDIT ITEM
    $("#main-content").on('click', '.edit_item', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        manager.editItem(url);
    });
    //DELETE ITEM
    $("body").on('click', '.delete_item', function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        manager.deleteItem(url);
    });
    //SET ITEM STATUS
    $("#main-content").on('click', '.status', function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        manager.setStatus(url);
    });
    //EDIT CATEGORIES
    $("#main-content").on('click', '#edit_cat', function () {
        manager.categories();
    });
    //CATEGORY CONTROLS
    $("body").on('click', '.category_control', function () {
        var $element = $(this);
        var $form = $element.closest("form");
        var op = $element.data('op');
        var controller = $form.find("#controller").val();
        var category_id;
        var category;
        if (op === "update") {
            category_id = $element.data('id');
            category = $element.data('category');
        } else if (op === "delete") {
            category_id = $element.data('id');
            alert("delete");
            return true;
        } else {
            category_id = "";
            category = "";
        }
        $form.find("#category_id").val(category_id);
        $form.find("#op").val(op);
        $form.find(".toggle_form_group").show().find("#category").focus().val(category);
        $form.find("button.todisable").prop("disabled", true);
        $form.find("a.todisable").addClass("disabled");
    });
    //CANCEL ADD/UPDATE CATEGORY
    $("body").on('click', '#cancel_add_category', function () {
        var $form_group = $(this).closest("form").find(".toggle_form_group");
        hideValidationFeedback($form_group);
        $form_group.find("input").val("");
        $form_group.hide();
        $(this).closest("form").find(".todisable").prop("disabled", false).removeClass("disabled");
    });
    //REMOVE CATEGORY
    $("ul#category_list").on('click', 'button.delete_category', function (e) {
        e.preventDefault();
        var url = base_url + $(this).val();
        manager.removeItem(url, false);
    });
    //SWITCH LANGUAGE
    $("body").on('click', '.lang_control', function () {
        var $element = $(this);
        var $form = $element.closest("form");
        if ($element.hasClass("actual")) {
            return false;
        }

        hideValidationFeedback($form);

        $(".lang_control").removeClass('actual btn-warning').addClass('btn-default');
        $element.removeClass('btn-default').addClass('actual btn-warning');
        var id = $form.find("#id").val();
        var lang_id = $element.data('lang');
        var callback;
        var partialUrl;
        var parent;
        if ($form.attr('id') === "category_form") {
            callback = function () {
                $form.find("#lang_id").val(lang_id);
            };
            partialUrl = base_url + manager.routes.partials.categoryList + lang_id;
            parent = "#category_list";
            getPartial(partialUrl, parent, callback);
        } else if ($form.attr('id') === "image_form") {
            var gallery_id = $form.find("#gallery_id").val();
            $form.find("#lang_id").val(lang_id);
            var populateUrl = base_url + manager.routes.items + gallery_id + "/" + id + "/" + lang_id;
            populateForm(populateUrl, $form);
        } else if ($form.attr('id') === "meta_form") {
            $form.find("#lang_id").val(lang_id);
            var populateUrl = base_url + manager.routes.metadata + lang_id;
            populateForm(populateUrl, $form);
        } else {
            callback = function () {
                $form.find("#lang_id").val(lang_id);
                var populateUrl = base_url + manager.routes.items + id + "/" + lang_id;
                populateForm(populateUrl, $form);
            };
            if ($form.find("#category_id").length > 0) {
                partialUrl = base_url + manager.routes.partials.categoryOptions + lang_id;
                parent = "#category_id";
                getPartial(partialUrl, parent, callback);
            } else {
                callback();
            }

        }

    });
    //SHOW IMAGES
    $("#main-content, nav, .navbar").on("click", ".images", function (e) {
        e.preventDefault();
        manager.routes = {
            controller: 'galleries/',
            items: 'galleries/getImages/',
            remove: 'galleries/deleteImage/',
            reorder: 'galleries/imageReorder/',
            status: 'galleries/setCover/',
            partials: {
                entityForm: 'galleries/imageDialog/'
            },
            entity: "Image",
            dialogTitle: "Image properties"
        };
        var callback = function () {
            initDropZone();
            initSortable();
        };
        var partialUrl = $(this).attr('href');
        var parent = $("#main-content");
        getPartial(partialUrl, parent, callback);
    });
    //SET INDEX IMAGE
    $("div#main-content").on('click', '.cover', function () {
        var $element = $(this);
        if ($element.closest("li").hasClass('index')) {
            return false;
        }
        $(".cover").not($element).prop("checked", false);
        var gallery_id = $element.data("gallery_id");
        var image_id = $element.data("image_id");
        var url = base_url + manager.routes.status + gallery_id + "/" + image_id;
        $('#image_array li').removeClass('index');
        $element.closest('li').addClass('index');
        performAjaxRequest(url, null, null);
    });
    //CHANGE PASSWORD
    $("#main-content").on('click', '#change_pwd', function () {
        interactor.dialog({
            type: 'form',
            partialUrl: base_url + manager.routes.partials.passwordForm,
            title: manager.routes.passwordDialogTitle
        });
    });
    //EDIT META DATA
    $("#main-content").on('click', '#edit_meta', function () {
        interactor.dialog({
            type: 'form',
            partialUrl: base_url + manager.routes.partials.metaForm,
            title: manager.routes.metaDialogTitle,
            populateUrl: manager.routes.metadata
        });
    });
    //CLOSE DIALOG WINDOW
    $("body").on("click", ".close-dialog", function () {
        hideValidationFeedback($(this).closest("#dialog"));
        $("input.tooltipstered").data('lastError', '');
        $(this).closest("#dialog").html("").dialog('close');
    });
    //TOGGLE INPUT FIELD
    $("body").on("click", ".toggle_field", function (e) {
        e.preventDefault();
        var $element = $(this);
        var form_group = $element.closest('.group').find('.form-group').not('.no-toggle');
        if ($element.hasClass('enabled')) {
            $element.removeClass('enabled').addClass('disabled');
            $element.find('.fa').replaceWith('<i class="fa fa-lock"></i>');
            form_group.find('.control-label').addClass('dim');
            form_group.find(':input').val("").prop('required', false).prop('disabled', true);
            hideValidationFeedback($element.closest('.group'));
        } else {
            $element.removeClass('disabled').addClass('enabled');
            $element.find('.fa').replaceWith('<i class="fa fa-unlock-alt"></i>');
            form_group.find('.control-label').removeClass('dim');
            form_group.find('input, select, textarea').not(".no-toggle").prop('required', true).prop('disabled', false).first().focus();
        }
    });
    //GET NEW CAPTCHA
    $('#reload_captcha').click(function () {
        var url = base_url + 'identity/createCaptcha';
        var callback = function (response) {
            if (response.status === true) {
                $("#captcha_img").html(response.captcha);
            }
        };
        performAjaxRequest(url, null, callback);
    });
    //CUSTOM UPLOAD
    $("#main-content").on('click', '.fake_upload_trigger', function () {
        $(this).closest("form").find("input[type=file]").trigger("click");
    });
    $("#main-content").on('change', '.custom-upload input[type=file]', function () {
        $(this).next().find('input').val($(this).val());
    });
    $("#main-content").on('click', '.delete_uploaded', function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var callback = function () {
            var $control = $(this).closest("form").find("input[type=file]");
            $control.val("");
            $control.next().find("input").val("");
            performAjaxRequest(url, null, default_callback);
        };
        interactor.dialog({
            type: 'confirm',
            message: "Are you sure, you want to delete this item?",
            callback: callback
        });
    });
    //toggle external link
    $("body").on('change', "#external", function () {
        var $element = $(this);
        var val = $element.val();
        if (val == 0) {
            $element.val(1);
            $element.closest('.group').find('.form-group > label').text("URL");
            $element.closest('.group').find('.form-group input').prop('type', 'url').attr('placeholder', 'URL');
            $element.closest('.group').find('#url_prefix').text("");
        } else {
            $element.val(0);
            $element.closest('.group').find('.form-group > label').text("Path");
            $element.closest('.group').find('.form-group input').prop('type', 'text').attr('placeholder', 'path');
            $element.closest('.group').find('#url_prefix').text(base_url);
        }
    });
    //save menu tree
    $("#main-content").on('click', '.save_tree', function () {
        var $element = $(this);
        var $nestable = $element.prev('.nestable');
        var tree = $nestable.nestable('serialize');

//        var callback = function (response) {
//            $("#menu_tree").html(response);
//        };

        performAjaxRequest(base_url + "menu/updateMenu", {tree: tree}, default_callback);

    });
});
//GET PARTIAL
function getPartial(url, parent, callback) {
    var $parent = $(parent);
    var $loader = $("#loader");
    if ($parent.length < 1) {
        return false;
    }

    $loader.show();
    $.get(url, function (partial) {

        $parent.html(partial);
        $loader.hide();
        if (callback && typeof (callback) === 'function') {
            callback();
        }

    });
}


//PERFORM AJAX REQUEST
function performAjaxRequest(url, data, callback, _formdata) {
    var formdata = (_formdata || false);
    if (formdata) {
        var request = $.ajax({
            type: "post",
            async: true,
            url: url,
            cache: false,
            data: data,
            dataType: "json",
            beforeSend: function () {
                $('#loader').show();
            },
            contentType: false,
            processData: false
        });
    } else {
        var request = $.ajax({
            type: "post",
            async: false,
            url: url,
            cache: false,
            data: data,
            dataType: "json",
            beforeSend: function () {
                $('#loader').show();
            }
        });
    }
    request.done(function (response) {
        $('#loader').hide();
        if (callback && typeof (callback) === 'function') {
            callback(response);
        }
    });
    request.fail(function (jqXHR, textStatus) {
        alert(jqXHR.status + " " + textStatus);
    });
}

//function showItems(url, parent) {
//
//    var request = $.ajax({
//        type: "post",
//        async: false,
//        url: url,
//        cache: false,
//        beforeSend: function () {
//            $('#loader').show();
//        }
//    });
//    request.done(function (response) {
//        $('#loader').hide();
//        //alert(response);
//
//        if (isJson(response)) {
//            updateDataTables(url);
//        } else {
//            $(parent).html(response);
//        }
//
////        $(".container").customScrollbar({
////            skin: "default-skin",
////            hScroll: false,
////            updateOnWindowResize: true
////        });
//
////        if ($("#image_array").length > 0) {
////            initImageSortable();
////            initTrashSortable();
////        }
//
//        var form_group = $(".toggle_form_group");
//        hideValidationFeedback(form_group);
//        form_group.find("input").val("");
//        form_group.hide();
//    });
//    request.fail(function (jqXHR, textStatus) {
//        alert(jqXHR.status + " " + textStatus);
//    });
//}

function populateForm(url, $form) {

    var callback = function (response) {

        if (response.status !== true) {
            interactor.dialog({type: 'message', context: 'error', message: "Hiba történt"});
            return false;
        }

        $form.find("input").first().focus();
        var data = response.data[0];
        $.each(data, function (key, value) {

            if ($.isPlainObject(value) && value.hasOwnProperty('date')) {
                value = value.date.substring(0, value.date.indexOf(' '));
            }

            var $field = $form.find(":input[name='" + key + "']");

            switch ($field.prop("type")) {
                case "select-one":
                    if (value === "" || value === null) {
                        $field.find("option").first().prop('selected', true);
                    } else {
                        $field.find("option[value='" + value + "']").prop('selected', true);
                    }
                    break;
                case "checkbox":
                    if (value == 1) {
                        $field.prop("checked", true);
                    } else {
                        $field.prop("checked", false);
                    }
                    break;
                case "radio":
                    $field.filter(function () {
                        return $(this).val() == value;
                    }).prop("checked", true);
                    break;
                case "textarea":
                    if (value === "" || value === null) {
                        $field.val("");
                        $field.text("");
                    } else {
                        $field.val(value);
                        $field.text(value);
                    }
                    break;
                default:
                    if ($field.prop('required') !== true && (value === "" || value === null) && !$field.hasClass("no-disable")) {
                        $field.val("");
                        $field.prop('required', false).prop('disabled', true);
                        $field.prev('.control-label').addClass('dim');
                        $field.closest('.group').find('.toggle_field .fa').replaceWith('<i class="fa fa-lock"></i>');
                        $field.closest('.group').find('.toggle_field').removeClass('enabled').addClass('disabled');
                    } else {
                        $field.val(value).prop('required', true).prop('disabled', false);
                        $field.prev('.control-label').removeClass('dim');
                        $field.closest('div.row').find('.toggle_field .fa').replaceWith('<i class="fa fa-unlock-alt"></i>');
                        $field.closest('div.row').find('.toggle_field').removeClass('disabled').addClass('enabled');
                    }
                    break;
            }

        });
        if ($("#current_image").length > 0) {
            $("#current_image").attr('src', base_url + "../images/" + data['type'] + "/" + data['filename']);
        }
    };

    performAjaxRequest(url, null, callback);
}

function initDropZone() {
    if ($("#imguploader").length > 0) {
        $("#imguploader").dropzone({
            url: base_url + 'galleries/upload',
            acceptedFiles: 'image/*',
            maxFilesize: 100,
            parallelUploads: 1,
            previewTemplate: '<div style="display:none"></div>',
            //previewsContainer: '#feedback',
            createImageThumbnails: false,
            paramName: 'userfile', //!!! IMPORTANT
            init: function () {
                this.on("success", function (file, response) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        var partialUrl = JSON.parse(response).partialUrl;
                        var parent = JSON.parse(response).parent;
                        getPartial(partialUrl, parent, null);
                    }
                });
                // Update the total progress bar
                this.on("totaluploadprogress", function (progress) {
                    $(".progress-bar").css("width", progress + "%");
                });
                // Error reporting
                this.on("error", function (file, response) {
                    $(".dz_errors").append('<span><i class="fa fa-warning"></i> ' + file.name + ' - ' + response + '</span>');
                });
                // Show the total progress bar when upload starts
                this.on("sending", function (file) {
                    $(".progress").css("opacity", 1);
                });
                // Hide the total progress bar when nothing's uploading anymore
                this.on("queuecomplete", function (progress) {
                    $(".progress").css("opacity", 0);
                });
            }
        });
    }
}

function initDatePicker() {
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        onSelect: function () {
            $(this).trigger("focus").trigger("blur");
        },
        onClose: function () {
            $(this).keyup();
        }
    });
    $(".timepicker").timepicker();
}

function initAutoComplete() {
    var url;
    var $form;
    var callback;
    if ($("#lang_form").length > 0) {
        url = base_url + "settings/getUnselectedLanguages";
        $form = $("#lang_form");
        callback = function (response) {
            $form.find(".autocomplete").autocomplete({
                source: response.langlist,
                select: function (e, ui) {
                    $form.find("#code").val(ui.item.code);
                    $form.find("#lang_id").val(ui.item.id);
                },
                change: function (e, ui) {
                    if (ui.item == null) {
                        $(this).val("");
                        $form.find("#code").val("");
                        $form.find("#lang_id").val("");
                    }
                }
            });
        };
    } else {
        return false;
    }
    performAjaxRequest(url, null, callback);
}

function initSortable() {

    $(".sortable").sortable({
        accept: ".sortable li",
        placeholder: "ui-state-highlight",
        appendTo: document.body,
        cursor: "move",
        //helper: "clone",
        scroll: true,
        //revert: true,        
        items: "li",
        scrollSpeed: 5,
        start: function (e, ui) {
            ui.item.find(".control_panel").hide();
        },
        stop: function (e, ui) {
            ui.item.find(".control_panel").show();
        },
        update: function (e, ui) {
            var ids = $(this).sortable('toArray', {attribute: 'data-id'});
            manager.reorder(ids);
        }
    }).disableSelection();
}

//HIDE VALIDATION FEEDBACK
function hideValidationFeedback(element) {
    element.find(".form-group").removeClass('has-error').removeClass('has-success');
    element.find(".form-group").find('.form-control-feedback').removeClass('glyphicon-remove');
    element.find(".tooltipstered").data('lastError', '').tooltipster('hide');
}

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

//UPDATE FEEDBACK MESSAGE
function updateFeedbackMessage(feedback_msg, type) {
    $('#feedback').append("<p class='feedback " + type + "'>" + feedback_msg + "</p>");
    if ($("#feedback p").length > 3) {
        $("#feedback p").first().hide(600, function () {
            $(this).remove();
        });
    }
}

/*******************************************************************************
 * 
 * SETTING ROUTES & DataTable OPTIONS
 * 
 ******************************************************************************/

function events() {
    manager.routes = {
        controller: 'events/',
        items: 'events/getEvents/',
        remove: 'events/deleteEvent/',
        dtReorder: 'events/reorder/',
        reorder: 'events/categoryReorder/',
        status: 'events/setStatus/',
        images: 'galleries/images/',
        partials: {
            entityForm: 'events/eventDialog/',
            categoryForm: 'events/categoryDialog/',
            categoryList: 'events/categoryList/',
            categoryOptions: 'events/categoryOptions/'
        },
        entity: "Event",
        dialogTitle: "Event properties"
    };
    var options = {
        ajax: manager.routes.items,
        columns: [
            {
                data: "start_date"
            },
            {
                //cover image
            },
            {
                data: "name"
            },
            {
                data: "category"
            },
            {
                data: "start_date"
            },
            {
                data: "end_date"
            },
            {
                //edit
                data: "id"
            },
            {
                //status
                data: "id"
            },
            {
                //delete
                data: "id"
            },
            {
                //gallery
                data: "gallery_id"
            }
        ],
        destroy: true,
        columnDefs: default_defs,
        order: [[4, "desc"]],
        createdRow: function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
        },
        initComplete: function () {
            tabulator.createSelectFilter();
        },
        responsive: true
    };
    datecolumn = 4;
    return options;
}

function projects() {
    manager.routes = {
        controller: 'projects/',
        items: 'projects/getProjects/',
        remove: 'projects/deleteProject/',
        dtReorder: 'projects/reorder/',
        reorder: 'projects/categoryReorder/',
        status: 'projects/setStatus/',
        images: 'galleries/images/',
        partials: {
            entityForm: 'projects/projectDialog/',
            categoryForm: 'projects/categoryDialog/',
            categoryList: 'projects/categoryList/',
            categoryOptions: 'projects/categoryOptions/'
        },
        entity: "Project",
        dialogTitle: "Project properties"
    };
    var options = {
        ajax: manager.routes.items,
        columns: [
            {
                data: "weight"
            },
            {
                //handle
            },
            {
                //cover image
            },
            {
                data: "name"
            },
            {
                data: "category"
            },
            {
                data: "date"
            },
            {
                //edit
                data: "id"
            },
            {
                //status
                data: "id"
            },
            {
                //delete
                data: "id"
            },
            {
                //gallery
                data: "gallery_id"
            }
        ],
        destroy: true,
        columnDefs: default_defs,
        order: [[0, "asc"]],
        //oLanguage: oLanguage,
        createdRow: function (row, data, index) {
            $('td:eq(0)', row).html(index + 1);
        },
        initComplete: function () {
            tabulator.createSelectFilter();
        },
        rowReorder: {
            update: false,
            selector: 'td:nth-child(2)'
        },
        responsive: true
    };
    datecolumn = 5;
    return options;
}

function galleries() {
    manager.routes = {
        controller: 'galleries/',
        items: 'galleries/getGalleries/',
        remove: 'galleries/deleteGallery/',
        dtReorder: 'galleries/reorder/',
        reorder: 'galleries/categoryReorder/',
        status: 'galleries/setStatus/',
        images: 'galleries/images/',
        partials: {
            entityForm: 'galleries/galleryDialog/',
            categoryForm: 'galleries/categoryDialog/',
            categoryList: 'galleries/categoryList/',
            categoryOptions: 'galleries/categoryOptions/'
        },
        entity: "Gallery",
        dialogTitle: "Gallery properties"
    };
    var options = {
        ajax: manager.routes.items,
        columns: [
            {
                data: "weight"
            },
            {
                //handle
            },
            {
                //cover image
            },
            {
                data: "name"
            },
            {
                data: "category"
            },
            {
                //edit
                data: "id"
            },
            {
                //status
                data: "id"
            },
            {
                //delete
                data: "id"
            },
            {
                //gallery
                data: "id"
            }
        ],
        destroy: true,
        columnDefs: default_defs,
        order: [[0, "asc"]],
        oLanguage: oLanguage,
        createdRow: function (row, data, index) {
            $('td:eq(0)', row).html(index + 1);
        },
        initComplete: function () {
            tabulator.createSelectFilter();
        },
        rowReorder: {
            update: false,
            selector: 'td:nth-child(2)'
        },
        responsive: true
    };
    return options;
}

function biography() {
    manager.routes = {
        controller: 'biography/',
        items: 'biography/getBio/',
        remove: 'biography/deleteBio/',
        dtReorder: 'biography/reorder/',
        reorder: 'biography/categoryReorder/',
        status: 'biography/setStatus/',
        partials: {
            entityForm: 'biography/bioDialog/',
            categoryForm: 'biography/categoryDialog/',
            categoryList: 'biography/categoryList/',
            categoryOptions: 'biography/categoryOptions/'
        },
        entity: "Biography",
        dialogTitle: "Entry properties"
    };
    var options = {
        ajax: manager.routes.items,
        columns: [
            {
                data: "weight"
            },
            {
                //handle
            },
            {
                data: "field1"
            },
            {
                data: "category"
            },
            {
                data: "start"
            },
            {
                data: "end"
            },
            {
                //edit
                data: "id"
            },
            {
                //status
                data: "id"
            },
            {
                //delete
                data: "id"
            }
        ],
        destroy: true,
        columnDefs: default_defs,
        order: [[0, "asc"]],
        createdRow: function (row, data, index) {
            $('td:eq(0)', row).html(index + 1);
        },
        initComplete: function () {
            tabulator.createSelectFilter();
        },
        rowReorder: {
            update: false,
            selector: 'td:nth-child(2)'
        },
        responsive: true
    };
    return options;
}

function menu() {
    manager.routes = {
        controller: 'menu/',
        items: 'menu/getMenu/',
        partials: {
            entityForm: 'menu/menuDialog/'
        },
        entity: "Menu item",
        dialogTitle: "Menu item properties"
    };
    return true;
}

function users() {
    manager.routes = {
        controller: 'users/',
        items: 'users/getMyData/',
        partials: {
            entityForm: 'users/userDialog/',
            passwordForm: 'users/passwordDialog/'
        },
        entity: "User",
        dialogTitle: "User data",
        passwordDialogTitle: "Change password"
    };
    return true;
}

function settings() {
    manager.routes = {
        controller: 'settings/',
        items: 'settings/languages/',
        metadata: 'settings/getMetaData/',
        remove: 'settings/removeLang/',
        status: 'settings/setDefaultLang/',
        dtReorder: 'settings/reorder',
        partials: {
            entityForm: 'settings/langDialog/',
            metaForm: 'settings/metaDialog/'
        },
        dialogTitle: "Languages",
        metaDialogTitle: "Meta data"
    };
    var options = {
        ajax: manager.routes.items,
        columns: [
            {
                data: "weight"
            },
            {
                //handle
            },
            {
                data: "label"
            },
            {
                data: "language"
            },
            {
                data: "code"
            },
            {
                //set default
                data: "id",
                render: function (data, type, full, meta) {
                    var cls1 = full['def'] == 1 ? "btn-success" : "btn-default";
                    var cls2 = full['def'] == 1 ? "fa-check" : "fa-times";
                    return '<a href="' + base_url + manager.routes.status + data + '" class="btn btn-sm ' + cls1 + ' status" title="státusz"><i class="fa ' + cls2 + '"></i></a>';
                }
            },
            {
                //delete
                data: "id",
                render: function (data, type, full, meta) {
                    if (!full['def']) {
                        return '<a href="' + base_url + manager.routes.remove + data + '" class="btn btn-sm btn-danger delete_item" title="töröl"><i class="fa fa-trash"></i></a>';
                    } else {
                        return "";
                    }
                }
            }
        ],
        destroy: true,
        columnDefs: default_defs,
        order: [[0, "asc"]],
        paging: false,
        //oLanguage: oLanguage,
        createdRow: function (row, data, index) {
            $('td:eq(0)', row).html(index + 1);
        },
        rowReorder: {
            update: false,
            selector: 'td:nth-child(2)'
        },
        responsive: true
    };

    return options;
}