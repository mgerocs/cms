<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->session->userdata('sitelang')['code']; ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo (!empty($meta->title)) ? $meta->title : ""; ?></title>
        <meta name="keywords" content="<?php echo (!empty($meta->keywords)) ? $meta->keywords : ""; ?>">
        <meta name="description" content="<?php echo (!empty($meta->description)) ? $meta->description : ""; ?>">
        <meta name="author" content="<?php echo (!empty($meta->author)) ? $meta->author : ""; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="<?php echo (!empty($meta->favicon)) ? base_url() . "images/brand/" . $meta->favicon : ""; ?>">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/fullpage/jquery.fullpage.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/scrollbar/jquery.custom-scrollbar.css" /> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/lightbox2/css/lightbox.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/font-awesome/css/font-awesome.min.css" />  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/flex-images/jquery.flex-images.css" />  
        <!--<link type="text/css" rel="stylesheet" href="<?php //echo base_url(); ?>css/style.css">-->

        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/modernizr/modernizr-custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>        
        <script>
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
        </script>

    </head>
    <body> 
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div class="container-fluid">            
            <div class="loader"></div>
            <h1>Empty project</h1>     
            <?php if (!empty($content)): ?>
                <?php $this->load->view($content); ?>
            <?php endif; ?>
            <img id="loader" src="ajax-loader.gif" style="display: none;"/>             
        </div>          

        <!-- JavaScript -->        
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/fullpage/jquery.fullpage.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/scrollbar/jquery.custom-scrollbar.min.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/lightbox2/js/lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery.interactive_bg.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/flex-images/jquery.flex-images.min.js"></script>
        <script>
            base_url = '<?php echo base_url(); ?>';
        </script>
        <!--<script type="text/javascript" src="<?php //echo base_url(); ?>js/script.js"></script>-->

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            //            (function (b, o, i, l, e, r) {
            //                b.GoogleAnalyticsObject = l;
            //                b[l] || (b[l] =
            //                        function () {
            //                            (b[l].q = b[l].q || []).push(arguments)
            //                        });
            //                b[l].l = +new Date;
            //                e = o.createElement(i);
            //                r = o.getElementsByTagName(i)[0];
            //                e.src = 'https://www.google-analytics.com/analytics.js';
            //                r.parentNode.insertBefore(e, r)
            //            }(window, document, 'script', 'ga'));
            //            ga('create', 'UA-XXXXX-X', 'auto');
            //            ga('send', 'pageview');
        </script>
    </body>
</html>
