
<?php if (!empty($categories)): ?>
    <?php foreach ($categories as $category) : ?>
        <li data-id="<?= $category['id']; ?>">
            <span><?= (!empty($category['category'])) ? $category['category'] : "[no translation]"; ?></span>
            <div class="control_panel">
                <button type="button" class="btn btn-sm btn-default op todisable category_control" data-op="update" data-id="<?= $category['id']; ?>" data-category="<?= $category['category']; ?>"><i class="fa fa-info"></i></button>
                <?php if (!in_array($category['id'], $categoriesofgalleries)): ?>                    
                    <a href="<?php echo base_url(); ?>admin/galleries/deleteGalleryCategory/<?= $category['id']; ?>" class="btn btn-sm btn-danger op todisable delete_item"><i class="fa fa-trash-o"></i></a>
                    <?php endif; ?>
            </div>
        </li>  
    <?php endforeach; ?>
<?php else: ?>
    <li>No categories defined.</li>
<?php endif; ?>


