<section class="no-spacing">
    <div class="subheader mb-md">
        <h2 class="header">Galleries</h2>           
    </div>

    <?php $this->load->view('admin/gallery/controls'); ?>

    <div>
        <?php $this->load->view($gallery_array); ?>
    </div>
</section>