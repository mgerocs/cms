
<table id="datatable" class="table table-hover datatable">
    <thead>
         <tr class="filters">            
            <td></td>
            <td></td>
            <td></td>
            <td class="form-group form-group-sm"><input type="text" class="form-control" placeholder="név"/></td>
            <td class="form-group form-group-sm"><select id="category_filter" class="form-control" placeholder="kategória"></select></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
         </tr>
        <tr>
            <th class="all"><!-- order --></th>
            <th class="no-order all handle"><!-- handle --></th>
            <th class="no-order coverImg all" data-priority="2"><!-- cover --></th>
            <th data-priority="3" class="all">Name</th>
            <th data-priority="4" class="min-tablet">Category</th> 
            <th data-priority="5" class="no-order op edit min-tablet-l"></th> 
            <th data-priority="5" class="no-order op setStatus min-tablet-l"></th> 
            <th data-priority="5" class="no-order op delete min-tablet-l"></th>
            <th data-priority="5" class="no-order op gallery min-tablet-l"></th>
        </tr>       
    </thead>

    <tfoot>
        <tr>            
            <th></th>           
            <th></th>
            <th></th>
            <th>Name</th> 
            <th>Category</th> 
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>




