
<?php if (!empty($images)) : ?>
    <?php foreach ($images as $img) : ?>            
        <li class="<?php echo ($img['cover'] == 1) ? "index" : ""; ?>" data-id="<?= $img['id']; ?>" data-filename="<?= $img['filename']; ?>">                
            
            <a href="<?php echo base_url(); ?>images/<?= $gallery->type; ?>/<?= $img['filename']; ?>" class="lightbox_trigger" data-lightbox="<?= $gallery->id; ?>" data-title="<?= $img['name']; ?>">
                <img src="<?php echo base_url(); ?>images/<?= $gallery->type; ?>/thumbs/<?= $img['filename']; ?>" class="img-thumbnail img-responsive" alt="" />
            </a>
            
            <div class='control_panel'>
                <a href="<?php echo base_url(); ?>admin/galleries/getImages/<?= $gallery->id; ?>/<?= $img['id']; ?>" class="btn btn-sm btn-primary edit_item">
                    <i class="fa fa-info"></i>
                </a>
                <div class="checkbox checkbox-success">
                    <input type="checkbox" class="cover" data-gallery_id="<?= $gallery->id; ?>" data-image_id="<?= $img['id']; ?>" <?php echo $img['cover'] == 1 ? 'checked' : ''; ?>/>
                    <label></label>
                </div>
                <a href='<?php echo base_url(); ?>admin/galleries/deleteImage/<?= $gallery->id; ?>/<?= $img['id']; ?>' class="btn btn-sm btn-danger delete_item">
                    <i class="fa fa-trash-o"></i>
                </a>
            </div>
            
        </li>
    <?php endforeach; ?>
<?php else: ?>
    <p class="noresult">No images uploaded.</p>
    <?php endif; ?>





