<div class="narrow_dialog">
    <form class="form" method="post" id="category_form" action="<?php echo base_url(); ?>admin/galleries/validateCategoryForm">
        <div class="row">        
            <ul id="category_list" class="sortable">
                <?php $this->load->view($category_list); ?>
            </ul>        
        </div>
        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>
        <div class="row">
            <button type="button" class="btn btn-success mt-xs todisable category_control" data-op="new"><i class="fa fa-plus"></i> new category</button>
        </div>

        <div class="row" style="height: 120px;"> 
            <div class="toggle_form_group mt-xs">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="category">Category:</label>
                    <input type="text" id="category" name="category" class="form-control" required />
                    <span class="glyphicon form-control-feedback"></span>
                </div>
                <input type="hidden" id="category_id" name="category_id" value="">
                <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">
                <input type="hidden" id="op" name="op" value="">                        
                <div class="button_panel mt-xs">
                    <button type="submit" class="btn btn-default op">Ok</button>
                    <button type="button" class="btn btn-default op" id="cancel_add_category">Cancel</button>
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="dialog_controls">
                <button type="button" class="btn btn-default close-dialog">Cancel</button>            
            </div>
        </div>        
    </form>
</div>


