<section class="no-spacing">
    <div class="subheader mb-md">
        <h2 class="header"><?= $gallery->name; ?></h2>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <ul id="image_array" class="sortable">
                <?php $this->load->view($image_array); ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="controls">
                <?php $this->load->view($controls); ?>
            </div>
        </div>
    </div>
</section>
