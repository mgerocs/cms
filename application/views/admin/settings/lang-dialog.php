<div class="narrow_dialog">
    <form action="<?php echo base_url(); ?>admin/settings/validateLangForm" id="lang_form" class="form" method="post" role="form" novalidate>

        <div class="row">        
            <div class="form-group form-group-sm has-feedback">
                <label class="control-label" for="lang">Language</label>
                <input type="text" class="form-control autocomplete" id="langpicker" name="language" placeholder="start typing..." required>
                <span class="glyphicon form-control-feedback"></span>
            </div> 
            <div class="form-group form-group-sm">
                <label class="" for="lang_code">Code</label>
                <input type="text" class="form-control" id="code" disabled>                        
            </div>         
        </div>    

        <input type="hidden" id="lang_id" name="lang_id" value="">
        <input type="hidden" id="op" name="op" value="">    

        <div class="dialog_controls mt-xs">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>

