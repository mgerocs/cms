<div class="row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"> 
        <div class="preview">
            <?php if (!empty($logo)): ?>            
                <img src="<?php echo base_url(); ?>images/brand/<?= $logo; ?>?token=<?= uniqid(); ?>" class="img-thumbnail img-responsive" alt="logo" />
            <?php else: ?>
                <img src="<?php echo base_url(); ?>css_admin/decor/placeholder.jpg" class="img-responsive" alt="logo" />
            <?php endif; ?>
        </div>
    </div>
    <div class="col-xs-9 col-sm-9 col-md-8 col-lg-6">
        <div class="custom-upload">
            <form action="<?php echo base_url(); ?>admin/settings/upload" method="post" enctype="multipart/form-data" class="form" novalidate>                    
                <input type="file" name="userfile" required>
                <div class="fake_file form-group">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="browse..." disabled>
                        <span class="input-group-addon fake_upload_trigger"><i class="fa fa-ellipsis-h"></i></span>
                    </div>
                </div>
                <input type="hidden" name="type" value="logo">
                <?php if (!empty($logo)): ?>
                    <a href="<?php echo base_url(); ?>admin/settings/deleteImage/logo" class="btn btn-sm btn-danger delete_uploaded" title="delete"><i class="fa fa-trash-o"></i></a>
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-default" title="upload"><i class="fa fa-upload"></i></button>
            </form>
        </div>
    </div>
</div>
