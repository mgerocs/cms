<section class="no-spacing">
    <div class="subheader mb-md">
        <h2>Settings</h2>           
    </div>

    <div class="subheader mb-md">
        <h3>Languages</h3>           
    </div>

    <?php $this->load->view('admin/settings/controls'); ?>

    <div id="langlist">
        <?php $this->load->view($lang_array); ?>
    </div>

    <hr>

    <div class="subheader mb-sm">
        <h3>Brand</h3>    
    </div>
    <div id="brand">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h4 class="subheader">Logo</h4>
                <div id="logo">
                    <?php $this->load->view('admin/settings/logo-form'); ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <h4 class="subheader">Favicon</h4>
                <div id="favicon">
                    <?php $this->load->view('admin/settings/favicon-form'); ?>
                </div>
            </div>
        </div>
    </div>

    <hr>

    <div class="subheader mb-md">
        <h3>SEO</h3>    
        <button type="button" class="btn btn-primary" id="edit_meta">Meta data</button>
    </div>
</section>