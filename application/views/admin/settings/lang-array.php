

<table id="datatable" class="table table-hover datatable">
    <thead>         
        <tr>
            <th class="all"><!-- order --></th>
            <th class="no-order all handle"><!-- handle --></th>    
            <th class="all">Label</th>
            <th class="no-order min-tablet-l">Language</th> 
            <th class="no-order min-tablet-l">Code</th>             
            <th data-priority="5" class="no-order op min-tablet-l"></th> 
            <th data-priority="5" class="no-order op min-tablet-l"></th>            
        </tr>       
    </thead>

    <tfoot>
        <tr>            
            <th></th>
            <th></th>
            <th>Label</th>
            <th>Language</th>
            <th>Code</th> 
            <th></th> 
            <th></th>
        </tr>
    </tfoot>
</table>