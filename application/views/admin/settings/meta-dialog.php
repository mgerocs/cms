<div class="narrow_dialog">
    <form action="<?php echo base_url(); ?>admin/settings/validateMetaForm" id="meta_form" class="form" method="post" role="form" novalidate>

        <div class="row group">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="title">Title</label>
                    <input class="form-control" id="title" name="title" placeholder="title">
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <span class="field_lock"><a href="" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>
        </div>
        
        <div class="row group">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="keywords">Keywords</label>
                    <textarea class="form-control" id="keywords" name="keywords" placeholder="keywords"></textarea>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <span class="field_lock"><a href="" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>
        </div>

        <div class="row group">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" placeholder="description"></textarea>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <span class="field_lock"><a href="" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>
        </div>    
        
        <div class="row group">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="author">Author</label>
                    <input class="form-control" id="author" name="author" placeholder="author">
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <span class="field_lock"><a href="" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>
        </div>

        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>

        <input type="hidden" id="id" name="id" value="">   
        <input type="hidden" id="gallery_id" name="gallery_id" value="">   
        <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">  
        <input type="hidden" id="op" name="op" value="">   

        <div class="dialog_controls mt-xs">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>