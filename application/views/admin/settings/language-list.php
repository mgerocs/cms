<?php if ($langswitch && !empty($languages)): ?>
    <div class="lang_controls mt-xs">
        <?php foreach ($languages as $lang): ?>
        <?php $cls = ($lang['def'] == 1) ? "btn-warning actual default" : "btn-default"; ?>
            <button type="button" class="btn todisable lang_control <?= $cls; ?>" data-lang="<?= $lang['id']; ?>"><?= $lang['code']; ?></button>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
