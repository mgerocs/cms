<section class="no-spacing">
    <div class="subheader mb-md">
        <h2>Projects</h2>           
    </div>

    <?php $this->load->view('admin/project/controls'); ?>

    <div>
        <?php $this->load->view($project_array); ?>
    </div>
</section>
