<div class="wide_dialog">
    <form action="<?php echo base_url(); ?>admin/projects/validateProjectForm" id="project_form" class="form" method="post" role="form" novalidate>

        <div class="group">
            <div class="form-group form-group-sm has-feedback no-toggle">
                <label class="control-label" for="name">Name</label>
                <input class="form-control no-disable" id="name" name="name" placeholder="name" required>
                <span class="glyphicon form-control-feedback"></span>
            </div>
        </div>        

        <div class="group">            
            <div class="form-group form-group-sm has-feedback no-toggle">
                <label class="control-label" for="category_id">Category</label>
                <select class="form-control no-disable" id="category_id" name="category_id"> 
                    <?php $this->load->view($category_options); ?>                   
                </select>
                <span class="glyphicon form-control-feedback"></span>
            </div>       
        </div>

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="date">Date</label>
                    <input type="date" class="form-control datepicker" id="date" name="date" placeholder="date" disabled />
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>           
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>                    
        </div>

        <div class="row group">            
            <div class="form-group form-group-sm has-feedback" style="min-height: 280px;">
                <label class="control-label" for="description">Description</label>
                <textarea class="form-control no-disable editor" id="description" name="description" placeholder="description"></textarea>
                <span class="glyphicon form-control-feedback"></span>
            </div>                    
        </div>    

        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>

        <input type="hidden" id="id" name="id" value="">       
        <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">       
        <input type="hidden" id="op" name="op" value="">   

        <div class="dialog_controls">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>