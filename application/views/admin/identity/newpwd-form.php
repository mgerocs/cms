<section class="mt-lg">

    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3">
        <h3>Set new password</h3>

        <form id="setnewpwdform" class="form" action="<?php echo base_url(); ?>admin/identity/validateNewPwdForm" novalidate>
            <div class="form-group form-group-sm">
                <label for="new_password" class="control-label">E-mail:</label>
                <div class="input-group input-group-sm">                    
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="teyt" class="form-control" value="<?= $recovery['email']; ?>" disabled>                    
                </div>
            </div>
            <div class="form-group form-group-sm has-feedback">
                <label for="new_password" class="control-label">Password:</label>
                <div class="input-group input-group-sm">                    
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" id="new_password_" name="new_password" class="form-control" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>                        
            <div class="form-group form-group-sm has-feedback">
                <label for="new_password_conf" class="control-label">Confirm password:</label>
                <div class="input-group input-group-sm">      
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" id="new_password_conf" name="new_password_conf" class="form-control" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>  
            <input type="hidden" name="recovery_id" value="<?= $recovery['id']; ?>" />
            <input type="hidden" name="email" value="<?= $recovery['email']; ?>" />
            <div class="button_panel">
                <a href="<?php echo base_url(); ?>admin" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </form>

</section>