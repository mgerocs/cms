<section>

    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3 mt-lg text-center">
        
        <h4>A link required for changing your password has been sent to the e-mail address you provided.</h4>  

        <a href="<?php echo base_url(); ?>admin" class="btn btn-default mt-sm">Back</a>
    </div>

</section>