<section style="margin-top: 6vw;">

    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4 col-sm-offset-2 col-md-offset-3 col-lg-offset-4">
        <h2>Sign in</h2>
        <form id="loginform" class="form" method="post" action="<?php echo base_url(); ?>admin/identity/validateLoginForm" novalidate>
            <div class="form-group form-group-sm">
                <label for="email" class="control-label">E-mail:</label>
                <div class="input-group input-group-sm"> 
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" id="email" name="email" class="form-control" placeholder="e-mail" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label for="password" class="control-label">Password:</label>
                <div class="input-group input-group-sm"> 
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" id="password" name="password" class="form-control" placeholder="password" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>

            <div id="captcha_panel">
                <div id="captcha_img"><?php echo $captcha; ?></div>
                <button type="button" id="reload_captcha" class="btn btn-default" title="Give me a new CAPTCHA!"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="form-group form-group-sm has-feedback">  
                <label class="control-label"></label>
                <div class="input-group input-group-sm"> 
                    <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                    <input type="text" id="captcha" name="captcha" class="form-control tooltip-right" placeholder="Type the numbers in the image above." required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div> 

            <input type="hidden" name="ajax" value="" />
            <button type="submit" id="signin_button" class="btn btn-success tooltip-right">Sign in</button> 
            <br><a href="<?php echo base_url(); ?>admin/identity/pwdrecovery" class="">I forgot my password</a>
        </form>
    </div>

</section>

