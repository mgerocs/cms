<section>

    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4 col-sm-offset-2 col-md-offset-3 col-lg-offset-4">
        <h3>Request new password</h3>
        <br>
        <p>Give your e-mail address to which a link will be sent.<br>
            Click on the link, and set your new password.</p>

        <form id="pwdrecoveryform" class="form" action="<?php echo base_url(); ?>admin/identity/validatePwdRecForm" novalidate>
            <div class="form-group form-group-sm has-feedback">
                <label for="email" class="control-label">E-mail:</label>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" id="pwdrec_email" name="email" placeholder="e-mail" class="form-control" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>   
            <div class="button_panel">
                <a href="<?php echo base_url(); ?>admin" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </form>


    </div>

</section>