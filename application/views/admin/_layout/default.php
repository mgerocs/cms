<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Admin</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/normalize.css">        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/dropzone/css/dropzone.css" />        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/jquery-ui/jquery-ui.min.css" />  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/nestable/nestable.css" />  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/lightbox2/css/lightbox.min.css" />
<!--        <link rel="stylesheet" type="text/css" href="<?php // echo base_url();      ?>vendor/datatables/media/css/jquery.dataTables.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/datatables/media/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/datatables/extensions/RowReorder/css/rowReorder.bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/datatables/extensions/Responsive/css/dataTables.responsive.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/font-awesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/tooltipster/css/tooltipster.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/tooltipster/css/themes/tooltipster-light.css"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/bootstrap/css/awesome-bootstrap-checkbox.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/bootstrap-select/css/bootstrap-select.min.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css_admin/style.css">

<!--<script src="js/vendor/modernizr-2.8.3.min.js"></script>-->
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- NAVIGATION -->
        <?php $this->load->view('admin/_layout/navigation'); ?>

        <!-- Add your site or application content here -->
        <div class="container-fluid">

            <div id="main-wrapper"> 
                <div class="col-sm-1 hidden-xs"></div>     
                <!-- MAIN CONTENT -->
                <div id="main-content" class="col-xs-12 col-sm-10">
                    <?php if (!empty($content)): ?>
                        <?php $this->load->view($content); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-1 hidden-xs"></div> 
            </div>

        </div>  

        <div id="dialog" title="">
            <!-- placeholder for message and form dialog -->            
        </div>
        <div id="confirm" title="" class="no-titlebar">
            <!-- placeholder for confirm dialog -->
        </div>

        <img id="loader" src="<?php echo base_url(); ?>ajax-loader.gif" style="display: none; z-index: 999999;">

        <!-- JavaScript -->
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/dropzone/dropzone.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery-ui/jquery-ui.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/nestable/jquery.nestable.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/ckeditor/ckeditor.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/ckeditor/adapters/jquery.js"></script>         
        
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery-ui/timepicker-addon.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/jquery-serialize-object/dist/jquery.serialize-object.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/form-validation/dist/jquery.validate.min.js"></script>
        <!--<script type="text/javascript" src="<?php //echo base_url(); ?>vendor/form-validation/dist/localization/messages_hu.min.js"></script> -->
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/lightbox2/js/lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/datatables/media/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/datatables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/tooltipster/js/jquery.tooltipster.min.js"></script>  
        <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bootstrap-select/js/bootstrap-select.min.js"></script> 

        <script>
            base_url = '<?php echo base_url(); ?>admin/';
        </script>

        <script type="text/javascript" src="<?php echo base_url(); ?>js_admin/script.js"></script> 

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
//            (function (b, o, i, l, e, r) {
//                b.GoogleAnalyticsObject = l;
//                b[l] || (b[l] =
//                        function () {
//                            (b[l].q = b[l].q || []).push(arguments)
//                        });
//                b[l].l = +new Date;
//                e = o.createElement(i);
//                r = o.getElementsByTagName(i)[0];
//                e.src = 'https://www.google-analytics.com/analytics.js';
//                r.parentNode.insertBefore(e, r)
//            }(window, document, 'script', 'ga'));
//            ga('create', 'UA-XXXXX-X', 'auto');
//            ga('send', 'pageview');
        </script>
    </body>
</html>

