<li>
    <a href="<?php echo base_url(); ?>admin/events" class="menuitem ajax" data-action="events" title="Events"><i class="fa fa-star"></i>Events</a>
</li>
<li>
    <a href="<?php echo base_url(); ?>admin/projects" class="menuitem ajax" data-action="projects" title="Projects"><i class="fa fa-folder-open-o"></i>Projects</a>
</li>
<!--<li>
    <a href="<?php //echo base_url(); ?>admin/galleries" class="menuitem ajax" data-action="galleries" title="Galériák"><i class="fa fa-picture-o"></i>Galériák</a>
</li>  -->
<li>
    <a href="<?php echo base_url(); ?>admin/biography" class="menuitem ajax" data-action="biography" title="CV"><i class="fa fa-graduation-cap"></i>CV</a>
</li>
<li>
    <a href="<?php echo base_url(); ?>admin/galleries/images/slider" class="menuitem images" data-action="slider" title="Slider"><i class="fa fa-th-large"></i>Slider</a>
</li>  
<li>
    <a href="<?php echo base_url(); ?>admin/menu" class="menuitem ajax" data-action="menu" title="Menu"><i class="fa fa-sitemap"></i>Menu</a>
</li>