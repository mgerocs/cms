<li>
    <a href="<?php echo base_url(); ?>admin/users" class="menuitem ajax" data-action="users" title="Users"><i class="fa fa-user"></i>Users</a>
</li>
<li>
    <a href="<?php echo base_url(); ?>admin/settings" class="menuitem ajax" data-action="settings" title="Setting"><i class="fa fa-sliders"></i>Settings</a>
</li>
<li>
    <a href="<?php echo base_url(); ?>admin/identity/logout" class="menuitem logout" title="Sign out"><i class="fa fa-sign-out"></i>Sign out</a>
</li>                
