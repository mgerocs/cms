<div class="navbar navbar-default navbar-fixed-top">    
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="#">
            <div class="brand_wrapper">
                <span id="navbar_logo">
                    <?php if (count(get_object_vars($meta)) > 0 && !empty($meta->logo)): ?>
                        <img src="<?php echo base_url(); ?>images/brand/<?= $meta->logo; ?>" class="img-responsive navbar_logo" alt="logo" />
                    <?php endif; ?>
                </span>
                <span class="brand">Admin</span>
            </div>
        </a>
    </div>

    <?php if ($this->session->userdata('is_logged_in')): ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav visible-xs">                    
                <?php $this->load->view('admin/_layout/menuitems-1'); ?> 
                <?php $this->load->view('admin/_layout/menuitems-2'); ?>                     
            </ul>     
            <ul class="nav navbar-nav navbar-right">
                <li class="navitem noselect hidden-xs">                       
                    <span class="btn navbar-btn">  
                        <i class="fa fa-language"></i><span id="lang_code"><?= $this->session->userdata('language')->code; ?></span>                            
                    </span>                        
                </li>
                <li class="navitem noselect hidden-xs">                       
                    <span class="btn navbar-btn">
                        <span id="logged_in"><i class="fa fa-user"></i><?= $this->session->userdata('user')->email; ?></span> [<?= $this->session->userdata('user')->group; ?>]
                    </span>                        
                </li>                    

            </ul>
        </div><!-- /.navbar-collapse -->
    <?php endif; ?>       
</div>

<!-- MAIN MENU -->
<nav class="col-sm-1 hidden-xs left">
    <?php if ($this->session->userdata('is_logged_in')): ?>
        <ul> 
            <?php $this->load->view('admin/_layout/menuitems-1'); ?>
        </ul>
    <?php endif; ?>
</nav>

<nav class="col-sm-1 hidden-xs right">
    <?php if ($this->session->userdata('is_logged_in')): ?>
        <ul>       
            <?php $this->load->view('admin/_layout/menuitems-2'); ?>
        </ul>
    <?php endif; ?>
</nav>