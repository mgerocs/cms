<section class="no-spacing">
    <div class="subheader mb-md">
        <h2 class="header">Users</h2>           
    </div>

    <h4 class="subheader">Portrait</h4>
    <div id="portrait">
        <?php $this->load->view('admin/user/portrait-form'); ?>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin: 0; padding: 0;">
            <h4 class="subheader">Data</h4>
            <a href="<?php echo base_url(); ?>admin/users/getMyData" class="btn btn-primary edit_item"><i class="fa fa-home"></i> Contact</a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin: 0; padding: 0;">
            <h4 class="subheader">Security</h4>
            <button type="button" id="change_pwd" class="btn btn-warning"><i class="fa fa-key"></i> Change password</button>
        </div>
    </div>
</section>




