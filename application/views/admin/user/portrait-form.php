<div class="row">
    <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1"> 
        <div class="preview">
            <?php if (!empty($portrait)): ?>            
                <img src="<?php echo base_url(); ?>images/brand/<?= $portrait; ?>?token=<?= uniqid(); ?>" class="img-thumbnail img-circle img-responsive" alt="portré" />
            <?php else: ?>
                <img src="<?php echo base_url(); ?>css_admin/decor/user-placeholder.png" class="img-responsive" alt="portrait" />
            <?php endif; ?>            
        </div>
    </div>
    <div class="col-xs-9 col-sm-6 col-md-4 col-lg-3">
        <div class="custom-upload">
            <form action="<?php echo base_url(); ?>admin/users/upload" method="post" enctype="multipart/form-data" class="form" novalidate>                    
                <input type="file" name="userfile" required>
                <div class="fake_file form-group">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="browse..." disabled>
                        <span class="input-group-addon fake_upload_trigger"><i class="fa fa-ellipsis-h"></i></span>
                    </div>
                </div>
                <input type="hidden" name="type" value="portrait">
                <?php if (!empty($portrait)): ?>
                    <a href="<?php echo base_url(); ?>admin/users/deleteImage/portrait" class="btn btn-sm btn-danger delete_uploaded" title="delete"><i class="fa fa-trash-o"></i></a>
                <?php endif; ?>
                <button type="submit" class="btn btn-sm btn-default" title="upload"><i class="fa fa-upload"></i></button>
            </form>
        </div>
    </div>
</div>
