<div class="wide_dialog">
    <form action="<?php echo base_url(); ?>admin/users/validateUserForm" class="form" id="userdata_form" method="post" role="form" novalidate>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="lastname">Lastname</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placegolder="lastname">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div> 
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="firstname">Firstname</label>
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="firstname">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div> 
                </div>
            </div>
        </div>

        <div class="row group"> 
            <div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback no-toggle">
                    <label class="control-label" for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="e-mail" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>   
            </div>
        </div>

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="address">
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="postal_code">Postal code</label>
                            <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="postal code">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" placeholder="city">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="phone1">Phone 1</label>
                            <input type="text" class="form-control" id="phone1" name="phone1" placeholder="phone">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="phone2">Phone 2</label>
                            <input type="text" class="form-control" id="phone2" name="phone2" placeholder="phone">
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
        </div>

        <!--
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                            <div class="form-group has-feedback no-toggle">
                                <label class="control-label" for="user_role_id">Szerepkör</label>
                                <select class="form-control no-disable" id="user_role_id" name="role_id" required>                                
        <?php // $this->load->view($role_options); ?>
                                </select>
                                <span class="glyphicon form-control-feedback"></span>
                            </div>
                        </div>
                    </div>        -->

        <input type="hidden" id="id" name="id" value="">
        <input type="hidden" id="op" name="op" value="">            

        <div class="dialog_controls">   
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Update</button>
        </div>
    </form>
</div>
