<div class="narrow_dialog">
    <form action="<?php echo base_url(); ?>admin/users/validatePasswordForm" id="password_form" class="form" method="post" role="form" novalidate>

        <div class="form-group form-group-sm has-feedback no-toggle">
            <label class="control-label" for="old_password">Current password</label>
            <input type="password" class="form-control no-disable" id="old_password" name="old_password" required>
            <span class="glyphicon form-control-feedback"></span>
        </div>
        <div class="form-group form-group-sm has-feedback no-toggle">
            <label class="control-label" for="password">New password</label>
            <input type="password" class="form-control no-disable" id="ch_password_" name="ch_password" required>
            <span class="glyphicon form-control-feedback"></span>
        </div>
        <div class="form-group form-group-sm has-feedback no-toggle">
            <label class="control-label" for="password_conf">Confirm new password</label>
            <input type="password" class="form-control no-disable" id="ch_password_conf_" name="ch_password_conf" required>
            <span class="glyphicon form-control-feedback"></span>
        </div>
        <div class="dialog_controls">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>