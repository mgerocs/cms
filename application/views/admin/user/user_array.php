    
<?php if (!empty($users)): ?>

    <?php
    $superadmins = array();
    $admins = array();
    $normal_users = array();

    foreach ($users as $user) {
        if ($user['role_id'] == 3) {
            $superadmins[] = $user;
        } elseif ($user['role_id'] == 2) {
            $admins[] = $user;
        } else {
            $normal_users[] = $user;
        }
    }
    ?>
    <h4>Szuperadmin</h4>
    <table class="table table-responsive table-hover">
        <thead>
            <tr>
                <th>E-mail</th>
                <th>Szerepkör</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>            
            <?php foreach ($superadmins as $user): ?>                
                <tr>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['role']; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>User_controller/user/<?= $user['user_id']; ?>" class="btn-sm btn-primary info" title="szerkeszt"><i class="fa fa-info-circle"></i></a>
                    </td>
                    <td>                        
                        <a href="<?php echo base_url(); ?>User_controller/removeUser/<?= $user['user_id']; ?>" class="btn-sm btn-danger delete" title="töröl"><i class="fa fa-trash"></i></a>                        
                    </td>
                    <td>
                        <?php if ($user['user_id'] == $this->session->userdata('user_id') || $this->session->userdata('role') == 3): ?>
                            <button type="button" value="<?= $user['user_id']; ?>" class="btn-sm btn-warning pwd_change"><i class="fa fa-key"></i></button>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($user['status'] == 1): ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/0" class="btn-sm btn-success status" title="deaktivál"><i class="fa fa-check"></i></a>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/1" class="btn-sm btn-default status" title="aktivál"><i class="fa fa-close"></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>        
    </table>

    <h4>Admin</h4>
    <table class="table table-responsive table-hover">
        <thead>
            <tr>
                <th>E-mail</th>
                <th>Szerepkör</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>           
            <?php foreach ($admins as $user): ?>                
                <tr>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['role']; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>User_controller/user/<?= $user['user_id']; ?>" class="btn-sm btn-primary info" title="szerkeszt"><i class="fa fa-info-circle"></i></a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>User_controller/removeUser/<?= $user['user_id']; ?>" class="btn-sm btn-danger delete" title="töröl"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>
                        <?php if ($user['user_id'] == $this->session->userdata('user_id') || $this->session->userdata('role') == 3): ?>
                            <button type="button" value="<?= $user['user_id']; ?>" class="btn-sm btn-warning pwd_change"><i class="fa fa-key"></i></button>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($user['status'] == 1): ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/0" class="btn-sm btn-success status" title="deaktivál"><i class="fa fa-check"></i></a>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/1" class="btn-sm btn-default status" title="aktivál"><i class="fa fa-close"></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>        
    </table>

    <h4>Felhasználó</h4>
    <table class="table table-responsive table-hover">
        <thead>
            <tr>
                <th>E-mail</th>
                <th>Szerepkör</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>          
            <?php foreach ($normal_users as $user): ?>                
                <tr>
                    <td><?= $user['email']; ?></td>
                    <td><?= $user['role']; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>User_controller/user/<?= $user['user_id']; ?>" class="btn-sm btn-primary info" title="szerkeszt"><i class="fa fa-info-circle"></i></a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>User_controller/removeUser/<?= $user['user_id']; ?>" class="btn-sm btn-danger delete" title="töröl"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>
                        <?php if ($user['user_id'] == $this->session->userdata('user_id') || $this->session->userdata('role') == 3): ?>
                            <button type="button" value="<?= $user['user_id']; ?>" class="btn-sm btn-warning pwd_change"><i class="fa fa-key"></i></button>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($user['status'] == 1): ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/0" class="btn-sm btn-success status" title="deaktivál"><i class="fa fa-check"></i></a>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>User_controller/setStatus/<?= $user['user_id']; ?>/1" class="btn-sm btn-default status" title="aktivál"><i class="fa fa-close"></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>        
    </table>


<?php else: ?>
    <p class="noresult">Nincs találat.</p>
<?php endif; ?>