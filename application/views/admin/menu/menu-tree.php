
<?php if (!empty($menu_tree)): ?>
    <div class="dd nestable" id="menu_tree_1">
        <ol class="dd-list">
            <?php foreach ($menu_tree as $item): ?>
                <li class="dd-item" data-id="<?= $item['id']; ?>">
                    <div class="dd-handle"><?= $item['label']; ?></div>
                    <?php if (array_key_exists('children', $item)): ?>
                        <ol class="dd-list">
                            <?php foreach ($item['children'] as $child): ?>
                                <li class="dd-item" data-id="<?= $child['id']; ?>">
                                    <div class="dd-handle"><?= $child['label']; ?></div>
                                    <?php if (array_key_exists('children', $child)): ?>
                                        <ol class="dd-list">
                                            <?php foreach ($child['children'] as $grandchild): ?>
                                                <li class="dd-item" data-id="<?= $grandchild['id']; ?>">
                                                    <div class="dd-handle"><?= $grandchild['label']; ?></div>                                                    
                                                </li>
                                            <?php endforeach; ?>
                                        </ol>
                                    <?php endif; ?> 
                                </li>
                            <?php endforeach; ?>
                        </ol>
                    <?php endif; ?>                    
                </li>  
            <?php endforeach; ?>
        </ol>
    </div>
<?php else: ?>
<p class="no-result">No menuitems.</p>
<?php endif; ?>


