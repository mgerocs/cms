<div class="wide_dialog">
    <form action="<?php echo base_url(); ?>admin/menu/validateMenuForm" id="meta_form" class="form" method="post" role="form" novalidate>

        <div class="row group">            
            <div class="form-group form-group-sm has-feedback">
                <label class="control-label" for="label">Label</label>
                <input type="text" class="form-control no-disable" id="label" name="label" placeholder="label" required>
                <span class="glyphicon form-control-feedback"></span>
            </div> 
        </div>

        <div class="row group">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label" for="url">Path</label>
                    <div>
                        <span id="url_prefix"><?php echo base_url(); ?></span><input type="text" class="form-control no-disable" id="url" name="url" placeholder="path" required>
                        <span class="glyphicon form-control-feedback"></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label class="checkbox-inline" style="font-size: 0.9em; padding-top: 26px;">
                    <input type="checkbox" class="form-control no-disable" value="0" id="external">external
                </label>
            </div>
        </div>        

        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>

        <input type="hidden" id="id" name="id" value="">   
        <input type="hidden" id="gallery_id" name="gallery_id" value="">   
        <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">  
        <input type="hidden" id="op" name="op" value="">   

        <div class="dialog_controls mt-xs">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>