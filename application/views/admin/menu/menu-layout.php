<section class="no-spacing">

    <div class="subheader mb-md">
        <h2>Menu</h2>           
    </div>    

    <?php $this->load->view('admin/menu/controls'); ?>

    <div id="menu_tree">
        <?php $this->load->view('admin/menu/menu-tree'); ?>                   
    </div>

</section>