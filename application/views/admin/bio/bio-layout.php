<section class="no-spacing">
    <div class="subheader mb-md">
        <h2 class="header">Biography</h2>           
    </div>

    <?php $this->load->view('admin/bio/controls'); ?>

    <div>
        <?php $this->load->view($bio_array); ?>
    </div>
</section>