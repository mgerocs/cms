
<table id="datatable" class="table table-hover datatable">
    <thead>
        <tr class="filters">              
            <td></td>                      
            <td></td>
            <td class="form-group form-group-sm"><input type="text" class="form-control" placeholder="label"/></td>
            <td class="form-group form-group-sm"><select id="category_filter" class="form-control" placeholder="category"></select></td>
            <td class="form-group form-group-sm"><select id="start_filter" class="form-control" placeholder="start"></select></td>
            <td class="form-group form-group-sm"><select id="end_filter" class="form-control" placeholder="end"></select></td>
            <td></td>
            <td></td>
            <td></td>            
        </tr>
        <tr>              
            <th class="all"><!-- order --></th>
            <th class="no-order all handle"><!-- handle --></th>                
            <th data-priority="3" class="all">Label</th>
            <th data-priority="4" class="min-tablet">Category</th> 
            <th data-priority="4" class="min-tablet">Start</th> 
            <th data-priority="4" class="min-tablet">End</th> 
            <th data-priority="5" class="no-order op edit min-tablet-l"></th> 
            <th data-priority="5" class="no-order op setStatus min-tablet-l"></th> 
            <th data-priority="5" class="no-order op delete min-tablet-l"></th>            
        </tr>       
    </thead>

    <tfoot>
        <tr>              
            <th></th>  
            <th></th>
            <th>Label</th> 
            <th>Category</th> 
            <th>Start</th> 
            <th>End</th> 
            <th></th>
            <th></th>
            <th></th>            
        </tr>
    </tfoot>
</table>




