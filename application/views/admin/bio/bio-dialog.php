<div class="wide_dialog">
    <form action="<?php echo base_url(); ?>admin/biography/validateBioForm" id="gallery_form" class="form" method="post" role="form" novalidate>

        <div class="group">            
            <div class="form-group form-group-sm has-feedback no-toggle">
                <label class="control-label" for="project_category_id">Category</label>
                <select class="form-control no-disable" id="category_id" name="category_id"> 
                    <?php $this->load->view($category_options); ?>                   
                </select>
                <span class="glyphicon form-control-feedback"></span>
            </div>       
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="start">Start</label>
                            <input type="text" class="form-control no-disable" id="start" name="start" placeholder="year" required>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <label class="checkbox-inline" style="font-size: 0.9em; padding-top: 26px;">
                            <input type="checkbox" class="form-control no-disable" id="in_progress" name="in_progress" value="1">in progress
                        </label>
                    </div> 
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="end">End</label>
                            <input type="text" class="form-control" id="end" name="end" placeholder="year" disabled>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div> 
                </div>
            </div>
        </div>

        <div class="group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback no-toggle">
                    <label class="control-label" for="field1">Field 1</label>
                    <input type="text" class="form-control no-disable" id="field1" name="field1" placeholder="field 1" required>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>
        </div>  

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="field2">Field 2</label>
                    <input type="text" class="form-control" id="field2" name="field2" placeholder="field 2" disabled>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>           
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>                    
        </div>  

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="field2">Field 3</label>
                    <input type="text" class="form-control" id="field3" name="field3" placeholder="field 3" disabled>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>           
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>                    
        </div> 

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="field4">Field 4</label>
                    <input type="text" class="form-control" id="field4" name="field4" placeholder="field 4" disabled>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>           
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>                    
        </div> 

        <div class="row group">
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="url">URL</label>
                    <input type="url" class="form-control" id="url" name="url" placeholder="URL" disabled>
                    <span class="glyphicon form-control-feedback"></span>
                </div>
            </div>           
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>                    
        </div>         

        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>

        <input type="hidden" id="id" name="id" value="">       
        <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">       
        <input type="hidden" id="op" name="op" value="">   

        <div class="dialog_controls">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>