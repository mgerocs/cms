<div class="row datefilter">
    <div class="form-group form-group-sm col-xs-6 col-sm-4 col-md-4 col-lg-3">
        <label class="control-label">from:</label>
        <input id="from" class="form-control form-control-sm" placeholder="from" />
    </div>
    <div class="form-group form-group-sm col-xs-6 col-sm-4 col-md-4 col-lg-3">
        <label class="control-label">until:</label>
        <input id="to" class="form-control form-control-sm" placeholder="until"/>
    </div>
</div>
<table id="datatable" class="table table-hover datatable">
    <thead>
        <tr class="filters"> 
            <td></td> 
            <td></td> 
            <td class="form-group form-group-sm"><input type="text" class="form-control" placeholder="name"/></td>
            <td class="form-group form-group-sm"><select id="category_filter" class="form-control" placeholder="category"></select></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>              
            <th class="no-order all"><!-- order --></th>                
            <th class="no-order coverImg all" data-priority="2"><!-- cover --></th>
            <th data-priority="3" class="all">Name</th>
            <th data-priority="4" class="min-tablet">Category</th> 
            <th data-priority="5" class="min-tablet ymd">Start</th> 
            <th data-priority="5" class="min-tablet ymd">End</th> 
            <th data-priority="6" class="no-order op edit min-tablet-l"></th> 
            <th data-priority="6" class="no-order op setStatus min-tablet-l"></th> 
            <th data-priority="6" class="no-order op delete min-tablet-l"></th>
            <th data-priority="6" class="no-order op gallery min-tablet-l"></th>
        </tr>       
    </thead>

    <tfoot>
        <tr>              
            <th></th>           
            <th></th>            
            <th>Name</th> 
            <th>Category</th> 
            <th>Start</th>
            <th>End</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>




