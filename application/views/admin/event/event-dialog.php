<div class="wide_dialog">
    <form action="<?php echo base_url(); ?>admin/events/validateEventForm" id="event_form" class="form" method="post" role="form" novalidate>

        <div class="group">
            <div class="form-group form-group-sm has-feedback no-toggle">
                <label class="control-label" for="name">Name</label>
                <input class="form-control no-disable" id="name" name="name" placeholder="name" required>
                <span class="glyphicon form-control-feedback"></span>
            </div>
        </div>        

        <div class="group">            
            <div class="form-group form-group-sm has-feedback no-toggle">
                <label class="control-label" for="category_id">Category</label>
                <select class="form-control no-disable" id="category_id" name="category_id"> 
                    <?php $this->load->view($category_options); ?>                   
                </select>
                <span class="glyphicon form-control-feedback"></span>
            </div>       
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="start_date">Start</label>
                            <input type="date" class="form-control no-disable datepicker" id="start_date" name="start_date" placeholder="date" required>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    </div>
                </div>
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="start_time">From</label>
                            <input type="text" class="form-control timepicker" id="start_time" name="start_time" placeholder="time" disabled>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="end_date">End</label>
                            <input type="date" class="form-control datepicker" id="end_date" name="end_date" placeholder="date" disabled>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="end_time">Until</label>
                            <input type="text" class="form-control timepicker" id="end_time" name="end_time" placeholder="time" disabled>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">              
                <div class="row group">                    
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="venue">Venue</label>
                            <input type="text" class="form-control no-disable" id="venue" name="venue" placeholder="venue" required />
                            <span class="glyphicon form-control-feedback"></span>
                        </div>  
                    </div>                      
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label" for="address">Address</label>
                            <input type="text" class="form-control no-disable" id="address" name="address" placeholder="address" required />
                            <span class="glyphicon form-control-feedback"></span>
                        </div>   
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

                    </div>
                </div>               
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">              
                <div class="row group">                    
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="address">URL 1</label>
                            <input type="url" class="form-control" id="url1" name="url1" placeholder="URL" disabled />
                            <span class="glyphicon form-control-feedback"></span>
                        </div>  
                    </div>                      
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row group">
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                        <div class="form-group form-group-sm has-feedback">
                            <label class="control-label dim" for="url2">URL 2</label>
                            <input type="url" class="form-control" id="url2" name="url2" placeholder="URL" disabled />
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                    </div>  
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
                    </div>
                </div>               
            </div>
        </div>

        <div class="row group"> 
            <div class="col-xs-10 col-sm-11 col-md-11 col-lg-11">
                <div class="form-group form-group-sm has-feedback">
                    <label class="control-label dim" for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" placeholder="description" disabled></textarea>
                    <span class="glyphicon form-control-feedback"></span>
                </div> 
            </div>
            <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
                <span class="field_lock"><a href="#" class="toggle_field"><i class="fa fa-lock"></i></a></span>
            </div>   
        </div>    

        <div class="row">
            <?php $this->load->view('admin/settings/language-list'); ?>
        </div>

        <input type="hidden" id="id" name="id" value="">       
        <input type="hidden" id="lang_id" name="lang_id" value="<?= $this->session->userdata('language')->id; ?>">       
        <input type="hidden" id="op" name="op" value="">   

        <div class="dialog_controls">
            <button type="button" class="btn btn-default close-dialog">Cancel</button>
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </form>
</div>