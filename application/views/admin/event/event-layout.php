<section class="no-spacing">
    <div class="subheader mb-md">
        <h2>Events</h2>           
    </div>

    <?php $this->load->view('admin/event/controls'); ?>

    <div>
        <?php $this->load->view($event_array); ?>
    </div>
</section>
