<option value="" selected>-- not specified --</option>
<?php if (!empty($categories)): ?>
    <?php foreach ($categories as $category) : ?>        
        <option value="<?= $category['id']; ?>">
            <?= !empty($category['category']) ? $category['category'] : "[no translation]"; ?>
        </option>        
    <?php endforeach; ?>
<?php endif; ?>

