<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="menu")
 */
class Menu {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $parent;    

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;
    
    /**
     * @Column(type="string", length=300, nullable=false)
     */
    protected $url;    

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;        

    /**
     * @OneToMany(targetEntity="MenuTr", mappedBy="menu", cascade={"remove"})     
     */
    protected $translations;

    public function __construct() {
        $this->status = 0;
        $this->create_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getParent() {
        return $this->parent;
    }

    function getStatus() {
        return $this->status;
    }

    function getWeight() {
        return $this->weight;
    }

    function getUrl() {
        return $this->url;
    }  

    function getCreate_date() {
        return $this->create_date;
    }

    function getTranslations() {
        return $this->translations;
    }

    function setParent($parent) {
        $this->parent = $parent;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }

}
