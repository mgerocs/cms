<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="metadata_tr")
 */
class MetaTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=1000, nullable=true)
     */
    protected $keywords;

    /**
     * @Column(type="string", length=1000, nullable=true)
     */
    protected $description;

    /**
     * @Column(type="string", length=100, nullable=true)
     */
    protected $title;

    /**
     * @ManyToOne(targetEntity="Meta", inversedBy="translations")
     * @JoinColumn(name="meta_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $meta;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    function getKeywords() {
        return $this->keywords;
    }

    function getDescription() {
        return $this->description;
    }

    function getTitle() {
        return $this->title;
    }

    function getLanguage() {
        return $this->language;
    }

    function getMeta() {
        return $this->meta;
    }

    function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setMeta($meta) {
        $this->meta = $meta;
    }

}
