<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="events")
 */
class Event {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $venue;

    /**
     * @Column(type="string", length=300, nullable=false)
     */
    protected $address;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $start_date;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $end_date;

    /**
     * @Column(type="string", length=8, nullable=true)
     */
    protected $start_time;

    /**
     * @Column(type="string", length=8, nullable=true)
     */
    protected $end_time;

    /**
     * @Column(type="string", length=300, nullable=true)
     */
    protected $url1;

    /**
     * @Column(type="string", length=300, nullable=true)
     */
    protected $url2;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ManyToOne(targetEntity="EventCategory", inversedBy="event")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @OneToOne(targetEntity="Gallery", cascade={"remove"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    /**
     * @OneToMany(targetEntity="EventTr", mappedBy="event", cascade={"remove"})     
     */
    protected $translations;

    public function __construct() {
        $this->status = 1;
        $this->create_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getVenue() {
        return $this->venue;
    }

    function getAddress() {
        return $this->address;
    }

    function getStatus() {
        return $this->status;
    }

    function getWeight() {
        return $this->weight;
    }

    function getStart_date() {
        return $this->start_date;
    }

    function getEnd_date() {
        return $this->end_date;
    }

    function getStart_time() {
        return $this->start_time;
    }

    function getEnd_time() {
        return $this->end_time;
    }

    function getUrl1() {
        return $this->url1;
    }

    function getUrl2() {
        return $this->url2;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getCategory() {
        return $this->category;
    }

    function getGallery() {
        return $this->gallery;
    }

    function getTranslations() {
        return $this->translations;
    }

    function setVenue($venue) {
        $this->venue = $venue;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setStart_date($start_date) {
        $this->start_date = new \DateTime($start_date);
    }

    function setEnd_date($end_date) {
        if ($end_date == NULL) {
            $this->end_date = $end_date;
        } else {
            $this->end_date = new \DateTime($end_date);
        }
    }

    function setStart_time($start_time) {
        $this->start_time = $start_time;
    }

    function setEnd_time($end_time) {
        $this->end_time = $end_time;
    }

    function setUrl1($url1) {
        $this->url1 = $url1;
    }

    function setUrl2($url2) {
        $this->url2 = $url2;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }

}
