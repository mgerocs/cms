<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="galleries_tr")
 */
class GalleryTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $name;
    
    /**
     * @Column(type="string", length=1000, nullable=true)
     */ 
    protected $description;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;
    
    /**
     * @ManyToOne(targetEntity="Gallery", inversedBy="translations")
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    public function __construct() {
        
    }

    function getName() {
        return $this->name;
    }
    
    function getDescription() {
        return $this->description;
    }

    function getLanguage() {
        return $this->language;
    }

    function getGallery() {
        return $this->gallery;
    }

    function setName($name) {
        $this->name = $name;
    }
    
    function setDescription($description) {
        $this->description = $description;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

}
