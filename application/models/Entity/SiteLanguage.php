<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="site_languages")
 */
class SiteLanguage {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @OneToOne(targetEntity="Language")
     * @JoinColumn(name="lang_id", referencedColumnName="id")
     */
    protected $language;  

    /**
     * @Column(type="boolean")
     */
    protected $def;
  

    /**
     * @Column(type="integer", nullable=false, options={"unsigned":true})     
     */
    protected $weight;

    public function __construct() {
        $this->def = 0;        
    }
    
    function getId() {
        return $this->id;
    }

    function getLanguage() {
        return $this->language;
    }   

    function getDefault() {
        return $this->def;
    }

    function getWeight() {
        return $this->weight;
    }
    
    function setLanguage($language) {
        $this->language = $language;
    } 

    function setDefault($default) {
        $this->def = $default;
    }
   
    function setWeight($weight) {
        $this->weight = $weight;
    }

}
