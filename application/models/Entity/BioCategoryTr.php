<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="bio_categories_tr")
 */
class BioCategoryTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $category;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="BioCategory", inversedBy="translations")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $bioCategory;

    public function __construct() {
        
    }

    function getCategory() {
        return $this->type;
    }

    function getLanguage() {
        return $this->language;
    }

    function getBioCategory() {
        return $this->bioCategory;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setBioCategory($bioCategory) {
        $this->bioCategory = $bioCategory;
    }

}
