<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="biography")
 */
class Biography {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", nullable=false)
     */
    protected $start;

    /**
     * @Column(type="string", nullable=true)
     */
    protected $end;

    /**
     * @Column(type="boolean")
     */
    protected $in_progress;

    /**
     * @Column(type="string", length=500, nullable=true)
     */
    protected $url;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ManyToOne(targetEntity="BioCategory", inversedBy="biography")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @OneToMany(targetEntity="BioTr", mappedBy="biography", cascade={"remove"})     
     */
    protected $translations;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;

    public function __construct() {
        $this->status = 1;
        $this->in_progress = 0;
        $this->create_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getStart() {
        return $this->start;
    }

    function getEnd() {
        return $this->end;
    }

    function getIn_progress() {
        return $this->in_progress;
    }

    function getUrl() {
        return $this->url;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getCategory() {
        return $this->category;
    }

    function getStatus() {
        return $this->status;
    }

    function getWeight() {
        return $this->weight;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setStart($start) {
        $this->start = $start;
    }

    function setEnd($end) {
        $this->end = $end;
    }

    function setIn_progress($in_progress) {
        $this->in_progress = $in_progress;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

}
