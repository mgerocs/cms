<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="recovery")
 */
class Recovery {

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="string", length=32, nullable=false)
     */
    protected $ip_address;

    /**
     * @Column(type="string", length=32, nullable=false)
     */
    protected $token;

    /**
     * @Column(type="boolean")
     */
    protected $used;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    public function __construct() {
        $this->used = 0;
        $this->create_date = new \DateTime("now");
    }

    function getEmail() {
        return $this->email;
    }

    function getIp_address() {
        return $this->ip_address;
    }

    function getToken() {
        return $this->token;
    }

    function getUsed() {
        return $this->used;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setIp_address($ip_address) {
        $this->ip_address = $ip_address;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setUsed($used) {
        $this->used = $used;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

}
