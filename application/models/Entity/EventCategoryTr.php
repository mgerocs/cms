<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="event_categories_tr")
 */
class EventCategoryTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $category;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="EventCategory", inversedBy="translations")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $eventCategory;

    public function __construct() {
        
    }

    function getCategory() {
        return $this->type;
    }

    function getLanguage() {
        return $this->language;
    }

    function getEventCategory() {
        return $this->eventCategory;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setEventCategory($eventCategory) {
        $this->eventCategory = $eventCategory;
    }

}
