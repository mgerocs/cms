<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="gallery_categories_tr")
 */
class GalleryCategoryTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $category;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="GalleryCategory", inversedBy="translations")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $galleryCategory;

    public function __construct() {
        
    }

    function getCategory() {
        return $this->type;
    }

    function getLanguage() {
        return $this->language;
    }

    function getGalleryCategory() {
        return $this->galleryCategory;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setGalleryCategory($galleryCategory) {
        $this->galleryCategory = $galleryCategory;
    }

}
