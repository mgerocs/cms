<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="users")
 */
class User {

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $lastname;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $firstname;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $password;

    /**
     * @Column(type="string", length=100, unique=true, nullable=false)
     */
    protected $email;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $city;

    /**
     * @Column(type="string", length=10, nullable=true)
     */
    protected $postal_code;

    /**
     * @Column(type="string", length=300, nullable=true)
     */
    protected $address;

    /**
     * @Column(type="string", length=20, nullable=true)
     */
    protected $phone1;

    /**
     * @Column(type="string", length=20, nullable=true)
     */
    protected $phone2;
    
    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $portrait;

    /**
     * @ManyToOne(targetEntity="Group", inversedBy="user")
     * @JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $group;

    function getLastname() {
        return $this->lastname;
    }

    function getFirstname() {
        return $this->firstname;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    function getCity() {
        return $this->city;
    }

    function getPostal_code() {
        return $this->postal_code;
    }

    function getAddress() {
        return $this->address;
    }
    
    function getPhone1() {
        return $this->phone1;
    }

    function getPhone2() {
        return $this->phone2;
    }
    
    function getPortrait() {
        return $this->portrait;
    }

    function getGroup() {
        return $this->group;
    }

    function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setPostal_code($postal_code) {
        $this->postal_code = $postal_code;
    }

    function setAddress($address) {
        $this->address = $address;
    }
    
    function setPhone1($phone1) {
        $this->phone1 = $phone1;
    }

    function setPhone2($phone2) {
        $this->phone2 = $phone2;
    }
    
    function setPortrait($portrait) {
        $this->portrait = $portrait;
    }

    function setGroup($group) {
        $this->group = $group;
    }

}
