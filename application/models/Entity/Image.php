<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="images")
 */
class Image {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=100, nullable=false)
     */
    protected $filename;

    /**
     * @Column(type="string", length=5, nullable=false)
     */
    protected $ext;

    /**
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     */
    protected $size;

    /**
     * @Column(type="boolean")
     */
    protected $cover;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;  

    /**
     * @ManyToOne(targetEntity="Gallery", inversedBy="images")
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    /**
     * @OneToMany(targetEntity="ImageTr", mappedBy="image", cascade={"remove"})     
     */
    protected $translations;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    public function __construct() {
        $this->cover = 0;
        $this->create_date = new \DateTime("now");
    }

    function getFilename() {
        return $this->filename;
    }

    function getExt() {
        return $this->ext;
    }

    function getSize() {
        return $this->size;
    }

    function getCover() {
        return $this->cover;
    }

    function getWeight() {
        return $this->weight;
    }

    function getGallery() {
        return $this->gallery;
    }

    function getTranslations() {
        return $this->translations;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function setFilename($filename) {
        $this->filename = $filename;
    }

    function setExt($ext) {
        $this->ext = $ext;
    }

    function setSize($size) {
        $this->size = $size;
    }

    function setCover($cover) {
        $this->cover = $cover;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

}
