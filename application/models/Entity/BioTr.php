<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="bio_tr")
 */
class BioTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */    
    protected $id;

    /**
     * @Column(type="string", length=500, nullable=false)
     */
    protected $field1;
    
    /**
     * @Column(type="string", length=500, nullable=true)
     */
    protected $field2;
    
    /**
     * @Column(type="string", length=500, nullable=true)
     */
    protected $field3;
    
    /**
     * @Column(type="string", length=500, nullable=true)
     */
    protected $field4;  

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;
    
    /**
     * @ManyToOne(targetEntity="Biography", inversedBy="translations")
     * @JoinColumn(name="bio_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $biography;

    public function __construct() {
        
    }
    
    function getId() {
        return $this->id;
    }

    function getField1() {
        return $this->field1;
    }

    function getField2() {
        return $this->field2;
    }

    function getField3() {
        return $this->field3;
    }

    function getField4() {
        return $this->field4;
    }   

    function getLanguage() {
        return $this->language;
    }

    function getBiography() {
        return $this->biography;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setField1($field1) {
        $this->field1 = $field1;
    }

    function setField2($field2) {
        $this->field2 = $field2;
    }

    function setField3($field3) {
        $this->field3 = $field3;
    }

    function setField4($field4) {
        $this->field4 = $field4;
    }
   
    function setLanguage($language) {
        $this->language = $language;
    }

    function setBiography($biography) {
        $this->biography = $biography;
    }    

}
