<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="metadata")
 */
class Meta {

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;    

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $logo;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $favicon;    

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $author;    

    /**
     * @OneToMany(targetEntity="MetaTr", mappedBy="meta", cascade={"remove"})     
     */
    protected $translations;
    
    public function __construct() {
       
    }   
   
    function getLogo() {
        return $this->logo;
    }

    function getFavicon() {
        return $this->favicon;
    }

    function getAuthor() {
        return $this->author;
    }

    function getTranslations() {
        return $this->translations;
    }

    function setLogo($logo) {
        $this->logo = $logo;
    }

    function setFavicon($favicon) {
        $this->favicon = $favicon;
    }

    function setAuthor($author) {
        $this->author = $author;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }    

}
