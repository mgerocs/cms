<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="images_tr")
 */
class ImageTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $name;
    
    /**
     * @Column(type="string", length=1000, nullable=true)
     */ 
    protected $description;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="Image", inversedBy="translations")
     * @JoinColumn(name="image_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $image;

    function getName() {
        return $this->name;
    }
    
    function getDescription() {
        return $this->description;
    }

    function getLanguage() {
        return $this->language;
    }

    function getImage() {
        return $this->image;
    }

    function setName($name) {
        $this->name = $name;
    }
    
    function setDescription($description) {
        $this->description = $description;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setImage($image) {
        $this->image = $image;
    } 

}
