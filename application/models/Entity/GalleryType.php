<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="gallery_types")
 */
class GalleryType {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=20, unique=true, nullable=false)
     */
    protected $type;

    public function __construct() {
        
    }

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }

}
