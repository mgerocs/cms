<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="projects")
 */
class Project {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ManyToOne(targetEntity="ProjectCategory", inversedBy="project")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @OneToOne(targetEntity="Gallery", cascade={"remove"})
     * @JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $gallery;

    /**
     * @OneToMany(targetEntity="ProjectTr", mappedBy="project", cascade={"remove"})     
     */
    protected $translations;

    public function __construct() {
        $this->status = 1;
        $this->create_date = new \DateTime("now");
    }

    function getStatus() {
        return $this->status;
    }

    function getWeight() {
        return $this->weight;
    }

    function getDate() {
        return $this->date;        
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getCategory() {
        return $this->category;
    }

    function getGallery() {
        return $this->gallery;
    }

    function getTranslations() {
        return $this->translations;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setDate($date) {
        $this->date = new \DateTime($date);
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setGallery($gallery) {
        $this->gallery = $gallery;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }

}
