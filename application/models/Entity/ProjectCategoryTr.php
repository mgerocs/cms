<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="project_categories_tr")
 */
class ProjectCategoryTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $category;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="ProjectCategory", inversedBy="translations")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $projectCategory;

    public function __construct() {
        
    }

    function getCategory() {
        return $this->type;
    }

    function getLanguage() {
        return $this->language;
    }

    function getProjectCategory() {
        return $this->projectCategory;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setProjectCategory($projectCategory) {
        $this->projectCategory = $projectCategory;
    }

}
