<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="groups")
 */
class Group {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=10, nullable=false)
     */
    protected $group;

    /**
     * @OneToMany(targetEntity="User", mappedBy="group")     
     */
    protected $user;

    public function __construct() {
        
    }

    function getGroup() {
        return $this->group;
    }

    function getUser() {
        return $this->user;
    }

    function setGroup($group) {
        $this->group = $group;
    }

    function setUser($user) {
        $this->user = $user;
    }

}
