<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="event_categories")
 */
class EventCategory {   
        
    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;   
    
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;
    
    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */   
    protected $weight;
    
    /**
     * @OneToMany(targetEntity="Event", mappedBy="category")     
     */    
    protected $event;
    
    /**
     * @OneToMany(targetEntity="EventCategoryTr", mappedBy="eventCategory", cascade={"remove"})     
     */
    protected $translations;
    
    public function __construct()
    {
       $this->create_date = new \DateTime("now");
    }    
    
    function getCreate_date() {
        return $this->create_date;
    }
    
    function getWeight() {
        return $this->weight;
    }
    
    function getEvent() {
        return $this->event;
    }

    function getTranslations() {
        return $this->translations;
    }
    
    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }    
    
    function setWeight($weight) {
        $this->weight = $weight;
    }
    
    function setEvent($event) {
        $this->event = $event;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }
    
}