<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="gallery_categories")
 */
class GalleryCategory {   
        
    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;   
    
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;
    
    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */   
    protected $weight;
    
    /**
     * @OneToMany(targetEntity="Gallery", mappedBy="category")     
     */    
    protected $gallery;
    
    /**
     * @OneToMany(targetEntity="GalleryCategoryTr", mappedBy="galleryCategory", cascade={"remove"})     
     */
    protected $translations;
    
    public function __construct()
    {
       $this->create_date = new \DateTime("now");
    }    
    
    function getCreate_date() {
        return $this->create_date;
    }
    
    function getWeight() {
        return $this->weight;
    }
    
    function getProject() {
        return $this->project;
    }

    function getTranslations() {
        return $this->translations;
    }
    
    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }    
    
    function setWeight($weight) {
        $this->weight = $weight;
    }
    
    function setProject($project) {
        $this->project = $project;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }
    
}