<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="galleries")
 */
class Gallery {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $create_date;

    /**
     * @ManyToOne(targetEntity="GalleryType")
     * @JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="GalleryCategory", inversedBy="gallery")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    protected $category;

    /**
     * @OneToMany(targetEntity="GalleryTr", mappedBy="gallery", cascade={"remove"})     
     */
    protected $translations;

    /**
     * @Column(type="boolean")
     */
    protected $status;

    /**
     * @Column(type="integer", nullable=true, options={"unsigned":true})
     */
    protected $weight;

    /**
     * @OneToMany(targetEntity="Image", mappedBy="gallery")     
     */
    protected $images;

    public function __construct() {
        $this->status = 1;
        $this->create_date = new \DateTime("now");
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getCreate_date() {
        return $this->create_date;
    }

    function getType() {
        return $this->type;
    }

    function getCategory() {
        return $this->category;
    }

    function getTranslations() {
        return $this->translations;
    }

    function getStatus() {
        return $this->status;
    }

    function getWeight() {
        return $this->weight;
    }

    function getImages() {
        return $this->images;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setCreate_date($create_date) {
        $this->create_date = $create_date;
    }

    function setTranslations($translations) {
        $this->translations = $translations;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function setImages($images) {
        $this->images = $images;
    }

}
