<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="events_tr")
 */
class EventTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $name;
    
    /**
     * @Column(type="string", length=1000, nullable=true)
     */ 
    protected $description;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="Event", inversedBy="translations")
     * @JoinColumn(name="event_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $event;

    function getName() {
        return $this->name;
    }
    
    function getDescription() {
        return $this->description;
    }

    function getLanguage() {
        return $this->language;
    }

    function getEvent() {
        return $this->event;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setEvent($event) {
        $this->event = $event;
    }    
     
    function setDescription($description) {
        $this->description = $description;
    }

}
