<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="languages")
 */
class Language {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=2, nullable=false)
     */
    protected $code;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $language;

    /**
     * @Column(type="string", length=50, nullable=true)
     */
    protected $label;    

    public function __construct() {

    }

    function getCode() {
        return $this->code;
    }

    function getLanguage() {
        return $this->language;
    }

    function getLabel() {
        return $this->label;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setLabel($label) {
        $this->label = $label;
    }   

}
