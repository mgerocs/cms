<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="projects_tr")
 */
class ProjectTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $name;
    
    /**
     * @Column(type="string", length=1000, nullable=true)
     */ 
    protected $description;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="Project", inversedBy="translations")
     * @JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $project;

    function getName() {
        return $this->name;
    }
    
    function getDescription() {
        return $this->description;
    }

    function getLanguage() {
        return $this->language;
    }

    function getProject() {
        return $this->project;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setProject($project) {
        $this->project = $project;
    }    
     
    function setDescription($description) {
        $this->description = $description;
    }

}
