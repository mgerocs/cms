<?php

namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="menu_tr")
 */
class MenuTr {

    /**
     * @Id
     * @Column(type="integer", nullable=false, options={"unsigned":true})
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="string", length=50, nullable=false)
     */
    protected $label;

    /**
     * @ManyToOne(targetEntity="SiteLanguage")
     * @JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $language;

    /**
     * @ManyToOne(targetEntity="Menu", inversedBy="translations")
     * @JoinColumn(name="menu_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $menu;

    function getLabel() {
        return $this->label;
    }

    function getLanguage() {
        return $this->language;
    }

    function getMenu() {
        return $this->menu;
    }

    function setLabel($label) {
        $this->label = $label;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setMenu($menu) {
        $this->menu = $menu;
    }

}
