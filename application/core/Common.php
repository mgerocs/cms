<?php

class Common extends MY_Controller {

    // Doctrine EntityManager
    public $em;

    function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('admin/identity');
        } else {
            return true;
        }
    }

    function getLanguages($lang_id = null) {
        try {
            $repo = $this->em->getRepository('Entity\SiteLanguage');
            $qb = $repo->createQueryBuilder('sl');

            $qb->select('sl.id AS id', 'sl.def AS def', 'sl.weight AS weight', 'l.language AS language', 'l.label AS label', 'l.code AS code');

            if ($lang_id) {
                $qb->where('sl.id = :id');
                $qb->setParameter('id', $lang_id);
            }
            $qb->leftJoin('sl.language', 'l');
            $qb->orderBy('sl.weight', 'ASC');

            $languages = $qb->getQuery()->getArrayResult();

            return $languages;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getMaxWeight($entity) {
        try {
            $weight = 0;

            $repo = $this->em->getRepository($entity);
            $qb = $repo->createQueryBuilder('e');
            $qb->select('count(e.id)');

            if ($qb->getQuery()->getSingleScalarResult() > 0) {
                $qb->select('MAX(e.weight)');
                $max = $qb->getQuery()->getSingleScalarResult();
                $weight = $max + 1;
            } else {
                $weight = 1;
            }

            return $weight;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorderEntity($data, $entity) {
        try {
            $ids = $data['ids'];
            $weights = $data['weights'];

            if (empty($ids) || empty($weights)) {
                throw new Exception("No data posted.");
            }

            for ($i = 0; $i < count($ids); $i++) {
                $item = $this->em->getRepository($entity)->find($ids[$i]);
                $item->setWeight($weights[$i]);
            }
            $this->em->flush();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getDefaultLang() {
        try {
            $repo = $this->em->getRepository('Entity\SiteLanguage');
            $qb = $repo->createQueryBuilder('sl');
            $qb->select('sl.id AS id', 'l.language AS language', 'l.label AS label', 'l.code AS code');
            $qb->leftJoin('sl.language', 'l');
            $qb->where('sl.def = 1');

            $default_lang = $qb->getQuery()->getOneOrNullResult();

            if (empty($default_lang)) {
                throw new Exception("Default language not found.");
            }

            $default_lang = (object) $default_lang;

            return $default_lang;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setSiteLang($language = null) {
        try {
            if (empty($language)) {
                $language = $this->getDefaultLang();
            }
            $this->session->set_userdata('default_lang', $language);
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getMeta($language = null) {
        try {
            
            if(empty($language)) {
                $language = $this->getDefaultLang()->id;
            }            

            $repo = $this->em->getRepository('Entity\Meta');
            $qb = $repo->createQueryBuilder('m');

            $qb->select('m.id', 'm.logo', 'm.favicon', 'm.author', 'mtr.title', 'mtr.keywords', 'mtr.description');
            $qb->leftJoin('m.translations', 'mtr', 'WITH', 'IDENTITY (mtr.language) = :lang_id OR mtr.id IS NULL');
            $qb->setParameter('lang_id', $language);

            $meta = $qb->getQuery()->getOneOrNullResult();
//            if (empty($meta)) {
//                throw new Exception("Meta not found.");
//            }

            $meta = (object) $meta;

            return $meta;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validation($ruleset) {
        if ($this->form_validation->run($ruleset) == FALSE) {
            $status = false;
        } else {
            $status = true;
        }
        return $status;
    }

    function sendErrorMessage($message) {
        echo json_encode(array('status' => false, 'error' => $message));
        exit();
    }

}
