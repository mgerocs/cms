<?php

$config = array(
    'login' => array(
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => 'trim|required|valid_email'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    ),
    'change_pwd' => array(
        array(
            'field' => 'old_password',
            'label' => 'Old password',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'ch_password',
            'label' => 'New password',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'ch_password_conf',
            'label' => 'Confirm password',
            'rules' => 'trim|required'
        )
    ),
    'language' => array(
        array(
            'field' => 'language',
            'label' => 'Language',
            'rules' => 'trim|required'
        )
    ),
    'pwdrecovery' => array(
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => 'trim|required|valid_email'
        )
    ),
    'newpwd' => array(
        array(
            'field' => 'new_password',
            'label' => 'Password',
            'rules' => 'trim|required|matches[new_password_conf]'
        ),
        array(
            'field' => 'new_password_conf',
            'label' => 'Jelszó újra',
            'rules' => 'trim|required'
        )
    ),
    'menu' => array(
        array(
            'field' => 'label',
            'label' => 'Label',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'url',
            'label' => 'URL',
            'rules' => 'trim|required'
        )
    )
);
