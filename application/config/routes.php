<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'IndexController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//CUSTOM ROUTES

//$route['identity'] = 'Identity_controller';
//$route['identity/(:any)'] = 'Identity_controller/$1';
//$route['identity/(:any)/(:any)'] = 'Identity_controller/$1/$2';
//$route['calendar'] = 'Calendar_controller';
//$route['calendar/(:any)'] = 'Calendar_controller/$1';
//$route['calendar/(:any)/(:num)'] = 'Calendar_controller/$1/$2';
//
//$route['admin'] = 'admin/Identity_controller_admin';
//$route['admin/identity'] = 'admin/Identity_controller_admin';
//$route['admin/identity/(:any)'] = 'admin/Identity_controller_admin/$1';
//$route['admin/identity/(:any)/(:any)'] = 'admin/Identity_controller_admin/$1/$2';
//$route['admin/calendar'] = 'admin/Calendar_controller_admin';
//$route['admin/calendar/(:any)'] = 'admin/Calendar_controller_admin/$1';
//$route['admin/calendar/(:any)/(:num)'] = 'admin/Calendar_controller_admin/$1/$2';
//$route['admin/service/(:any)'] = 'admin/Service_controller_admin/$1';
//$route['admin/service/(:any)/(:num)'] = 'admin/Service_controller_admin/$1/$2';

/****** WEBSITE ******/
$route['images/(:any)'] = 'IndexController/images/$1';
$route['setlang/(:any)'] = 'IndexController/setSiteLang/$1';

/****** ADMIN ******/
$route['admin'] = 'admin/IdentityController/loginform';
$route['admin/identity'] = 'admin/IdentityController/loginform';
$route['admin/identity/(:any)'] = 'admin/IdentityController/$1';
$route['admin/identity/(:any)/(:any)'] = 'admin/IdentityController/$1/$2';
//$route['admin/identity/(:any)/(:any)/(:any)'] = 'admin/IdentityController/$1/$2/$3';

$route['admin/users'] = 'admin/UserController/users';
$route['admin/users/(:any)'] = 'admin/UserController/$1';
$route['admin/users/(:any)/(:any)'] = 'admin/UserController/$1/$2';

$route['admin/settings'] = 'admin/SettingsController/settings';
$route['admin/settings/(:any)'] = 'admin/SettingsController/$1';
$route['admin/settings/(:any)/(:any)'] = 'admin/SettingsController/$1/$2';

$route['admin/dashboard'] = 'admin/ProjectController/projects';

$route['admin/events'] = 'admin/EventController/events';
$route['admin/events/(:any)'] = 'admin/EventController/$1';
$route['admin/events/(:any)/(:any)'] = 'admin/EventController/$1/$2';
$route['admin/events/(:any)/(:any)/(:any)'] = 'admin/EventController/$1/$2/$3';

$route['admin/projects'] = 'admin/ProjectController/projects';
$route['admin/projects/(:any)'] = 'admin/ProjectController/$1';
$route['admin/projects/(:any)/(:any)'] = 'admin/ProjectController/$1/$2';
$route['admin/projects/(:any)/(:any)/(:any)'] = 'admin/ProjectController/$1/$2/$3';

//$route['admin/galleries'] = 'admin/GalleryController/galleries';
$route['admin/galleries/(:any)'] = 'admin/GalleryController/$1';
$route['admin/galleries/(:any)/(:any)'] = 'admin/GalleryController/$1/$2';
$route['admin/galleries/(:any)/(:any)/(:any)'] = 'admin/GalleryController/$1/$2/$3';
$route['admin/galleries/(:any)/(:any)/(:any)/(:any)'] = 'admin/GalleryController/$1/$2/$3/$4';

$route['admin/biography'] = 'admin/BioController/biography';
$route['admin/biography/(:any)'] = 'admin/BioController/$1';
$route['admin/biography/(:any)/(:any)'] = 'admin/BioController/$1/$2';
$route['admin/biography/(:any)/(:any)/(:any)'] = 'admin/BioController/$1/$2/$3';

$route['admin/menu'] = 'admin/MenuController/menu';
$route['admin/menu/(:any)'] = 'admin/MenuController/$1';