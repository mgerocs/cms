<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class IndexController extends Common {

    private $folder;

    function __construct() {
        parent::__construct();
        $this->folder = "website/";
        $sitelang = $this->session->userdata('sitelang');
        if (!isset($sitelang)) {
            $sitelang['id'] = $this->getDefaultLang()->id;
            $sitelang['code'] = $this->getDefaultLang()->code;
            $this->session->set_userdata('sitelang', $sitelang);
        }
    }

    function index() {
        try {
            $sitelang = $this->session->userdata('sitelang');
            $this->lang->load($sitelang['code'] . "_lang", $sitelang['code']);
            $data['meta'] = $this->getMeta($sitelang['id']);
            $data['user'] = $this->getUserData(1);
//            $data['content'] = $this->folder . "sections";
//            $data['projects'] = $this->getProjects($sitelang['id']);
            $data['languages'] = $this->getLanguages();
            $data['sitelang'] = $this->session->userdata('sitelang');
            $this->load->view('website/_layout/default', $data);
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

//    function home() {
//        try {
//            $content = $this->folder . "gallery/slider";
//            $data['galleries'] = $this->getGalleries($this->getDefaultLang()->id);
//            $data['slides'] = $this->getImages("slider");
//            $this->load->view($content, $data);
//        } catch (Exception $ex) {
//            $this->sendErrorMessage($ex->getMessage());
//        }
//    }

    function images($gallery_id) {
        try {
            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            $content = $this->folder . "images";
            $data['images'] = $this->getImages($gallery_id);
            if ($this->input->is_ajax_request()) {
                $this->load->view($content, $data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getProjects($lang_id) {
        try {

            if (empty($lang_id)) {
                throw new Exception("Parameter is missing.");
            }

            $repo = $this->em->getRepository('Entity\Project');
            $qb = $repo->createQueryBuilder('p');

            $qb->select('p.id', 'IDENTITY (p.gallery) AS gallery_id', 'IDENTITY (p.category) AS category_id', 'p.date', 'p.weight AS pweight', 'pc.weight AS pcweight', 'ptr.name');
            $qb->leftJoin('p.translations', 'ptr', 'WITH', 'IDENTITY (ptr.language) = :lang_id OR ptr.id IS NULL');
            $qb->leftJoin('p.category', 'pc');
            $qb->leftJoin('pc.translations', 'pctr', 'WITH', 'IDENTITY (pctr.language) = :lang_id OR pctr.id IS NULL');
            $qb->andWhere('p.status = 1');
            $qb->setParameter('lang_id', $lang_id);
            $qb->addOrderBy('pcweight', 'ASC');
            $qb->addOrderBy('pweight', 'ASC');

            $projects = $qb->getQuery()->getArrayResult();

            return $projects;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getImages($gallery_id) {
        try {

            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }
            
            if (!is_numeric($gallery_id)) {
                //find GalleryType
                $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => "project"));
                if (empty($galleryType)) {
                    throw new Exception("GalleryType not found.");
                }
                $gallery_id = $galleryType->getId();                
            }

            $lang_id = $this->session->userdata('sitelang')['id'];

            $repo = $this->em->getRepository('Entity\Image');
            $qb = $repo->createQueryBuilder('i');

            $qb->select('i.id', 'i.filename', 'IDENTITY (i.gallery) AS gallery_id', 'i.cover', 'itr.name AS name', 'itr.description AS description', 'gt.type AS type');
            $qb->leftJoin('i.translations', 'itr', 'WITH', 'IDENTITY (itr.language) = :lang_id OR itr.id IS NULL');
            $qb->leftJoin('i.gallery', 'g');
            $qb->leftJoin('g.type', 'gt');
            $qb->where('i.gallery = :gallery_id');
            $qb->setParameter('gallery_id', $gallery_id);
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('i.weight', 'ASC');

            $images = $qb->getQuery()->getArrayResult();

            return $images;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getGalleryInfo($gallery_id) {
        try {

            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            $lang_id = $this->session->userdata('default_lang')->id;

            $repo = $this->em->getRepository('Entity\Gallery');
            $qb = $repo->createQueryBuilder('g');

            $qb->select('g.id', 'gtr.name', 'gt.id AS type_id', 'gt.type AS type');
            $qb->leftJoin('g.translations', 'gtr', 'WITH', 'IDENTITY (gtr.language) = :lang_id OR gtr.id IS NULL');
            $qb->leftJoin('g.type', 'gt');
            $qb->andWhere('g.id = :id');
            $qb->setParameter('id', $gallery_id);
            $qb->setParameter('lang_id', $lang_id);

            $gallery = $qb->getQuery()->getOneOrNullResult();
            if (empty($gallery)) {
                throw new Exception("Gallery not found.");
            }

            $gallery = (object) $gallery;

            if ($gallery->type == "slider") {
                $gallery->name = "Slider";
            }

            return $gallery;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getUserData($user_id) {
        try {
            if (empty($user_id)) {
                throw new Exception("Parameter is missing.");
            }

            $repo = $this->em->getRepository('Entity\User');
            $qb = $repo->createQueryBuilder('u');

            $qb->select('u.id', 'u.lastname', 'u.firstname', 'u.email', 'u.city', 'u.postal_code', 'u.address', 'u.phone1', 'u.phone2');
            $qb->where('u.id = :user_id');
            $qb->setParameter('user_id', $user_id);

            $user = $qb->getQuery()->getOneOrNullResult();

            if (empty($user)) {
                throw new Exception("User not found.");
            }

            $user = (object) $user;

            return $user;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getLanguages($lang_id = null) {
        try {
            $repo = $this->em->getRepository('Entity\SiteLanguage');
            $qb = $repo->createQueryBuilder('sl');

            $qb->select('sl.id AS id', 'sl.def AS def', 'sl.weight AS weight', 'l.language AS language', 'l.label AS label', 'l.code AS code');

            if ($lang_id) {
                $qb->where('sl.id = :id');
                $qb->setParameter('id', $lang_id);
            }
            $qb->leftJoin('sl.language', 'l');
            $qb->orderBy('sl.weight', 'ASC');

            $languages = $qb->getQuery()->getArrayResult();

            return $languages;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setSiteLang($language = null) {
        try {
            if (empty($language)) {
                $language = $this->getDefaultLang()->id;
            }

            $sitelang['id'] = $language;
            $sitelang['code'] = $this->getLanguages($language)[0]['code'];

            $this->session->set_userdata('sitelang', $sitelang);

            redirect();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
