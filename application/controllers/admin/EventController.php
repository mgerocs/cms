<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EventController extends Common {

    private $folder;

    function __construct() {
        parent::__construct();

        $this->folder = "admin/event/";
    }

    function events() {
        $this->is_logged_in();
        $content = $this->folder . "event-layout";
        $data['event_array'] = $this->folder . "event-array";
        $data['lang'] = $this->session->userdata('language')->id;
        $data['meta'] = $this->getMeta();
        if ($this->input->is_ajax_request()) {
            $this->load->view($content, $data);
        } else {
            $data['content'] = $content;
            $this->load->view('admin/_layout/default', $data);
        }
    }

    function eventDialog($langswitch = false) {
        $content = $this->folder . "event-dialog";
        $data['categories'] = $this->getEventCategories($this->session->userdata('language')->id);
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['category_options'] = $this->folder . "category-options";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryDialog($langswitch = false) {
        $data['categories'] = $this->getEventCategories($this->session->userdata('language')->id);
        $data['categoriesofevents'] = $this->getCategoriesOfEvents();
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $content = $this->folder . "category-dialog";
        $data['category_list'] = $this->folder . "category-list";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryList($lang_id) {
        $data['categories'] = $this->getEventCategories($lang_id);
        $data['categoriesofevents'] = $this->getCategoriesOfEvents();
        $data['languages'] = $this->getLanguages();
        $content = $this->folder . "category-list";
        $this->load->view($content, $data);
    }

    function categoryOptions($lang_id) {
        $data['categories'] = $this->getEventCategories($lang_id);
        $content = $this->folder . "category-options";
        $this->load->view($content, $data);
    }

    function validateEventForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('venue', 'Venue', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_rules('start_date', 'Start', 'trim|required');

            (!empty($this->input->post('end_date'))) ? $this->form_validation->set_rules('end_date', 'End', 'required') : $data['end_date'] = NULL;
            (!empty($this->input->post('start_time'))) ? $this->form_validation->set_rules('start_time', 'From', 'required') : $data['start_time'] = NULL;
            (!empty($this->input->post('end_time'))) ? $this->form_validation->set_rules('end_time', 'Until', 'required') : $data['end_time'] = NULL;
            (!empty($this->input->post('url1'))) ? $this->form_validation->set_rules('url1', 'URL 1', 'required') : $data['url1'] = NULL;
            (!empty($this->input->post('url2'))) ? $this->form_validation->set_rules('url2', 'URL 2', 'required') : $data['url2'] = NULL;   
            (!empty($this->input->post('description'))) ? $this->form_validation->set_rules('description', 'Description', 'required') : $data['description'] = NULL;   

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }
 
            if (!empty($data['end_date']) && new DateTime($data['end_date'] . " 00:00:00") < new DateTime($data['start_date'] . " 00:00:00")) {
                $error = 'The end date cannot be earlier than the start date.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "#end_date"));
                exit();
            }
            
            if ($data['op'] == "new") {
                $this->insertEvent($data);
            } else {
                $this->updateEvent($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateCategoryForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('category', 'Category', 'trim|required');

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertEventCategory($data);
            } else {
                $this->updateEventCategory($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertEvent($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            //find GalleryType
            $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => "event"));
            if (empty($galleryType)) {
                throw new Exception("GalleryType not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            //$weight = $this->getMaxWeight('Entity\Event');

            //insert Gallery
            $gallery = new Entity\Gallery;
            $gallery->setType($galleryType);
            $this->em->persist($gallery);
            $this->em->flush();

            //insert GalleryTr
            $galleryTr = new Entity\GalleryTr;
            $galleryTr->setName($data['name']);
            $galleryTr->setGallery($gallery);
            $galleryTr->setLanguage($language);
            $this->em->persist($galleryTr);
            $this->em->flush();

            //find EventCategory
            $eventCategory = $this->em->getRepository('Entity\EventCategory')->find($data['category_id']);

            //insert Event
            $event = new Entity\Event;
            $event->setGallery($gallery);
            $event->setCategory($eventCategory);
            //$event->setWeight($weight);
            $event->setStart_date($data['start_date']);
            
            $start_date = !empty($data['start_time']) ? $data['start_date'] . " " . $data['start_time'] . ":00" : $data['start_date'] . " 00:00:00";            
            $event->setStart_date($start_date);
            $event->setStart_time($data['start_time']);
            
            if(!empty($data['end_date'])) {
                $end_date = !empty($data['end_time']) ? $data['end_date'] . " " . $data['end_time'] . ":00" : $data['end_date'] . " 00:00:00";
            }
            else {
                $end_date = NULL;
            }

            $event->setEnd_date($end_date);
            $event->setEnd_time($data['end_time']);
            
            $event->setVenue($data['venue']);
            $event->setAddress($data['address']);
            $event->setUrl1($data['url1']);
            $event->setUrl2($data['url2']);         
            $this->em->persist($event);
            $this->em->flush();

            //insert EventTr
            $eventTr = new Entity\EventTr;
            $eventTr->setName($data['name']);
            $eventTr->setDescription($data['description']);
            $eventTr->setEvent($event);
            $eventTr->setLanguage($language);
            $this->em->persist($eventTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "New event created.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateEvent($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $event = $this->em->getRepository('Entity\Event')->find($data['id']);
            if (!$event) {
                throw new Exception("Event not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($event->getGallery());
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $galleryTr = $this->em->getRepository('Entity\GalleryTr')->findOneBy(array('gallery' => $gallery, 'language' => $language));
            if (!$galleryTr) {
                $galleryTr = new Entity\GalleryTr;
                $galleryTr->setName($data['name']);
                $galleryTr->setGallery($gallery);
                $galleryTr->setLanguage($language);
                $this->em->persist($galleryTr);
            } else {
                $galleryTr->setName($data['name']);
            }

            $galleryTr->setName($data['name']);

            $eventCategory = $this->em->getRepository('Entity\EventCategory')->find($data['category_id']);
            $event->setCategory($eventCategory);            
            
            $start_date = !empty($data['start_time']) ? $data['start_date'] . " " . $data['start_time'] . ":00" : $data['start_date'] . " 00:00:00";            
            $event->setStart_date($start_date);
            $event->setStart_time($data['start_time']);
            
            if(!empty($data['end_date'])) {
                $end_date = !empty($data['end_time']) ? $data['end_date'] . " " . $data['end_time'] . ":00" : $data['end_date'] . " 00:00:00";
            }
            else {
                $end_date = NULL;
            }

            $event->setEnd_date($end_date);
            $event->setEnd_time($data['end_time']);
            
            $event->setVenue($data['venue']);
            $event->setAddress($data['address']);
            $event->setUrl1($data['url1']);
            $event->setUrl2($data['url2']);       

            $eventTr = $this->em->getRepository('Entity\EventTr')->findOneBy(array('event' => $event, 'language' => $language));
            if (!$eventTr) {
                $eventTr = new Entity\EventTr;
                $eventTr->setName($data['name']);
                $eventTr->setDescription($data['description']);
                $eventTr->setEvent($event);
                $eventTr->setLanguage($language);
                $this->em->persist($eventTr);
            } else {
                $eventTr->setName($data['name']);
                $eventTr->setDescription($data['description']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Event updated successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteEvent($event_id) {
        try {
            if (empty($event_id)) {
                throw new Exception("Parameter is missing.");
            }

            $event = $this->em->getRepository('Entity\Event')->find($event_id);
            if (!$event) {
                throw new Exception("Event not found.");
            }

            $this->em->remove($event);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Event deleted successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertEventCategory($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\EventCategory');

            //insert EventCategory
            $eventCategory = new Entity\EventCategory;
            $eventCategory->setWeight($weight);
            $this->em->persist($eventCategory);
            $this->em->flush();

            //insert EventCategoryTr
            $eventCategoryTr = new Entity\EventCategoryTr;
            $eventCategoryTr->setCategory($data['category']);
            $eventCategoryTr->setEventCategory($eventCategory);
            $eventCategoryTr->setLanguage($language);
            $this->em->persist($eventCategoryTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "New category created.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateEventCategory($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $eventCategory = $this->em->getRepository('Entity\EventCategory')->find($data['category_id']);
            if (!$eventCategory) {
                throw new Exception("EventCategory not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $eventCategoryTr = $this->em->getRepository('Entity\EventCategoryTr')->findOneBy(array('eventCategory' => $eventCategory, 'language' => $language));
            if (!$eventCategoryTr) {
                $eventCategoryTr = new Entity\EventCategoryTr;
                $eventCategoryTr->setCategory($data['category']);
                $eventCategoryTr->setEventCategory($eventCategory);
                $eventCategoryTr->setLanguage($language);
                $this->em->persist($eventCategoryTr);
            } else {
                $eventCategoryTr->setCategory($data['category']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Category successfully modified.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteEventCategory($category_id) {
        try {
            if (empty($category_id)) {
                throw new Exception("Parameter is missing.");
            }

            $eventCategory = $this->em->getRepository('Entity\EventCategory')->find($category_id);
            if (!$eventCategory) {
                throw new Exception("EventCategory not found.");
            }

            $this->em->remove($eventCategory);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Category deleted successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getEvents($event_id = null, $lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Event');
            $qb = $repo->createQueryBuilder('e');

            $qb->select('e.id', 'IDENTITY (e.gallery) AS gallery_id', 'e.status', 'e.start_date', /*'e.weight',*/'e.end_date', 'e.start_time', 'e.end_time', 'e.venue', 'e.address', 'e.url1',  'e.url2', 'e.create_date', 'etr.name', 'etr.description', 'ec.id AS category_id', 'ectr.category', 'gt.type', 'i.filename');
            $qb->leftJoin('e.translations', 'etr', 'WITH', 'IDENTITY (etr.language) = :lang_id OR etr.id IS NULL');
            $qb->leftJoin('e.category', 'ec');
            $qb->leftJoin('ec.translations', 'ectr', 'WITH', 'IDENTITY (ectr.language) = :lang_id OR ectr.id IS NULL');
            $qb->leftJoin('e.gallery', 'g');
            $qb->leftJoin('g.type', 'gt');
            $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');
            if (!empty($event_id)) {
                $qb->andWhere('e.id = :id');
                $qb->setParameter('id', $event_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('e.start_date', 'DESC');

            $events = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'data' => $events));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getEventCategories($lang_id = null, $category_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\EventCategory');
            $qb = $repo->createQueryBuilder('ec');

            $qb->select('ec.id', 'ectr.category');
            $qb->leftJoin('ec.translations', 'ectr', 'WITH', 'IDENTITY (ectr.language) = :lang_id OR ectr.id IS NULL');
            if (!empty($category_id)) {
                $qb->andWhere('ec.id = :id');
                $qb->setParameter('id', $category_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('ec.weight', 'ASC');

            $eventCategories = $qb->getQuery()->getArrayResult();

            return $eventCategories;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getCategoriesOfEvents() {
        try {
            $repo = $this->em->getRepository('Entity\Event');
            $qb = $repo->createQueryBuilder('e');

            $qb->select('IDENTITY (e.category)');
            $qb->distinct();

            $categories = $qb->getQuery()->getArrayResult();

            $id_array = array();
            $cntr = 0;

            foreach ($categories as $key => $val) {
                $temp = array_values($val);
                $id_array[$cntr] = $temp[0];
                $cntr++;
            }

            return $id_array;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\Event');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function categoryReorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\EventCategory');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setStatus($event_id, $status) {
        try {
            if (empty($event_id)) {
                throw new Exception("Parameter is missing.");
            }

            $event = $this->em->getRepository('Entity\Event')->find($event_id);
            if (!$event) {
                throw new Exception("Event not found.");
            }

            $event->setStatus($status);
            $this->em->flush();

            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
