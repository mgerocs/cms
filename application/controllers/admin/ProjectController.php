<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectController extends Common {

    private $folder;

    function __construct() {
        parent::__construct();

        $this->folder = "admin/project/";
    }

    function projects() {
        $this->is_logged_in();
        $content = $this->folder . "project-layout";
        $data['project_array'] = $this->folder . "project-array";
        $data['lang'] = $this->session->userdata('language')->id;
        $data['meta'] = $this->getMeta();
        if ($this->input->is_ajax_request()) {
            $this->load->view($content, $data);
        } else {
            $data['content'] = $content;
            $this->load->view('admin/_layout/default', $data);
        }
    }

    function projectDialog($langswitch = false) {
        $content = $this->folder . "project-dialog";
        $data['categories'] = $this->getProjectCategories($this->session->userdata('language')->id);
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['category_options'] = $this->folder . "category-options";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryDialog($langswitch = false) {
        $data['categories'] = $this->getProjectCategories($this->session->userdata('language')->id);
        $data['categoriesofprojects'] = $this->getCategoriesOfProjects();
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $content = $this->folder . "category-dialog";
        $data['category_list'] = $this->folder . "category-list";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryList($lang_id) {
        $data['categories'] = $this->getProjectCategories($lang_id);
        $data['categoriesofprojects'] = $this->getCategoriesOfProjects();
        $data['languages'] = $this->getLanguages();
        $content = $this->folder . "category-list";
        $this->load->view($content, $data);
    }

    function categoryOptions($lang_id) {
        $data['categories'] = $this->getProjectCategories($lang_id);
        $content = $this->folder . "category-options";
        $this->load->view($content, $data);
    }

    function validateProjectForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('name', 'Név', 'trim|required');

            (!empty($this->input->post('date'))) ? $this->form_validation->set_rules('date', 'Dátum', 'required') : $data['date'] = NULL;
          
            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertProject($data);
            } else {
                $this->updateProject($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateCategoryForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('category', 'Kategória', 'trim|required');

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertProjectCategory($data);
            } else {
                $this->updateProjectCategory($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertProject($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            //find GalleryType
            $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => "project"));
            if (empty($galleryType)) {
                throw new Exception("GalleryType not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\Project');

            //insert Gallery
            $gallery = new Entity\Gallery;
            $gallery->setType($galleryType);
            $this->em->persist($gallery);
            $this->em->flush();

            //insert GalleryTr
            $galleryTr = new Entity\GalleryTr;
            $galleryTr->setName($data['name']);
            $galleryTr->setGallery($gallery);
            $galleryTr->setLanguage($language);
            $this->em->persist($galleryTr);
            $this->em->flush();

            //find ProjectCategory
            $projectCategory = $this->em->getRepository('Entity\ProjectCategory')->find($data['category_id']);

            //insert Project
            $project = new Entity\Project;
            $project->setGallery($gallery);
            $project->setCategory($projectCategory);
            $project->setWeight($weight);
            $project->setDate($data['date'] . " 00:00:00");
            $this->em->persist($project);
            $this->em->flush();

            //insert ProjectTr
            $projectTr = new Entity\ProjectTr;
            $projectTr->setName($data['name']);
            $projectTr->setDescription($data['description']);
            $projectTr->setProject($project);
            $projectTr->setLanguage($language);
            $this->em->persist($projectTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Új projekt létrehozva.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateProject($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $project = $this->em->getRepository('Entity\Project')->find($data['id']);
            if (!$project) {
                throw new Exception("Project not found.");
            }                      

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }
            
            $gallery = $this->em->getRepository('Entity\Gallery')->find($project->getGallery());
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }
            
            $galleryTr = $this->em->getRepository('Entity\GalleryTr')->findOneBy(array('gallery' => $gallery, 'language' => $language));
            if (!$galleryTr) {
                $galleryTr = new Entity\galleryTr;
                $galleryTr->setName($data['name']);                
                $galleryTr->setGallery($gallery);
                $galleryTr->setLanguage($language);
                $this->em->persist($galleryTr);
            }  
            else {
                $galleryTr->setName($data['name']);                
            }
            
            $galleryTr->setName($data['name']);

            $projectCategory = $this->em->getRepository('Entity\ProjectCategory')->find($data['category_id']);
            $project->setCategory($projectCategory);
            $project->setDate($data['date'] . " 00:00:00");
            
            $projectTr = $this->em->getRepository('Entity\ProjectTr')->findOneBy(array('project' => $project, 'language' => $language));
            if (!$projectTr) {
                $projectTr = new Entity\ProjectTr;
                $projectTr->setName($data['name']);
                $projectTr->setDescription($data['description']);
                $projectTr->setProject($project);
                $projectTr->setLanguage($language);
                $this->em->persist($projectTr);
            } else {
                $projectTr->setName($data['name']);
                $projectTr->setDescription($data['description']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Projekt módosítása sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteProject($project_id) {
        try {
            if (empty($project_id)) {
                throw new Exception("Parameter is missing.");
            }

            $project = $this->em->getRepository('Entity\Project')->find($project_id);
            if (!$project) {
                throw new Exception("Project not found.");
            }

            $this->em->remove($project);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Projekt törlése sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertProjectCategory($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\ProjectCategory');

            //insert ProjectCategory
            $projectCategory = new Entity\ProjectCategory;
            $projectCategory->setWeight($weight);
            $this->em->persist($projectCategory);
            $this->em->flush();

            //insert ProjectCategoryTr
            $projectCategoryTr = new Entity\ProjectCategoryTr;
            $projectCategoryTr->setCategory($data['category']);
            $projectCategoryTr->setProjectCategory($projectCategory);
            $projectCategoryTr->setLanguage($language);
            $this->em->persist($projectCategoryTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Új kategória létrehozva.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateProjectCategory($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $projectCategory = $this->em->getRepository('Entity\ProjectCategory')->find($data['category_id']);
            if (!$projectCategory) {
                throw new Exception("ProjectCategory not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $projectCategoryTr = $this->em->getRepository('Entity\ProjectCategoryTr')->findOneBy(array('projectCategory' => $projectCategory, 'language' => $language));
            if (!$projectCategoryTr) {
                $projectCategoryTr = new Entity\ProjectCategoryTr;
                $projectCategoryTr->setCategory($data['category']);
                $projectCategoryTr->setProjectCategory($projectCategory);
                $projectCategoryTr->setLanguage($language);
                $this->em->persist($projectCategoryTr);
            } else {
                $projectCategoryTr->setCategory($data['category']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Kategória módosítása sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteProjectCategory($category_id) {
        try {
            if (empty($category_id)) {
                throw new Exception("Parameter is missing.");
            }

            $projectCategory = $this->em->getRepository('Entity\ProjectCategory')->find($category_id);
            if (!$projectCategory) {
                throw new Exception("ProjectCategory not found.");
            }

            $this->em->remove($projectCategory);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Kategória törlése sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getProjects($project_id = null, $lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Project');
            $qb = $repo->createQueryBuilder('p');

            $qb->select('p.id', 'IDENTITY (p.gallery) AS gallery_id', 'p.status', 'p.weight', 'p.date', 'p.create_date', 'ptr.name', 'ptr.description', 'pc.id AS category_id', 'pctr.category', 'gt.type', 'i.filename');
            $qb->leftJoin('p.translations', 'ptr', 'WITH', 'IDENTITY (ptr.language) = :lang_id OR ptr.id IS NULL');
            $qb->leftJoin('p.category', 'pc');
            $qb->leftJoin('pc.translations', 'pctr', 'WITH', 'IDENTITY (pctr.language) = :lang_id OR pctr.id IS NULL');
            $qb->leftJoin('p.gallery', 'g');
            $qb->leftJoin('g.type', 'gt');
            $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');
            if (!empty($project_id)) {
                $qb->andWhere('p.id = :id');
                $qb->setParameter('id', $project_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('p.weight', 'ASC');

            $projects = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'data' => $projects));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getProjectCategories($lang_id = null, $category_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\ProjectCategory');
            $qb = $repo->createQueryBuilder('pc');

            $qb->select('pc.id', 'pctr.category');
            $qb->leftJoin('pc.translations', 'pctr', 'WITH', 'IDENTITY (pctr.language) = :lang_id OR pctr.id IS NULL');
            if (!empty($category_id)) {
                $qb->andWhere('pc.id = :id');
                $qb->setParameter('id', $category_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('pc.weight', 'ASC');

            $projectCategories = $qb->getQuery()->getArrayResult();

            return $projectCategories;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getCategoriesOfProjects() {
        try {
            $repo = $this->em->getRepository('Entity\Project');
            $qb = $repo->createQueryBuilder('p');

            $qb->select('IDENTITY (p.category)');
            $qb->distinct();

            $categories = $qb->getQuery()->getArrayResult();

            $id_array = array();
            $cntr = 0;

            foreach ($categories as $key => $val) {
                $temp = array_values($val);
                $id_array[$cntr] = $temp[0];
                $cntr++;
            }

            return $id_array;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\Project');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function categoryReorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\ProjectCategory');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setStatus($project_id, $status) {
        try {
            if (empty($project_id)) {
                throw new Exception("Parameter is missing.");
            }

            $project = $this->em->getRepository('Entity\Project')->find($project_id);
            if (!$project) {
                throw new Exception("Project not found.");
            }

            $project->setStatus($status);
            $this->em->flush();

            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
