<?php

class IdentityController extends Common {

    private $folder;
    private $upload_path;
    private $expiration;

    function __construct() {
        parent::__construct();

        $this->load->helper('string');
        $this->load->helper('captcha');

        $this->upload_path = realpath(APPPATH . '../images');
        $this->expiration = time() - 7200;

        $this->folder = "admin/identity/";
    }

    function loginform() {
        $data['captcha'] = $this->createCaptcha();
        $data['content'] = "admin/identity/loginform";
        $data['meta'] = $this->getMeta();
        $this->load->view('admin/_layout/default', $data);
    }

    function pwdrecovery() {
        $data['content'] = $this->folder . "pwdrecovery";
        $data['meta'] = $this->getMeta();
        $this->load->view('admin/_layout/default', $data);
    }

    function pwdrec_info() {
        $data['content'] = $this->folder . "pwdrec-info";
        $data['meta'] = $this->getMeta();
        $this->load->view('admin/_layout/default', $data);
    }

    function newpwdForm($token = null) {
        $data = $this->checkRecoveryToken($token);
        if (!empty($token) && !empty($data)) {
            $data['content'] = $this->folder . "newpwd-form";
            $data['meta'] = $this->getMeta();
            $data['recovery'] = $data;
            $this->load->view('admin/_layout/default', $data);
        } else {
            die("Invalid token. Please, check the link.");
        }
    }

    function pwdrec_success() {
        $data['content'] = $this->folder . "pwdrec-success";
        $data['meta'] = $this->getMeta();
        $this->load->view('admin/_layout/default', $data);
    }

    function validateLoginForm() {

        try {

            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('login') === FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if (!$this->checkCaptcha()) {
                $error = 'Wrong CAPTCHA code.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "#captcha", 'recaptcha' => true));
                exit();
            }

            $repo1 = $this->em->getRepository('Entity\User');
            $qb1 = $repo1->createQueryBuilder('u');
            $qb1->select('u.id AS id', 'u.lastname AS lastname', 'u.firstname AS firstname', 'u.email AS email', 'gr.id AS group_id', 'gr.group');
            $qb1->leftJoin('u.group', 'gr');
            $qb1->where('u.email = :email');
            $qb1->andWhere('u.password = :password');
            $qb1->setParameter('email', $data['email']);
            $qb1->setParameter('password', hash('sha256', $data['password']));

            $user = $qb1->getQuery()->getOneOrNullResult();

            if (empty($user)) {
                $error = 'Wrong username or password.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "#password", 'recaptcha' => true));
                exit();
            }

            $repo2 = $this->em->getRepository('Entity\SiteLanguage');
            $qb2 = $repo2->createQueryBuilder('sl');
            $qb2->select('sl.id AS id', 'l.language AS language', 'l.label AS label', 'l.code AS code');
            $qb2->leftJoin('sl.language', 'l');
            $qb2->where('sl.def = 1');

            $default_lang = $qb2->getQuery()->getOneOrNullResult();

            if (empty($default_lang)) {
                throw new Exception("Default language not found.");
            }

            $user = (object) $user;
            $default_lang = (object) $default_lang;

            $this->session->set_userdata('is_logged_in', true);
            $this->session->set_userdata('user', $user);
            $this->session->set_userdata('language', $default_lang);

            echo json_encode(array('status' => true, 'redirect' => base_url() . "admin/dashboard"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function createCaptcha() {
        try {
            $vals = array(
                'word' => random_string('numeric', 6),
                'img_path' => 'captcha/',
                'img_url' => base_url() . '/captcha/',
                'font_path' => 'css_admin/font/Capture_it.ttf',
                'img_width' => '200',
                'img_height' => 43,
                'expiration' => 7200
            );
            $cap = create_captcha($vals);

            $data = array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );
            $this->db->flush_cache();
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);

            if ($this->input->is_ajax_request()) {
                echo json_encode(array('status' => true, 'captcha' => $cap['image']));
            } else {
                return $cap['image'];
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function checkCaptcha() {
        try {
            $this->db->flush_cache();
            $this->db->where('captcha_time <', $this->expiration);
            $this->db->delete('captcha');

            //Then see if a captcha exists:
            $this->db->flush_cache();
            $this->db->select('captcha.*');
            $this->db->from('captcha');            
            $this->db->where('captcha_time >', $this->expiration);
            $this->db->where('ip_address', $this->input->ip_address());
            $this->db->where('word', $this->input->post('captcha')); 
            $this->db->where('captcha_id IN (SELECT MAX(captcha_id) FROM captcha)', NULL, FALSE);            
            $query = $this->db->get();            
            $captcha = $query->row_array();

            if (!empty($captcha)) {
                return TRUE;
            } else {
                return FALSE;
            }
            
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validatePwdRecForm() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('pwdrecovery') === FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $token = random_string('unique');

            $recoveryCheck = $this->em->getRepository('Entity\Recovery')->findOneBy(array('email' => $data['email'], 'used' => 0));

            if ($recoveryCheck) {
                $error = 'A new password has already been requested with this e-mail address. Please, check your e-mail account.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "input#pwdrec_email"));
                exit();
            }

            $recovery = new Entity\Recovery;
            $recovery->setEmail($data['email']);
            $recovery->setIp_address($this->input->ip_address());
            $recovery->setToken($token);
            $this->em->persist($recovery);
            $this->em->flush();

            $link = base_url() . "identity/setnewpwd/" . urlencode($token);

            $message = "";
            $message .= "<h3>Set new password</h3>";
            $message .= "<p>Click on the link below: </br></p>";
            $message .= "<a href='" . $link . "' target='_blank'>Request new password</a>";

            //$this->sendEmail("info@propellerweb.hu", $data['email'], "Új jelszó", $message);

            echo json_encode(array('status' => true, 'redirect' => base_url() . "admin/identity/pwdrec_info"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateNewPwdForm() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('newpwd') === FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $recovery = $this->em->getRepository('Entity\Recovery')->find($data['recovery_id']);
            if (!$recovery) {
                throw new Exception("Recovery not found.");
            }

            $user = $this->em->getRepository('Entity\User')->findOneBy(array('email' => $data['email']));
            if (!$user) {
                throw new Exception("User not found.");
            }

            $user->setPassword(hash('sha256', $data['new_password']));
            $recovery->setUsed(1);

            $this->em->flush();

            echo json_encode(array('status' => true, 'redirect' => base_url() . "admin/identity/pwdrec_success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function checkRecoveryToken($token) {
        try {
            if (empty($token)) {
                throw new Exception("Parameter is missing.");
            }

            $repo = $this->em->getRepository('Entity\Recovery');
            $qb = $repo->createQueryBuilder('r');

            $qb->select('r.id, r.email', 'r.token', 'r.create_date');
            $qb->where('r.token = :token');
            $qb->andWhere('r.used = 0');
            $qb->setParameter('token', $token);

            $recovery = $qb->getQuery()->getArrayResult();
            if (count($recovery) == 1) {
                return $recovery[0];
            } else {
                throw new Exception("Recovery error");
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('admin/identity');
    }

}
