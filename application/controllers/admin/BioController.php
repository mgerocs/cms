<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BioController extends Common {

    private $folder;

    function __construct() {
        parent::__construct();

        $this->folder = "admin/bio/";
    }

    function biography() {
        try {
            $this->is_logged_in();
            $content = $this->folder . "bio-layout";
            $data['bio_array'] = $this->folder . "bio-array";
            $data['lang'] = $this->session->userdata('language')->id;
            $data['meta'] = $this->getMeta();
            if ($this->input->is_ajax_request()) {
                $this->load->view($content, $data);
            } else {
                $data['content'] = $content;
                $this->load->view('admin/_layout/default', $data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function bioDialog($langswitch = false) {
        $content = $this->folder . "bio-dialog";
        $data['categories'] = $this->getBioCategories($this->session->userdata('language')->id);
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['category_options'] = $this->folder . "category-options";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryDialog($langswitch = false) {
        $data['categories'] = $this->getBioCategories($this->session->userdata('language')->id);
        $data['categoriesofbio'] = $this->getCategoriesOfBio();
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $content = $this->folder . "category-dialog";
        $data['category_list'] = $this->folder . "category-list";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryList($lang_id) {
        $data['categories'] = $this->getBioCategories($lang_id);
        $data['categoriesofbio'] = $this->getCategoriesOfBio();
        $data['languages'] = $this->getLanguages();
        $content = $this->folder . "category-list";
        $this->load->view($content, $data);
    }

    function categoryOptions($lang_id) {
        $data['categories'] = $this->getBioCategories($lang_id);
        $content = $this->folder . "category-options";
        $this->load->view($content, $data);
    }

    function validateBioForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('start', 'Start', 'trim|required');
            $this->form_validation->set_rules('field1', 'Field 1', 'trim|required');

            (!empty($this->input->post('end'))) ? $this->form_validation->set_rules('end', 'End', 'required') : $data['end'] = NULL;
            (!empty($this->input->post('field2'))) ? $this->form_validation->set_rules('field2', 'Field 2', 'required') : $data['field2'] = NULL;
            (!empty($this->input->post('field3'))) ? $this->form_validation->set_rules('field3', 'Field 3', 'required') : $data['field3'] = NULL;
            (!empty($this->input->post('field4'))) ? $this->form_validation->set_rules('field4', 'Field 4', 'required') : $data['field4'] = NULL;
            (!empty($this->input->post('url'))) ? $this->form_validation->set_rules('url', 'URL', 'required') : $data['url'] = NULL;

            if (!isset($data['in_progress'])) {
                $data['in_progress'] = 0;
            }

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertBio($data);
            } else {
                $this->updateBio($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateCategoryForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('category', 'Category', 'trim|required');

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertBioCategory($data);
            } else {
                $this->updateBioCategory($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertBio($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            //find GalleryCategory
            $bioCategory = $this->em->getRepository('Entity\BioCategory')->find($data['category_id']);

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\Biography');

            //insert Biography
            $bio = new Entity\Biography;
            $bio->setCategory($bioCategory);
            $bio->setStart($data['start']);
            $bio->setEnd($data['end']);
            $bio->setIn_progress($data['in_progress']);
            $bio->setUrl($data['url']);
            $bio->setWeight($weight);
            $this->em->persist($bio);
            $this->em->flush();

            //insert BioTr
            $bioTr = new Entity\BioTr;
            $bioTr->setField1($data['field1']);
            $bioTr->setField2($data['field2']);
            $bioTr->setField3($data['field3']);
            $bioTr->setField4($data['field4']);
            $bioTr->setBiography($bio);
            $bioTr->setLanguage($language);
            $this->em->persist($bioTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "New entry created.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateBio($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $bio = $this->em->getRepository('Entity\Biography')->find($data['id']);
            if (!$bio) {
                throw new Exception("Biography not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $bioCategory = $this->em->getRepository('Entity\BioCategory')->find($data['category_id']);
            $bio->setCategory($bioCategory);
            $bio->setStart($data['start']);
            $bio->setEnd($data['end']);
            $bio->setIn_progress($data['in_progress']);
            $bio->setUrl($data['url']);

            $bioTr = $this->em->getRepository('Entity\BioTr')->findOneBy(array('biography' => $bio, 'language' => $language));
            if (!$bioTr) {
                $bioTr = new Entity\BioTr;
                $bioTr->setField1($data['field1']);
                $bioTr->setField2($data['field2']);
                $bioTr->setField3($data['field3']);
                $bioTr->setField4($data['field4']);
                $bioTr->setBiography($bio);
                $bioTr->setLanguage($language);
                $this->em->persist($bioTr);
            } else {
                $bioTr->setField1($data['field1']);
                $bioTr->setField2($data['field2']);
                $bioTr->setField3($data['field3']);
                $bioTr->setField4($data['field4']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Entry updated successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteBio($bio_id) {
        try {
            if (empty($bio_id)) {
                throw new Exception("Parameter is missing.");
            }

            $bio = $this->em->getRepository('Entity\Biography')->find($bio_id);
            if (!$bio) {
                throw new Exception("Biography not found.");
            }

            $this->em->remove($bio);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Entry deleted successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertBioCategory($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\BioCategory');

            //insert BioCategory
            $bioCategory = new Entity\BioCategory;
            $bioCategory->setWeight($weight);
            $this->em->persist($bioCategory);
            $this->em->flush();

            //insert BioCategoryTr
            $bioCategoryTr = new Entity\BioCategoryTr;
            $bioCategoryTr->setCategory($data['category']);
            $bioCategoryTr->setBioCategory($bioCategory);
            $bioCategoryTr->setLanguage($language);
            $this->em->persist($bioCategoryTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "New category created.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateBioCategory($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $bioCategory = $this->em->getRepository('Entity\BioCategory')->find($data['category_id']);
            if (!$bioCategory) {
                throw new Exception("BioCategory not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $bioCategoryTr = $this->em->getRepository('Entity\BioCategoryTr')->findOneBy(array('bioCategory' => $bioCategory, 'language' => $language));
            if (!$bioCategoryTr) {
                $bioCategoryTr = new Entity\BioCategoryTr;
                $bioCategoryTr->setCategory($data['category']);
                $bioCategoryTr->setBioCategory($bioCategory);
                $bioCategoryTr->setLanguage($language);
                $this->em->persist($bioCategoryTr);
            } else {
                $bioCategoryTr->setCategory($data['category']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Category updated successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteBioCategory($category_id) {
        try {
            if (empty($category_id)) {
                throw new Exception("Parameter is missing.");
            }

            $bioCategory = $this->em->getRepository('Entity\BioCategory')->find($category_id);
            if (!$bioCategory) {
                throw new Exception("BioCategory not found.");
            }

            $this->em->remove($bioCategory);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Category deleted succesfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getBio($bio_id = null, $lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Biography');
            $qb = $repo->createQueryBuilder('b');

            $qb->select('b.id', 'b.start', 'b.end', 'b.in_progress', 'b.status', 'b.weight', 'b.create_date', 'btr.field1', 'btr.field2', 'btr.field3', 'btr.field4', 'b.url', 'bc.id AS category_id', 'bctr.category');
            $qb->leftJoin('b.translations', 'btr', 'WITH', 'IDENTITY (btr.language) = :lang_id OR btr.id IS NULL');
            $qb->leftJoin('b.category', 'bc');
            $qb->leftJoin('bc.translations', 'bctr', 'WITH', 'IDENTITY (bctr.language) = :lang_id OR bctr.id IS NULL');
            if (!empty($bio_id)) {
                $qb->andWhere('b.id = :id');
                $qb->setParameter('id', $bio_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('b.weight', 'ASC');

            $bio = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'data' => $bio));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getBioCategories($lang_id = null, $category_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\BioCategory');
            $qb = $repo->createQueryBuilder('bc');

            $qb->select('bc.id', 'bctr.category');
            $qb->leftJoin('bc.translations', 'bctr', 'WITH', 'IDENTITY (bctr.language) = :lang_id OR bctr.id IS NULL');
            if (!empty($category_id)) {
                $qb->andWhere('bc.id = :id');
                $qb->setParameter('id', $category_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('bc.weight', 'ASC');

            $bioCategories = $qb->getQuery()->getArrayResult();

            return $bioCategories;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getCategoriesOfBio() {
        try {
            $repo = $this->em->getRepository('Entity\Biography');
            $qb = $repo->createQueryBuilder('b');

            $qb->select('IDENTITY (b.category)');
            $qb->distinct();

            $categories = $qb->getQuery()->getArrayResult();

            $id_array = array();
            $cntr = 0;

            foreach ($categories as $key => $val) {
                $temp = array_values($val);
                $id_array[$cntr] = $temp[0];
                $cntr++;
            }

            return $id_array;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\Biography');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function categoryReorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\BioCategory');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setStatus($bio_id, $status) {
        try {
            if (empty($bio_id)) {
                throw new Exception("Parameter is missing.");
            }

            $bio = $this->em->getRepository('Entity\Biography')->find($bio_id);
            if (!$bio) {
                throw new Exception("Biography not found.");
            }

            $bio->setStatus($status);
            $this->em->flush();

            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
