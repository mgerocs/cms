<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class IndexController extends Common {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->is_logged_in();
        $data['content'] = "";
        $this->load->view('admin/_layout/default', $data);
    }

}
