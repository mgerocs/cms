<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ImageController extends Common {

    function __construct() {
        parent::__construct();        
    }

    function index() {
        echo "hello";
    }

    function insertImage($gallery_id) {
        try {
            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($gallery_id);
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $image = new Entity\Image;
            $image->setFilename("test.jpg");
            $image->setExt(".jpg");
            $image->setSize(300);
            $image->setGallery($gallery);
            //$image->setOrder();

            $this->em->persist($image);
            $this->em->flush();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateImage($image_id) {
        try {
            if (empty($image_id)) {
                throw new Exception("Parameter is missing.");
            }

            $image = $this->em->getRepository('Entity\Image')->find($image_id);
            if (!$image) {
                throw new Exception("Image not found.");
            }

            $image->setTitle("Title added");
            $image->setDesc("Description added");

            $this->em->flush();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteImage($image_id) {
        try {
            if (empty($image_id)) {
                throw new Exception("Parameter is missing.");
            }

            $image = $this->em->getRepository('Entity\Image')->find($image_id);
            if (!$image) {
                throw new Exception("Image not found.");
            }

            $this->em->remove($image);
            $this->em->flush();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getImages($param = null) {
        try {
            $repo = $this->em->getRepository('Entity\Image');
            $qb = $repo->createQueryBuilder('i');
            if ($param) {
                if (is_numeric($param)) {                    
                    $image_id = $param;
                    $qb->where('i.id = :id');
                    $qb->setParameter('id', $image_id);                    
                } else {
                    $type = $param;
                    $qb->innerJoin('i.gallery', 'g');
                    $qb->innerJoin('g.type', 'gt');     
                    $qb->where('gt.type = :type');
                    $qb->setParameter('type', $type);                   
                }
            }
            $qb->orderBy('i.create_date', 'DESC');
            $images = $qb->getQuery()->getArrayResult();
            echo json_encode(array('status' => true, 'images' => $images));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
