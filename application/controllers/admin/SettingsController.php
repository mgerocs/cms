<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SettingsController extends Common {

    private $folder;
    private $allowed_types;
    private $upload_path;

    function __construct() {
        parent::__construct();        

        $this->folder = "admin/settings/";

        $this->allowed_types = 'gif|jpg|jpeg|png';
        $this->upload_path = realpath(APPPATH . '../');
    }

    function settings() {
        $this->is_logged_in();
        $content = $this->folder . "settings-layout";
        $data['lang_array'] = $this->folder . "lang-array";
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $meta = $this->em->getRepository('Entity\Meta')->find(1);
         if (is_object($meta) && count(get_object_vars($meta)) > 0) {
            $data['logo'] = $meta->getLogo();
            $data['favicon'] = $meta->getFavicon();
        } else {
            $data['logo'] = "";
            $data['favicon'] = "";
        }
        if ($this->input->is_ajax_request()) {
            $this->load->view($content, $data);
        } else {
            $data['content'] = $content;
            $this->load->view('admin/_layout/default', $data);
        }
    }

    function languages() {
        $languages = $this->getLanguages();
        echo json_encode(array('status' => true, 'data' => $languages));
        exit();
    }

    function lang_array() {
        $content = $this->folder . "lang-array";
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $this->load->view($content, $data);
    }

    function langDialog() {
        $content = $this->folder . "lang-dialog";
        $data['lang'] = $this->session->userdata('language')->id;
        $this->load->view($content, $data);
    }

    function metaDialog($langswitch = true) {
        $content = $this->folder . "meta-dialog";
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function logoForm() {
        $content = $this->folder . "logo-form";
        $meta = $this->em->getRepository('Entity\Meta')->find(1);
        $data['logo'] = $meta->getLogo();
        $this->load->view($content, $data);
    }

    function faviconForm() {
        $content = $this->folder . "favicon-form";
        $meta = $this->em->getRepository('Entity\Meta')->find(1);
        $data['favicon'] = $meta->getFavicon();
        $this->load->view($content, $data);
    }

    function getUnselectedLanguages() {
        try {
            $repo1 = $this->em->getRepository('Entity\SiteLanguage');
            $qb1 = $repo1->createQueryBuilder('sl');
            $sub = $qb1->select('IDENTITY (sl.language)');

            $repo2 = $this->em->getRepository('Entity\Language');
            $qb2 = $repo2->createQueryBuilder('l');

            $qb2->select('l.id AS id', 'l.code AS code', 'l.label AS value');
            $qb2->where($qb2->expr()->notIn('l.id', $sub->getDQL()));

            $languages = $qb2->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'langlist' => $languages));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateLangForm() {
        try {

            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('language') === FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $language = $this->em->getRepository('Entity\Language')->find($data['lang_id']);
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\SiteLanguage');

            $siteLanguage = new Entity\SiteLanguage;
            $siteLanguage->setLanguage($language);
            $siteLanguage->setWeight($weight);
            $this->em->persist($siteLanguage);
            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "Új nyelv hozzáadva.",
                'context' => "success"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateMetaForm() {
        try {

            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('title', 'Cím', 'trim');

            (!empty($this->input->post('title'))) ? $this->form_validation->set_rules('title', 'Cím', 'trim|required') : $data['title'] = NULL;
            (!empty($this->input->post('keywords'))) ? $this->form_validation->set_rules('keywords', 'Kulcsszavak', 'required') : $data['keywords'] = NULL;
            (!empty($this->input->post('description'))) ? $this->form_validation->set_rules('description', 'Leírás', 'required') : $data['description'] = NULL;
            (!empty($this->input->post('author'))) ? $this->form_validation->set_rules('author', 'Szerző', 'trim|required') : $data['author'] = NULL;

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $meta = $this->em->getRepository('Entity\Meta')->find(1);
            if (empty($meta)) {
                throw new Exception("Meta not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $meta->setAuthor($data['author']);

            $metaTr = $this->em->getRepository('Entity\MetaTr')->findOneBy(array('meta' => $meta, 'language' => $language));
            if (!$metaTr) {
                $metaTr = new Entity\MetaTr;
                $metaTr->setTitle($data['title']);
                $metaTr->setKeywords($data['keywords']);
                $metaTr->setDescription($data['description']);
                $metaTr->setMeta($meta);
                $metaTr->setLanguage($language);
                $this->em->persist($metaTr);
            } else {
                $metaTr->setTitle($data['title']);
                $metaTr->setKeywords($data['keywords']);
                $metaTr->setDescription($data['description']);
            }

            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "Adatok módosítása sikeres.",
                'context' => "success",
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setDefaultLang($lang_id) {
        try {

            if (empty($lang_id)) {
                throw new Exception("Parameter  is missing.");
            }

            $repo = $this->em->getRepository('Entity\SiteLanguage');
            $qb = $repo->createQueryBuilder('sl');

            $qb->update('Entity\SiteLanguage', 'sl');
            $qb->set('sl.def', $qb->expr()->literal(0));
            $qb->getQuery()->execute();

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($lang_id);
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $language->setDefault(1);
            $this->em->flush();

            $repo = $this->em->getRepository('Entity\SiteLanguage');
            $qb = $repo->createQueryBuilder('sl');
            $qb->select('sl.id AS id', 'l.language AS language', 'l.label AS label', 'l.code AS code');
            $qb->leftJoin('sl.language', 'l');
            $qb->where('sl.def = 1');

            $default_lang = $qb->getQuery()->getOneOrNullResult();

            if (empty($default_lang)) {
                throw new Exception("Default language not found.");
            }

            $default_lang = (object) $default_lang;

            $this->session->set_userdata('language', $default_lang);

            echo json_encode(array(
                'status' => true,
                'message' => "Alapértelmezett nyelv megváltoztatva.",
                'context' => "success",
                'updateUI' => array(
                    '#lang_code' => $this->session->userdata('language')->code
                )
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function removeLang($lang_id) {
        try {

            if (empty($lang_id)) {
                throw new Exception("Parameter  is missing.");
            }

            $siteLanguage = $this->em->getRepository('Entity\SiteLanguage')->find($lang_id);
            if (empty($siteLanguage)) {
                throw new Exception("Language not found.");
            }

            if ($siteLanguage->getDefault() == 1) {
                echo json_encode(array('status' => false));
                exit();
            }

            $this->em->remove($siteLanguage);
            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "Nyelv törlése sikeres.",
                'context' => "success"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        $data = $this->input->post();
        $this->reorderEntity($data, 'Entity\SiteLanguage');
        echo json_encode(array('status' => true));
        exit();
    }

    function upload() {
        try {

            $type = $this->input->post('type');
            if (empty($type)) {
                throw new Exception("Parameter is missing.");
            }

            $meta = $this->em->getRepository('Entity\Meta')->find(1);
            if (empty($meta)) {
                throw new Exception("Meta not found.");
            }

            //construct upload path
            $upload_path = $this->upload_path . "/images/brand/";

            $path = $_FILES["userfile"]["name"];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            switch ($type) {
                case "logo":
                    $filename = "logo";
                    $meta->setLogo($filename . "." . $ext);
                    break;
                case "favicon":
                    $filename = "favicon";
                    $meta->setFavicon($filename . "." . $ext);
                    break;
            }

            $errors = array();

            $config = array(
                'upload_path' => $upload_path,
                'allowed_types' => $this->allowed_types,
                'file_name' => $filename,
                'overwrite' => true
            );

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $errors = strip_tags($this->upload->display_errors());
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $fileinfo = $this->upload->data();

            $this->em->flush();

            $response = array(
                'status' => true,
                'dtReload' => false,
                'partialUrl' => base_url() . 'admin/settings/' . $type . 'Form',
                'parent' => "#" . $type
            );
            if ($type == "logo") {
                $response['updateUI'] = array("#navbar_logo" => '<img src="' . base_url() . 'images/brand/' . $filename . "." . $ext . '?token=' . uniqid() . '" class="img-responsive navbar_logo" alt="logó" />');
            }
            echo json_encode($response);
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteImage($type) {
        try {
            if (empty($type)) {
                throw new Exception("Parameter is missing.");
            }

            $meta = $this->em->getRepository('Entity\Meta')->find(1);
            if (empty($meta)) {
                throw new Exception("Meta not found.");
            }

            $filename = "";

            switch ($type) {
                case "logo":
                    $filename = $meta->getLogo();
                    $meta->setLogo(NULL);
                    break;
                case "favicon":
                    $filename = $meta->getFavicon();
                    $meta->setFavicon(NULL);
                    break;
            }

            $delete_path = $this->upload_path . "/images/brand/";

            if (!empty($filename)) {
                if (file_exists($delete_path . $filename)) {
                    unlink($delete_path . $filename);
                }
            }

            $this->em->flush();

            $response = array(
                'status' => true,
                'dtReload' => false,
                'partialUrl' => base_url() . 'admin/settings/' . $type . 'Form',
                'parent' => "#" . $type
            );
            if ($type == "logo") {
                $response['updateUI'] = array("#navbar_logo" => '');
            }
            echo json_encode($response);
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getMetaData($lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Meta');
            $qb = $repo->createQueryBuilder('m');

            $qb->select('m.id', 'm.author', 'mtr.keywords', 'mtr.description', 'mtr.title');
            $qb->leftJoin('m.translations', 'mtr', 'WITH', 'IDENTITY (mtr.language) = :lang_id OR mtr.id IS NULL');
            $qb->setParameter('lang_id', $lang_id);

            $meta = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'data' => $meta));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
