<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends Common {

    private $folder;
    private $allowed_types;
    private $upload_path;

    function __construct() {
        parent::__construct();        

        $this->folder = "admin/user/";

        $this->allowed_types = 'gif|jpg|jpeg|png';
        $this->upload_path = realpath(APPPATH . '../');
    }

    function users() {
        $this->is_logged_in();
        $content = $this->folder . "user-layout";
        $data['user'] = $this->session->userdata('user');
        $user = $this->em->getRepository('Entity\User')->find($this->session->userdata('user')->id);
        $data['portrait'] = $user->getPortrait();
        if ($this->input->is_ajax_request()) {
            $this->load->view($content, $data);
        } else {
            $data['content'] = $content;
            $this->load->view('admin/_layout/default', $data);
        }
    }

    function userDialog($langswitch = false) {
        $content = $this->folder . "user-dialog";
        $this->load->view($content);
    }

    function passwordDialog() {
        $content = $this->folder . "password-dialog";
        $this->load->view($content);
    }

    function portraitForm() {
        try {
            $content = $this->folder . "portrait-form";
            $user = $this->em->getRepository('Entity\User')->find($this->session->userdata('user')->id);
            if (empty($user)) {
                throw new Exception("User not found.");
            }
            $data['portrait'] = $user->getPortrait();
            $this->load->view($content, $data);
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateUserForm() {
        try {

            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');

            (!empty($this->input->post('lastname'))) ? $this->form_validation->set_rules('lastname', 'Vezetéknév', 'trim|required') : $data['lastname'] = NULL;
            (!empty($this->input->post('firstname'))) ? $this->form_validation->set_rules('firstname', 'Keresztnév', 'trim|required') : $data['firstname'] = NULL;
            (!empty($this->input->post('address'))) ? $this->form_validation->set_rules('address', 'Cím', 'trim|required') : $data['address'] = NULL;
            (!empty($this->input->post('city'))) ? $this->form_validation->set_rules('city', 'Város', 'trim|required') : $data['city'] = NULL;
            (!empty($this->input->post('postal_code'))) ? $this->form_validation->set_rules('postal_code', 'Irányítószám', 'trim|required') : $data['postal_code'] = NULL;
            (!empty($this->input->post('phone1'))) ? $this->form_validation->set_rules('phone1', 'Telefon 1', 'trim|required') : $data['phone1'] = NULL;
            (!empty($this->input->post('phone2'))) ? $this->form_validation->set_rules('phone2', 'Telefon 2', 'trim|required') : $data['phone2'] = NULL;

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $user = $this->em->getRepository('Entity\User')->find($this->session->userdata('user')->id);
            if (empty($user)) {
                throw new Exception("User not found.");
            }

            $user->setLastname($data['lastname']);
            $user->setFirstname($data['firstname']);
            $user->setEmail($data['email']);
            $user->setAddress($data['address']);
            $user->setCity($data['city']);
            $user->setPostal_code($data['postal_code']);
            $user->setPhone1($data['phone1']);
            $user->setPhone2($data['phone2']);
            $this->em->flush();

            $this->session->userdata('user')->lastname = $data['lastname'];
            $this->session->userdata('user')->firstname = $data['firstname'];
            $this->session->userdata('user')->email = $data['email'];

            echo json_encode(array(
                'status' => true,
                'message' => "Adatok módosítása sikeres.",
                'context' => "success",
                'updateUI' => array(
                    '#logged_in' => $this->session->userdata('user')->email
                )
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validatePasswordForm() {
        try {

            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('change_pwd') == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $repo = $this->em->getRepository('Entity\User');
            $qb = $repo->createQueryBuilder('u');
            $qb->select('u');
            $qb->where('u.email = :email');
            $qb->andWhere('u.password = :password');
            $qb->setParameter('email', $this->session->userdata('user')->email);
            $qb->setParameter('password', hash('sha256', $data['old_password']));

            $user = $qb->getQuery()->getOneOrNullResult();

            if (empty($user)) {
                $error = 'Hibás felhasználónév vagy jelszó.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "#old_password"));
                exit();
            }
            if ($user->getPassword() == hash('sha256', $data['ch_password'])) {
                $error = 'A régi és az új jelszó nem lehet azonos.';
                echo json_encode(array('status' => false, 'error' => $error, 'element' => "#ch_password_"));
                exit();
            }

            $user->setPassword(hash('sha256', $data['ch_password']));
            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "Jelszó módosítása sikeres.",
                'context' => "success"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getMyData() {
        $repo = $this->em->getRepository('Entity\User');
        $qb = $repo->createQueryBuilder('u');
        $qb->select('u.id AS id', 'u.lastname AS lastname', 'u.firstname AS firstname', 'u.email AS email', 'u.address AS address', 'u.city AS city', 'u.postal_code AS postal_code', 'u.phone1 AS phone1', 'u.phone2 AS phone2');
        $qb->where('u.id = :id');
        $qb->setParameter('id', $this->session->userdata('user')->id);

        $user = $qb->getQuery()->getArrayResult();

        if (empty($user)) {
            throw new Exception("User not found.");
        }
        echo json_encode(array('status' => true, 'data' => $user));
        exit();
    }

    function upload() {
        try {

            $user = $this->em->getRepository('Entity\User')->find($this->session->userdata('user')->id);
            if (empty($user)) {
                throw new Exception("User not found.");
            }

            //construct upload path
            $upload_path = $this->upload_path . "/images/brand/";

            $path = $_FILES["userfile"]["name"];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            $filename = "portrait";
            $user->setPortrait($filename . "." . $ext);

            $errors = array();

            $config = array(
                'upload_path' => $upload_path,
                'allowed_types' => $this->allowed_types,
                'file_name' => $filename,
                'overwrite' => true
            );

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $errors = strip_tags($this->upload->display_errors());
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $fileinfo = $this->upload->data();

            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'partialUrl' => base_url() . 'admin/users/portraitForm',
                'parent' => "#portrait"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteImage() {
        try {

            $user = $this->em->getRepository('Entity\User')->find($this->session->userdata('user')->id);
            if (empty($user)) {
                throw new Exception("User not found.");
            }

            $filename = $user->getPortrait();
            $user->setPortrait(NULL);

            $delete_path = $this->upload_path . "/images/brand/";

            if (!empty($filename)) {
                if (file_exists($delete_path . $filename)) {
                    unlink($delete_path . $filename);
                }
            }

            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'partialUrl' => base_url() . 'admin/users/portraitForm',
                'parent' => "#portrait"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
