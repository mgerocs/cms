<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MenuController extends Common {

    function __construct() {
        parent::__construct();

        $this->folder = "admin/menu/";
    }

    function menu() {
        $this->is_logged_in();
        $content = $this->folder . "menu-layout";
        //$data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
//        $meta = $this->em->getRepository('Entity\Meta')->find(1);
//        if (is_object($meta) && count(get_object_vars($meta)) > 0) {
//            $data['logo'] = $meta->getLogo();
//            $data['favicon'] = $meta->getFavicon();
//        } else {
//            $data['logo'] = "";
//            $data['favicon'] = "";
//        }
        $data['menu_tree'] = $this->getMenu();
        if ($this->input->is_ajax_request()) {
            $this->load->view($content, $data);
        } else {
            $data['content'] = $content;
            $this->load->view('admin/_layout/default', $data);
        }
    }

    function languages() {
        $languages = $this->getLanguages();
        echo json_encode(array('status' => true, 'data' => $languages));
        exit();
    }

    function menuDialog($langswitch = true) {
        $content = $this->folder . "menu-dialog";
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function menuTree() {
        $content = $this->folder . "menu-tree";
        $data['menu_tree'] = $this->getMenu();
        $this->load->view($content, $data);
    }

    function validateMenuForm() {
        try {

            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $errors = array();

            if ($this->validation('menu') === FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\Menu');

            $menu = new Entity\Menu;
            $menu->setParent(0);
            $menu->setWeight($weight);
            $menu->setUrl($data['url']);
            $this->em->persist($menu);

            $menuTr = $this->em->getRepository('Entity\MenuTr')->findOneBy(array('menu' => $menu, 'language' => $language));
            if (!$menuTr) {
                $menuTr = new Entity\MenuTr;
                $menuTr->setLabel($data['label']);
                $menuTr->setMenu($menu);
                $menuTr->setLanguage($language);
                $this->em->persist($menuTr);
            } else {
                $menuTr->setLabel($data['label']);
                $this->em->persist($menuTr);
            }

            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "New menu created.",
                'context' => "success",
                'partialUrl' => base_url() . "admin/menu/menuTree",
                'parent' => "#menu_tree"
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        $data = $this->input->post();
        $this->reorderEntity($data, 'Entity\SiteLanguage');
        echo json_encode(array('status' => true));
        exit();
    }

    function deleteMenu($menu_id) {
        try {
            if (empty($menu_id)) {
                throw new Exception("Parameter is missing.");
            }

            $menu = $this->em->getRepository('Entity\Menu')->find($menu_id);
            if (!$menu) {
                throw new Exception("Menu not found.");
            }

            $this->em->remove($menu);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Menu deleted successfully.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getMenu($lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Menu');
            $qb = $repo->createQueryBuilder('m');

            $qb->select('m.id', 'm.parent', 'm.status', 'm.weight', 'm.url', 'mtr.label');
            $qb->leftJoin('m.translations', 'mtr', 'WITH', 'IDENTITY (mtr.language) = :lang_id OR mtr.id IS NULL');
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('m.weight', 'ASC');

            $items = $qb->getQuery()->getArrayResult();

            function buildTree($elements, $parentId = 0) {
                $branch = array();

                foreach ($elements as $element) {
                    if ($element['parent'] == $parentId) {
                        $children = buildTree($elements, $element['id']);
                        if ($children) {
                            $element['children'] = $children;
                        }
                        $branch[] = $element;
                    }
                }

                return $branch;
            }

            $tree = buildTree($items);

            return $tree;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateMenu() {

        $data = $this->input->post('tree');

        if (empty($data)) {
            throw new Exception("No data posted.");
        }

        $tree = $data;
        $repo = $this->em->getRepository('Entity\Menu');

        function updateTree($repo, $branch, $parent = 0) {
            foreach ($branch as $key => $item) {
                $menu = $repo->find($item['id']);
                $menu->setParent($parent);
                $menu->setWeight($key + 1);
                if (array_key_exists('children', $item)) {
                    updateTree($repo, $item['children'], $item['id']);
                }
            }
        }

        updateTree($repo, $tree);

        $this->em->flush();

        echo json_encode(array(
            'status' => true,
            'partialUrl' => base_url() . "admin/menu/menuTree",
            'parent' => "#menu_tree"
        ));
        exit();
    }

}
