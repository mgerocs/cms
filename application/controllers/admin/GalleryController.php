<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryController extends Common {

    private $folder;
    private $allowed_types;
    private $upload_path;

    function __construct() {
        parent::__construct();        

        $this->folder = "admin/gallery/";

        $this->allowed_types = 'gif|jpg|jpeg|png';
        $this->upload_path = realpath(APPPATH . '../');
    }

//    function galleries() {
//        $this->is_logged_in();
//        $content = $this->folder . "gallery-layout";
//        $data['gallery_array'] = $this->folder . "gallery-array";
//        $data['lang'] = $this->session->userdata('language')->id;
//        if ($this->input->is_ajax_request()) {
//            $this->load->view($content, $data);
//        } else {
//            $data['content'] = $content;
//            $this->load->view('admin/_layout/default', $data);
//        }
//    }

    function images($gallery_id) {
        try {
            $this->is_logged_in();
            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            if (!is_numeric($gallery_id)) {
                $type = $gallery_id;
                $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => $type));
                if (empty($galleryType)) {
                    throw new Exception("GalleryType not found.");
                }

                $gallery = $this->em->getRepository('Entity\Gallery')->findOneBy(array('type' => $galleryType));
                if (empty($gallery)) {
                    throw new Exception("Gallery not found.");
                }
                
                $gallery_id = $gallery->getId();
            }

            $content = $this->folder . "image-layout";
            $data['controls'] = $this->folder . "image-controls";
            $data['image_array'] = $this->folder . "image-array";
            $data['lang'] = $this->session->userdata('language')->id;
            $data['images'] = $this->getImages($gallery_id);
            $data['gallery'] = $this->getGalleryInfo($gallery_id);
            if ($this->input->is_ajax_request()) {
                $this->load->view($content, $data);
            } else {
                $data['content'] = $content;
                $this->load->view('admin/_layout/default', $data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function slider() {
        try {
            $this->is_logged_in();
            $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => "slider"));
            if (empty($galleryType)) {
                throw new Exception("GalleryType not found.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->findOneBy(array('type' => $galleryType));
            if (empty($gallery)) {
                throw new Exception("Gallery not found.");
            }
            $this->images($gallery->getId());
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function imageArray($gallery_id) {
        $data['images'] = $this->getImages($gallery_id);
        $data['gallery'] = $this->getGalleryInfo($gallery_id);
        $content = $this->folder . "image-array";
        $this->load->view($content, $data);
    }

    function galleryDialog($langswitch = false) {
        $content = $this->folder . "gallery-dialog";
        $data['categories'] = $this->getGalleryCategories($this->session->userdata('language')->id);
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $data['category_options'] = $this->folder . "category-options";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryDialog($langswitch = false) {
        $data['categories'] = $this->getGalleryCategories($this->session->userdata('language')->id);
        $data['categoriesofgalleries'] = $this->getCategoriesOfGalleries();
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $content = $this->folder . "category-dialog";
        $data['category_list'] = $this->folder . "category-list";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function imageDialog($langswitch = true) {
        $data['languages'] = $this->getLanguages();
        $data['lang'] = $this->session->userdata('language')->id;
        $content = $this->folder . "image-dialog";
        $data['langswitch'] = $langswitch;
        $this->load->view($content, $data);
    }

    function categoryList($lang_id) {
        $data['categories'] = $this->getGalleryCategories($lang_id);
        $data['categoriesofgalleries'] = $this->getCategoriesOfGalleries();
        $data['languages'] = $this->getLanguages();
        $content = $this->folder . "category-list";
        $this->load->view($content, $data);
    }

    function categoryOptions($lang_id) {
        $data['categories'] = $this->getGalleryCategories($lang_id);
        $content = $this->folder . "category-options";
        $this->load->view($content, $data);
    }

    function validateGalleryForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('name', 'Név', 'trim|required');

            (!empty($this->input->post('description'))) ? $this->form_validation->set_rules('description', 'Leírás', 'required') : $data['description'] = NULL;

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertGallery($data);
            } else {
                $this->updateGallery($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateCategoryForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('category', 'Kategória', 'trim|required');

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            if ($data['op'] == "new") {
                $this->insertGalleryCategory($data);
            } else {
                $this->updateGalleryCategory($data);
            }
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function validateImageForm() {
        try {
            $data = $this->input->post();

            if (empty($data)) {
                throw new Exception("No data posted.");
            }

            $this->form_validation->set_rules('name', 'Cím', 'trim');

            (!empty($this->input->post('name'))) ? $this->form_validation->set_rules('name', 'Cím', 'required') : $data['name'] = NULL;
            (!empty($this->input->post('description'))) ? $this->form_validation->set_rules('description', 'Leírás', 'required') : $data['description'] = NULL;

            $errors = array();

            if ($this->form_validation->run() == FALSE) {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $image = $this->em->getRepository('Entity\Image')->find($data['id']);
            if (!$image) {
                throw new Exception("Image not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $imageTr = $this->em->getRepository('Entity\ImageTr')->findOneBy(array('image' => $image, 'language' => $language));
            if (!$imageTr) {
                $imageTr = new Entity\ImageTr;
                $imageTr->setName($data['name']);
                $imageTr->setDescription($data['description']);
                $imageTr->setImage($image);
                $imageTr->setLanguage($language);
                $this->em->persist($imageTr);
            } else {
                $imageTr->setName($data['name']);
                $imageTr->setDescription($data['description']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Kép módosítása sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertGallery($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            //find GalleryType
            $galleryType = $this->em->getRepository('Entity\GalleryType')->findOneBy(array('type' => "custom"));
            if (empty($galleryType)) {
                throw new Exception("GalleryType not found.");
            }

            //find GalleryCategory
            $galleryCategory = $this->em->getRepository('Entity\GalleryCategory')->find($data['category_id']);

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\Gallery');

            //insert Gallery
            $gallery = new Entity\Gallery;
            $gallery->setType($galleryType);
            $gallery->setCategory($galleryCategory);
            $gallery->setWeight($weight);
            $this->em->persist($gallery);
            $this->em->flush();

            //insert GalleryTr
            $galleryTr = new Entity\GalleryTr;
            $galleryTr->setName($data['name']);
            $galleryTr->setDescription($data['description']);
            $galleryTr->setGallery($gallery);
            $galleryTr->setLanguage($language);
            $this->em->persist($galleryTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Új galéria létrehozva.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateGallery($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($data['id']);
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $galleryCategory = $this->em->getRepository('Entity\GalleryCategory')->find($data['category_id']);
            $gallery->setCategory($galleryCategory);

            $galleryTr = $this->em->getRepository('Entity\GalleryTr')->findOneBy(array('gallery' => $gallery, 'language' => $language));
            if (!$galleryTr) {
                $galleryTr = new Entity\GalleryTr;
                $galleryTr->setName($data['name']);
                $galleryTr->setDescription($data['description']);
                $galleryTr->setGallery($gallery);
                $galleryTr->setLanguage($language);
                $this->em->persist($galleryTr);
            } else {
                $galleryTr->setName($data['name']);
                $galleryTr->setDescription($data['description']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Galéria módosítása sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteGallery($gallery_id) {
        try {
            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($gallery_id);
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $images = $this->getImages($gallery_id);

            $galleryType = $this->em->getRepository('Entity\GalleryType')->find($gallery->getType());
            if (!$galleryType) {
                throw new Exception("GalleryType not found.");
            }

            $folder = $galleryType->getType();

            $delete_path = $this->upload_path . "/images/" . $folder . "/";

            if (!empty($images)) {
                foreach ($images as $image) {
                    if (file_exists($delete_path . $image['filename'])) {
                        unlink($delete_path . $image['filename']);
                        unlink($delete_path . "thumbs/" . $image['filename']);
                    }
                }
            }

            $this->em->remove($gallery);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Galéria törlése sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function insertGalleryCategory($data) {
        try {

            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            $weight = $this->getMaxWeight('Entity\GalleryCategory');

            //insert GalleryCategory
            $galleryCategory = new Entity\GalleryCategory;
            $galleryCategory->setWeight($weight);
            $this->em->persist($galleryCategory);
            $this->em->flush();

            //insert GalleryCategoryTr
            $galleryCategoryTr = new Entity\GalleryCategoryTr;
            $galleryCategoryTr->setCategory($data['category']);
            $galleryCategoryTr->setGalleryCategory($galleryCategory);
            $galleryCategoryTr->setLanguage($language);
            $this->em->persist($galleryCategoryTr);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Új kategória létrehozva.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function updateGalleryCategory($data) {

        try {
            if (empty($data)) {
                throw new Exception("Parameter is missing.");
            }

            $galleryCategory = $this->em->getRepository('Entity\GalleryCategory')->find($data['category_id']);
            if (!$galleryCategory) {
                throw new Exception("GalleryCategory not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->find($data['lang_id']);
            if (!$language) {
                throw new Exception("Language not found.");
            }

            $galleryCategoryTr = $this->em->getRepository('Entity\GalleryCategoryTr')->findOneBy(array('galleryCategory' => $galleryCategory, 'language' => $language));
            if (!$galleryCategoryTr) {
                $galleryCategoryTr = new Entity\GalleryCategoryTr;
                $galleryCategoryTr->setCategory($data['category']);
                $galleryCategoryTr->setGalleryCategory($galleryCategory);
                $galleryCategoryTr->setLanguage($language);
                $this->em->persist($galleryCategoryTr);
            } else {
                $galleryCategoryTr->setCategory($data['category']);
            }

            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Kategória módosítása sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteGalleryCategory($category_id) {
        try {
            if (empty($category_id)) {
                throw new Exception("Parameter is missing.");
            }

            $galleryCategory = $this->em->getRepository('Entity\GalleryCategory')->find($category_id);
            if (!$galleryCategory) {
                throw new Exception("GalleryCategory not found.");
            }

            $this->em->remove($galleryCategory);
            $this->em->flush();

            echo json_encode(array('status' => true, 'message' => "Kategória törlése sikeres.", 'context' => "success"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function deleteImage($gallery_id, $image_id) {
        try {
            if (empty($gallery_id) || empty($image_id)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($gallery_id);
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $galleryType = $this->em->getRepository('Entity\GalleryType')->find($gallery->getType());
            if (!$galleryType) {
                throw new Exception("GalleryType not found.");
            }

            $image = $this->em->getRepository('Entity\Image')->find($image_id);
            if (!$image) {
                throw new Exception("Image not found.");
            }

            $this->em->remove($image);
            $this->em->flush();

            $folder = $galleryType->getType();

            $delete_path = $this->upload_path . "/images/" . $folder . "/";

            if (file_exists($delete_path . $image->getFilename())) {
                unlink($delete_path . $image->getFilename());
                unlink($delete_path . "thumbs/" . $image->getFilename());
            }

            echo json_encode(array(
                'status' => true,
                'message' => "Kép törlése sikeres.",
                'context' => "success",
                'partialUrl' => base_url() . "admin/galleries/imageArray/" . $gallery_id,
                'parent' => "#image_array"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getGalleries($gallery_id = null, $lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Gallery');
            $qb = $repo->createQueryBuilder('g');

            $qb->select('g.id', 'g.status', 'g.weight', 'g.create_date', 'gtr.name', 'gtr.description', 'gc.id AS category_id', 'gctr.category', 'gt.id AS type_id', 'gt.type', 'i.filename');
            $qb->leftJoin('g.translations', 'gtr', 'WITH', 'IDENTITY (gtr.language) = :lang_id OR gtr.id IS NULL');
            $qb->leftJoin('g.category', 'gc');
            $qb->leftJoin('gc.translations', 'gctr', 'WITH', 'IDENTITY (gctr.language) = :lang_id OR gctr.id IS NULL');
            $qb->leftJoin('g.type', 'gt');
            $qb->leftJoin('g.images', 'i', 'WITH', 'i.cover = 1');
            if (!empty($gallery_id)) {
                $qb->andWhere('g.id = :id');
                $qb->setParameter('id', $gallery_id);
            }
            $qb->andWhere('gt.type = :type');
            $qb->setParameter('type', "custom");
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('g.weight', 'ASC');

            $galleries = $qb->getQuery()->getArrayResult();

            echo json_encode(array('status' => true, 'data' => $galleries));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getGalleryCategories($lang_id = null, $category_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\GalleryCategory');
            $qb = $repo->createQueryBuilder('gc');

            $qb->select('gc.id', 'gctr.category');
            $qb->leftJoin('gc.translations', 'gctr', 'WITH', 'IDENTITY (gctr.language) = :lang_id OR gctr.id IS NULL');
            if (!empty($category_id)) {
                $qb->andWhere('gc.id = :id');
                $qb->setParameter('id', $category_id);
            }
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('gc.weight', 'ASC');

            $galleryCategories = $qb->getQuery()->getArrayResult();

            return $galleryCategories;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getCategoriesOfGalleries() {
        try {
            $repo = $this->em->getRepository('Entity\Gallery');
            $qb = $repo->createQueryBuilder('g');

            $qb->select('IDENTITY (g.category)');
            $qb->distinct();

            $categories = $qb->getQuery()->getArrayResult();

            $id_array = array();
            $cntr = 0;

            foreach ($categories as $key => $val) {
                $temp = array_values($val);
                $id_array[$cntr] = $temp[0];
                $cntr++;
            }

            return $id_array;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function reorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\Gallery');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function categoryReorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\GalleryCategory');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function imageReorder() {
        try {
            $data = $this->input->post();
            if (empty($data)) {
                throw new Exception("No posted data.");
            }
            $this->reorderEntity($data, 'Entity\Image');
            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setStatus($gallery_id, $status) {
        try {
            if (empty($gallery_id)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($gallery_id);
            if (!$gallery) {
                throw new Exception("Gallery not found.");
            }

            $gallery->setStatus($status);
            $this->em->flush();

            echo json_encode(array('status' => true));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getImages($gallery_id, $image_id = null, $lang_id = null) {
        try {

            if (empty($lang_id)) {
                $lang_id = $this->session->userdata('language')->id;
            }

            $repo = $this->em->getRepository('Entity\Image');
            $qb = $repo->createQueryBuilder('i');

            $qb->select('i.id', 'i.filename', 'i.weight', 'i.create_date', 'i.cover', 'IDENTITY (i.gallery) AS gallery_id', 'itr.name', 'itr.description', 'gt.type AS type');
            $qb->leftJoin('i.translations', 'itr', 'WITH', 'IDENTITY (itr.language) = :lang_id OR itr.id IS NULL');
            $qb->leftJoin('i.gallery', 'g');
            $qb->leftJoin('g.type', 'gt');
            $qb->where('i.gallery = :gallery_id');
            if (!empty($image_id)) {
                $qb->andWhere('i.id = :id');
                $qb->setParameter('id', $image_id);
            }
            $qb->setParameter('gallery_id', $gallery_id);
            $qb->setParameter('lang_id', $lang_id);
            $qb->orderBy('i.weight', 'ASC');

            $images = $qb->getQuery()->getArrayResult();

            if (!empty($image_id)) {
                echo json_encode(array('status' => true, 'data' => $images));
                exit();
            }

            return $images;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function getGalleryInfo($gallery_id) {
        try {

            $lang_id = $this->session->userdata('language')->id;

            $repo = $this->em->getRepository('Entity\Gallery');
            $qb = $repo->createQueryBuilder('g');

            $qb->select('g.id', 'gtr.name', 'gt.id AS type_id', 'gt.type AS type');
            $qb->leftJoin('g.translations', 'gtr', 'WITH', 'IDENTITY (gtr.language) = :lang_id OR gtr.id IS NULL');
            $qb->leftJoin('g.type', 'gt');
            $qb->andWhere('g.id = :id');
            $qb->setParameter('id', $gallery_id);
            $qb->setParameter('lang_id', $lang_id);

            $gallery = $qb->getQuery()->getOneOrNullResult();
            if (empty($gallery)) {
                throw new Exception("Gallery not found.");
            }

            $gallery = (object) $gallery;

            if ($gallery->type == "slider") {
                $gallery->name = "Slider";
            }

            return $gallery;
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function upload() {
        try {
            //acquire gal_id and type_id
            $gallery_id = $this->input->post('gallery_id');
            $type_id = $this->input->post('type_id');

            if (empty($gallery_id) || empty($type_id)) {
                throw new Exception("Parameter is missing.");
            }

            $gallery = $this->em->getRepository('Entity\Gallery')->find($gallery_id);
            if (empty($gallery)) {
                throw new Exception("Gallery not found.");
            }

            $galleryType = $this->em->getRepository('Entity\GalleryType')->find($type_id);
            if (empty($galleryType)) {
                throw new Exception("GalleryType not found.");
            }

            $language = $this->em->getRepository('Entity\SiteLanguage')->findOneBy(array('def' => 1));
            if (empty($language)) {
                throw new Exception("Language not found.");
            }

            //acquire folder name corresponding to type_id (project, slider, etc.)
            $folder = $galleryType->getType();

            //construct upload path
            $upload_path = $this->upload_path . "/images/" . $folder . "/";

            //convert filename accentless
            $this->load->helper("text");
            $filename = $_FILES["userfile"]["name"];
            $filename = convert_accented_characters($filename);

            $errors = array();

            $config = array(
                'upload_path' => $upload_path,
                'allowed_types' => $this->allowed_types,
                'file_name' => $filename
            );

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $errors = strip_tags($this->upload->display_errors());
                echo json_encode(array('status' => false, 'errors' => $errors));
                exit();
            }

            $fileinfo = $this->upload->data();

            // RESIZING 
            $thumbconfig = array(
                'source_image' => $fileinfo['full_path'], //get original image
                'new_image' => $upload_path . 'thumbs', //save as new image //need to create thumbs first
                'maintain_ratio' => true,
                'width' => 200,
                'height' => 200
            );
            $this->load->library('image_lib', $thumbconfig);
            $this->image_lib->resize(); //generating thumb
            $this->image_lib->clear();

            $weight = $this->getMaxWeight('Entity\Image');

            //insert Image
            $image = new Entity\Image;
            $image->setGallery($gallery);
            $image->setWeight($weight);
            $image->setFilename($fileinfo['file_name']);
            $image->setExt($fileinfo['file_ext']);
            $image->setSize($fileinfo['file_size']);
            $this->em->persist($image);
            $this->em->flush();

            //insert (empty) ImageTr
            $imageTr = new Entity\ImageTr;
            $imageTr->setImage($image);
            $imageTr->setLanguage($language);
            $this->em->persist($imageTr);
            $this->em->flush();

            echo json_encode(array(
                'status' => true,
                'message' => "Kép feltöltése sikeres.",
                'context' => "success",
                'partialUrl' => base_url() . "admin/galleries/imageArray/" . $gallery_id,
                'parent' => "#image_array"));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

    function setCover($gallery_id, $image_id) {
        try {

            if (empty($gallery_id) || empty($image_id)) {
                throw new Exception("Parameter  is missing.");
            }

            $repo = $this->em->getRepository('Entity\Image');
            $qb = $repo->createQueryBuilder('i');

            $qb->update('Entity\Image', 'i');
            $qb->set('i.cover', $qb->expr()->literal(0));
            $qb->where('i.gallery = :id');
            $qb->setParameter('id', $gallery_id);
            $qb->getQuery()->execute();

            $image = $this->em->getRepository('Entity\Image')->find($image_id);
            if (empty($image)) {
                throw new Exception("Image not found.");
            }

            $image->setCover(1);
            $this->em->flush();

            echo json_encode(array(
                'status' => true
            ));
            exit();
        } catch (Exception $ex) {
            $this->sendErrorMessage($ex->getMessage());
        }
    }

}
