
lightbox.option({
    'showImageNumberLabel': false
});

$(document).ready(function () {

    $('#fullpage').fullpage({
        //Navigation
        menu: false,
        lockAnchors: false,
        //anchors: ['home', 'paintings', 'drawings', 'about_me'],
        navigation: true,
        navigationPosition: 'right',
        //navigationTooltips: [],
        showActiveTooltip: false,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',
        //Scrolling
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        fitToSection: true,
        fitToSectionDelay: 1000,
        scrollBar: false,
        easing: 'easeInOutCubic',
        easingcss3: 'ease',
        loopBottom: false,
        loopTop: false,
        loopHorizontal: false,
        continuousVertical: false,
        //normalScrollElements: '',
        scrollOverflow: false,
        touchSensitivity: 15,
        //normalScrollElements: '#images',
        normalScrollElementTouchThreshold: 5,
        //Accessibility
        keyboardScrolling: true,
        animateAnchor: true,
        recordHistory: true,
        //Design
        controlArrows: true,
        verticalCentered: true,
        resize: false,
        //sectionsColor: [],
        paddingTop: 0,
        paddingBottom: 0,
        fixedElements: '',
        responsiveWidth: 0,
        responsiveHeight: 0,
        //Custom selectors
        sectionSelector: '.section',
        slideSelector: '.slide',
        //events
        /*afterLoad: function(anchorLink, index){},         
         afterResize: function(){},
         afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){},
         onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){}*/
        afterRender: function () {
            $(".fp-next").html('<i class="fa fa-chevron-right"></i>');
            $(".fp-prev").html('<i class="fa fa-chevron-left"></i>');
        },
        onLeave: function (index, nextIndex, direction) {   
            $(".fp-slidesNav").hide();
            var $element = $(".ibg-bg, #home_controls");
            var $arrowDown = $(".arrow.down");
            if (nextIndex > 1) {
                $arrowDown.fadeOut(300);
                $element.animate({opacity: 0}, 500, 'easeInOutCubic', function() {
                    
                });
            } else {
                $arrowDown.fadeIn(300);
                $element.animate({opacity: 1}, 500, 'easeInOutCubic', function() {
                    
                });
            }            
            var $slide = $(".section:eq(" + (nextIndex - 1) + ") .slide").first();
            var gallery_id = $slide.find("#gallery").data("gallery_id");
            if (gallery_id !== undefined) {                
                getImages(gallery_id, $slide);
            }
        },
        afterLoad: function(anchorLink, index) {
            $(".fp-slidesNav").show();
        },
        onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
            var $slide = $(".section:eq(" + (index - 1) + ") .slide:eq(" + nextSlideIndex + ")");
            var gallery_id = $slide.find("#gallery").data("gallery_id");
            if (gallery_id !== undefined) {
                getImages(gallery_id, $slide);
            }
        }
    });


    $(".bg").interactive_bg({
        strength: 15,
        scale: 1.03,
        animationSpeed: "200ms",
        contain: true,
        wrapContent: false
    });
    // change background size on window resize
    $(window).resize(function () {
        $(".bg > .ibg-bg").css({
            width: $(".bg").outerWidth(),
            height: $(".bg").outerHeight()
        });
    });

    $("body").on("click", ".arrow.down", function () {
        $.fn.fullpage.moveSectionDown();
    });

    $(window).mousemove(function (e) {       
        if (e.pageY > $(window).height() - 32) {
            $(".fp-controlArrow.fp-prev").css("left", "2em");
            $(".fp-controlArrow.fp-next").css("right", "2em");
        }
        else {
            $(".fp-controlArrow.fp-prev").css("left", "-2em");
            $(".fp-controlArrow.fp-next").css("right", "-2em");
        }
    });

    $(".langswitch a").click(function (e) {
        e.preventDefault();        
        var $element = $(this);
        if ($element.hasClass("active")) {
            return false;
        }
        $(".langswitch a").removeClass("active");
        $element.addClass("active");
        var href = $element.attr('href');
        window.location.href = href;
    });    

});


function getImages(gallery_id, $slide) {
    var request = $.ajax({
        type: "post",
        async: true,
        url: base_url + "images/" + gallery_id,
        cache: false,
        //data: data,
        dataType: "html",
        beforeSend: function () {
            $('#loader').show();
        }
    });
    request.done(function (response) {
        $('#loader').hide();
        var wheight = $(window).height();
        $slide.find("#images").html(response);
        $slide.find('.flex-images').flexImages({
            rowHeight: wheight / 4,
            maxRows: 3
        });
    });
    request.fail(function (jqXHR, textStatus) {
        alert(jqXHR.status + " " + textStatus);
    });
}