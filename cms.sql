-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Hoszt: 127.0.0.1
-- Létrehozás ideje: 2016. Már 14. 18:31
-- Szerver verzió: 5.6.21
-- PHP verzió: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `cms`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `biography`
--

CREATE TABLE IF NOT EXISTS `biography` (
`id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `end` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_progress` tinyint(1) NOT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `biography`
--

INSERT INTO `biography` (`id`, `category_id`, `start`, `end`, `in_progress`, `url`, `create_date`, `status`, `weight`) VALUES
(1, 1, '2013', NULL, 1, NULL, '2016-03-06 16:48:50', 1, 2),
(3, 2, '2014', '2015', 1, NULL, '2016-03-06 17:31:26', 1, 3),
(4, 2, '2014', NULL, 0, 'dfsdfsdf', '2016-03-06 17:39:10', 1, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `bio_categories`
--

CREATE TABLE IF NOT EXISTS `bio_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `bio_categories`
--

INSERT INTO `bio_categories` (`id`, `create_date`, `weight`) VALUES
(1, '2016-03-06 14:03:22', 2),
(2, '2016-03-06 17:34:29', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `bio_categories_tr`
--

CREATE TABLE IF NOT EXISTS `bio_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `bio_categories_tr`
--

INSERT INTO `bio_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 1, 'csoportos performanszok'),
(2, 5, 1, 'group performances'),
(3, 1, 2, 'szóló performanszok');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `bio_tr`
--

CREATE TABLE IF NOT EXISTS `bio_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `bio_id` int(10) unsigned DEFAULT NULL,
  `field1` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `field2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field3` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field4` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `bio_tr`
--

INSERT INTO `bio_tr` (`id`, `language_id`, `bio_id`, `field1`, `field2`, `field3`, `field4`) VALUES
(1, 1, 1, 'test field', NULL, NULL, NULL),
(3, 1, 3, 'test field 1', NULL, NULL, NULL),
(4, 1, 4, 'dsfsdfsdf', NULL, 'sdfsdfsdf', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
`captcha_id` bigint(13) unsigned NOT NULL,
  `captcha_time` int(10) unsigned NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=471 DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(464, 1457955388, '::1', '348097'),
(465, 1457955674, '::1', '607932'),
(466, 1457955699, '::1', '539670'),
(467, 1457956217, '::1', '295860'),
(468, 1457956667, '::1', '740816'),
(469, 1457956807, '::1', '917482'),
(470, 1457957309, '::1', '508743');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('2a4541cb44b9dbc7efe3c3142f6af01c0e6677a5', '::1', 1457632971, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633323933363b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('6ea62e16f8358b5574dbb07a375d6710792ef239', '::1', 1457633572, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633333537323b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('8f05bf346a8cb1731e8d21fc82e46984988588d5', '::1', 1457634055, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633343035353b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('110b2b1744c298fa487eca50baf9af6c2287b9e0', '::1', 1457634384, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633343338343b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('51a043b83ffed1a55414521cfa382c746b207e67', '::1', 1457634744, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633343734343b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('38fa046b8f1ba1f806e3b91c54b35f1fca94229c', '::1', 1457635168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633353136383b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('23cad5e155a9f20ace53e777f41a789eed125a50', '::1', 1457635514, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633353531343b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('212069ec06e58234718efe3069e4ecceede7d61b', '::1', 1457635941, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633353934313b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('b18d16a752441088e6d309428b8b36212bc8a495', '::1', 1457637158, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633373135383b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('5e0c9893c94a47748263e87acb3caff4809757e0', '::1', 1457638871, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633383837313b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('d75a4ad08f4d28d35af9d508d7b3adb29debeaf6', '::1', 1457639269, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633393236393b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('97afd11d1a655a2c1cd82493d3ee0cef2c3d807c', '::1', 1457639651, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373633393635313b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('ca1fc52998e36083e285ee4d630a04e093942c10', '::1', 1457640479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373634303136333b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('e86a2ab177186ac7ace7f6a9a227bc120411bc56', '::1', 1457640639, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373634303633393b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('a9cc613440fb2a62c9497171dfccfdce736c2a9f', '::1', 1457641152, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373634313135313b),
('8b3a140aefd4c8fccbcbadfdb77282c2a2f60e50', '::1', 1457955406, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935353338373b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('c43343ab544a49fdc938aa9970772239beba3f73', '::1', 1457955698, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935353639383b),
('45c30a0709a05ec4c676caac2124d349b31126a5', '::1', 1457955705, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935353730353b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('2965954df17f1eb494377f4952ae8b2a8c6cd87b', '::1', 1457956185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935363138353b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('b1eaa2244d658c995eaf6b35c28c89fe624d76a0', '::1', 1457956216, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935363231363b),
('1a69155d141ea98755d493bdf94d34a004a3b77e', '::1', 1457956816, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935363636363b69735f6c6f676765645f696e7c623a313b757365727c4f3a383a22737464436c617373223a363a7b733a323a226964223b693a313b733a383a226c6173746e616d65223b733a303a22223b733a393a2266697273746e616d65223b733a383a224dc3a17479c3a173223b733a353a22656d61696c223b733a31373a226d6765726f637340676d61696c2e636f6d223b733a383a2267726f75705f6964223b693a313b733a353a2267726f7570223b733a353a2261646d696e223b7d6c616e67756167657c4f3a383a22737464436c617373223a343a7b733a323a226964223b693a313b733a383a226c616e6775616765223b733a363a226d6167796172223b733a353a226c6162656c223b733a393a2248756e67617269616e223b733a343a22636f6465223b733a323a226875223b7d),
('a56fcbe23e696a08ccfb84a2743e4ecf80930da4', '::1', 1457957309, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373935373330383b);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `events`
--

CREATE TABLE IF NOT EXISTS `events` (
`id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `venue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `start_time` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_time` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url1` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url2` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `events_tr`
--

CREATE TABLE IF NOT EXISTS `events_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `event_categories`
--

CREATE TABLE IF NOT EXISTS `event_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `event_categories_tr`
--

CREATE TABLE IF NOT EXISTS `event_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
`id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `galleries`
--

INSERT INTO `galleries` (`id`, `type_id`, `create_date`, `category_id`, `status`, `weight`) VALUES
(49, 2, '2016-01-25 00:00:00', NULL, 1, NULL),
(50, 1, '2016-02-24 15:00:41', NULL, 1, NULL),
(51, 1, '2016-02-24 15:49:07', NULL, 1, NULL),
(52, 1, '2016-02-24 15:49:23', NULL, 1, NULL),
(53, 1, '2016-02-25 09:50:03', NULL, 1, NULL),
(54, 1, '2016-02-25 09:50:16', NULL, 1, NULL),
(55, 1, '2016-02-25 17:26:07', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galleries_tr`
--

CREATE TABLE IF NOT EXISTS `galleries_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `galleries_tr`
--

INSERT INTO `galleries_tr` (`id`, `language_id`, `gallery_id`, `name`, `description`) VALUES
(48, 1, 50, 'TestProject', NULL),
(49, 1, 51, 'Teszt Project2', NULL),
(50, 1, 52, 'Teszt Project34', NULL),
(51, 1, 53, 'Awesome Project', NULL),
(52, 1, 54, 'Szuper Project', NULL),
(53, 1, 55, 'Plus One', NULL),
(54, 5, 52, 'English348', NULL),
(55, 5, 51, 'English2', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories`
--

CREATE TABLE IF NOT EXISTS `gallery_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_categories`
--

INSERT INTO `gallery_categories` (`id`, `create_date`, `weight`) VALUES
(3, '2016-01-19 15:23:51', 1),
(4, '2016-01-19 15:28:05', 2),
(5, '2016-01-26 17:29:08', 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_categories_tr`
--

CREATE TABLE IF NOT EXISTS `gallery_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_categories_tr`
--

INSERT INTO `gallery_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 3, 'test2'),
(2, 1, 4, 'élmények'),
(3, 1, 5, 'tánc6'),
(4, 5, 5, 'dance');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery_types`
--

CREATE TABLE IF NOT EXISTS `gallery_types` (
`id` int(10) unsigned NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `gallery_types`
--

INSERT INTO `gallery_types` (`id`, `type`) VALUES
(3, 'custom'),
(1, 'project'),
(2, 'slider');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(10) unsigned NOT NULL,
  `group` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `groups`
--

INSERT INTO `groups` (`id`, `group`) VALUES
(1, 'admin'),
(2, 'editor');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images`
--

CREATE TABLE IF NOT EXISTS `images` (
`id` int(10) unsigned NOT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `cover` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `images`
--

INSERT INTO `images` (`id`, `gallery_id`, `filename`, `ext`, `size`, `cover`, `weight`, `create_date`) VALUES
(39, 49, 'icon_gears.png', '.png', 15, 1, 3, '2016-01-25 14:28:09'),
(40, 49, 'kozbeszerzes.png', '.png', 65, 0, 1, '2016-01-25 14:29:19'),
(48, 49, 'elerhetoseg.png', '.png', 14, 0, 2, '2016-01-28 15:15:27'),
(54, 53, 'bor.jpg', '.jpg', 3567, 1, 1, '2016-02-25 16:05:46'),
(55, 53, 'daralo.jpg', '.jpg', 4349, 0, 3, '2016-02-25 16:05:46'),
(57, 54, 'tanc3.jpg', '.jpg', 4678, 0, 7, '2016-02-25 16:07:43'),
(58, 51, 'self.jpg', '.jpg', 597, 0, 8, '2016-03-10 19:13:17'),
(59, 51, 'longneck_whole.jpg', '.jpg', 555, 0, 9, '2016-03-10 19:13:23'),
(61, 52, 'dance1.jpg', '.jpg', 5808, 0, 11, '2016-03-10 19:14:37'),
(62, 52, 'filc11.jpg', '.jpg', 1012, 0, 12, '2016-03-10 19:14:37'),
(63, 52, 'filc21.jpg', '.jpg', 479, 0, 13, '2016-03-10 19:14:38'),
(67, 51, 'inverz1.jpg', '.jpg', 1163, 0, 16, '2016-03-10 19:22:13'),
(69, 51, 'kontur21.png', '.png', 1205, 0, 18, '2016-03-10 19:22:43'),
(70, 51, 'longneck_head2.jpg', '.jpg', 166, 0, 19, '2016-03-10 19:23:48'),
(71, 51, 'inverz11.jpg', '.jpg', 1163, 0, 20, '2016-03-10 19:23:54'),
(72, 51, 'kontur11.png', '.png', 2027, 0, 21, '2016-03-10 19:23:55'),
(73, 51, 'background.jpg', '.jpg', 540, 0, 22, '2016-03-10 19:24:17'),
(76, 53, 'filc12.jpg', '.jpg', 1012, 0, 25, '2016-03-10 19:26:42'),
(80, 53, 'filc32.jpg', '.jpg', 617, 0, 29, '2016-03-10 19:28:03'),
(81, 54, 'background1.jpg', '.jpg', 540, 0, 30, '2016-03-10 19:28:31'),
(82, 54, 'filc33.jpg', '.jpg', 617, 0, 31, '2016-03-10 19:28:40'),
(83, 54, 'inverz12.jpg', '.jpg', 1163, 0, 32, '2016-03-10 19:28:40'),
(84, 54, 'inverz2_nobg.png', '.png', 761, 0, 33, '2016-03-10 19:28:41'),
(85, 54, 'kontur12.png', '.png', 2027, 0, 34, '2016-03-10 19:28:42'),
(86, 54, 'home_page1.jpg', '.jpg', 485, 0, 35, '2016-03-10 19:28:50'),
(87, 54, 'inverz13.jpg', '.jpg', 1163, 0, 36, '2016-03-10 19:28:51'),
(88, 54, 'inverz1_nobg.png', '.png', 532, 0, 37, '2016-03-10 19:28:51'),
(89, 54, 'inverz2_nobg1.png', '.png', 761, 0, 38, '2016-03-10 19:28:52'),
(90, 51, 'filc14.jpg', '.jpg', 1012, 0, 39, '2016-03-10 19:44:24'),
(92, 50, 'filc13.jpg', '.jpg', 1012, 0, 40, '2016-03-10 19:47:36'),
(93, 50, 'home_page.jpg', '.jpg', 485, 0, 41, '2016-03-10 19:47:36'),
(94, 53, 'inverz14.jpg', '.jpg', 1163, 0, 42, '2016-03-10 19:52:27');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `images_tr`
--

CREATE TABLE IF NOT EXISTS `images_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `image_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `images_tr`
--

INSERT INTO `images_tr` (`id`, `language_id`, `image_id`, `name`, `description`) VALUES
(42, 1, 39, 'magyar cím', NULL),
(43, 1, 40, NULL, NULL),
(51, 5, 39, 'english title', NULL),
(54, 1, 48, NULL, NULL),
(60, 1, 54, 'Bor', NULL),
(61, 1, 55, 'Daráló', NULL),
(63, 1, 57, NULL, NULL),
(65, 5, 55, 'Grinder', NULL),
(66, 5, 54, 'Wine', NULL),
(67, 1, 58, NULL, NULL),
(68, 1, 59, NULL, NULL),
(70, 1, 61, NULL, NULL),
(71, 1, 62, NULL, NULL),
(72, 1, 63, NULL, NULL),
(76, 1, 67, NULL, NULL),
(78, 1, 69, NULL, NULL),
(79, 1, 70, NULL, NULL),
(80, 1, 71, NULL, NULL),
(81, 1, 72, NULL, NULL),
(82, 1, 73, NULL, NULL),
(85, 1, 76, NULL, NULL),
(89, 1, 80, NULL, NULL),
(90, 1, 81, NULL, NULL),
(91, 1, 82, NULL, NULL),
(92, 1, 83, NULL, NULL),
(93, 1, 84, NULL, NULL),
(94, 1, 85, NULL, NULL),
(95, 1, 86, NULL, NULL),
(96, 1, 87, NULL, NULL),
(97, 1, 88, NULL, NULL),
(98, 1, 89, NULL, NULL),
(99, 1, 90, NULL, NULL),
(101, 1, 92, NULL, NULL),
(102, 1, 93, NULL, NULL),
(103, 1, 94, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `languages`
--

INSERT INTO `languages` (`id`, `code`, `language`, `label`) VALUES
(1, 'hu', 'magyar', 'Hungarian'),
(2, 'en', 'English', 'English'),
(3, 'de', 'Deutsch', 'German'),
(4, 'it', 'Italiano', 'Italian'),
(5, 'sp', 'Espanol', 'Spanish'),
(6, 'cz', 'cestina', 'Czech');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata`
--

CREATE TABLE IF NOT EXISTS `metadata` (
`id` int(11) NOT NULL,
  `logo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `metadata`
--

INSERT INTO `metadata` (`id`, `logo`, `favicon`, `author`) VALUES
(1, NULL, NULL, 'Gerőcs Mátyás');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `metadata_tr`
--

CREATE TABLE IF NOT EXISTS `metadata_tr` (
`id` int(10) unsigned NOT NULL,
  `meta_id` int(11) DEFAULT NULL,
  `keywords` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `metadata_tr`
--

INSERT INTO `metadata_tr` (`id`, `meta_id`, `keywords`, `description`, `title`, `language_id`) VALUES
(3, 1, 'keywords', NULL, 'cms', 1),
(4, 1, 'keywords', NULL, 'cms', 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `projects`
--

INSERT INTO `projects` (`id`, `category_id`, `gallery_id`, `status`, `weight`, `create_date`, `date`) VALUES
(40, 3, 50, 1, 3, '2016-02-24 15:00:41', '2016-02-24 00:00:00'),
(41, NULL, 51, 1, 1, '2016-02-24 15:49:07', '2016-02-20 00:00:00'),
(42, NULL, 52, 1, 2, '2016-02-24 15:49:23', '2017-02-18 00:00:00'),
(43, 2, 53, 1, 4, '2016-02-25 09:50:03', '2016-02-25 00:00:00'),
(44, NULL, 54, 1, 5, '2016-02-25 09:50:16', '2016-02-25 00:00:00'),
(45, 4, 55, 1, 6, '2016-02-25 17:26:08', '2016-05-14 00:00:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projects_tr`
--

CREATE TABLE IF NOT EXISTS `projects_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `project_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `projects_tr`
--

INSERT INTO `projects_tr` (`id`, `language_id`, `project_id`, `name`, `description`) VALUES
(56, 1, 40, 'Teszt Project', NULL),
(57, 1, 41, 'Teszt Project2', '<p>dsfdgdfg</p>\r\n\r\n<p><strong>dfgdfgdfg</strong></p>\r\n\r\n<blockquote>\r\n<p><strong>fdgdfgdfdfgdfgdfg</strong></p>\r\n</blockquote>\r\n'),
(58, 1, 42, 'Teszt Project34', '<h1>hfghfgh</h1>\r\n'),
(59, 1, 43, 'Nagyszerű Project', 'cxvxcvxcvv'),
(60, 1, 44, 'Szuper Project', NULL),
(61, 1, 45, 'Árvíztűrő Tükörfúrógép', NULL),
(62, 5, 41, 'English2', '<p>dsfdgdfg</p>\r\n\r\n<p><em>657567567</em></p>\r\n'),
(63, 5, 42, 'English348', NULL),
(64, 5, 40, 'Eng', NULL),
(65, 5, 43, 'Awesome Project', 'dsfsdfsdf'),
(66, 5, 44, 'Super Project', NULL),
(67, 5, 45, 'Plus One', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories`
--

CREATE TABLE IF NOT EXISTS `project_categories` (
`id` int(10) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `weight` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `project_categories`
--

INSERT INTO `project_categories` (`id`, `create_date`, `weight`) VALUES
(1, '2016-02-25 09:50:28', 2),
(2, '2016-02-25 09:50:38', 1),
(3, '2016-02-25 09:50:46', 3),
(4, '2016-02-25 17:26:36', 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_categories_tr`
--

CREATE TABLE IF NOT EXISTS `project_categories_tr` (
`id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `project_categories_tr`
--

INSERT INTO `project_categories_tr` (`id`, `language_id`, `category_id`, `category`) VALUES
(1, 1, 1, 'design'),
(2, 1, 2, 'movie'),
(3, 1, 3, 'performance'),
(4, 1, 4, 'plus one');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `recovery`
--

CREATE TABLE IF NOT EXISTS `recovery` (
`id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `used` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `recovery`
--

INSERT INTO `recovery` (`id`, `email`, `ip_address`, `token`, `used`, `create_date`) VALUES
(1, 'mgerocs@gmail.com', '::1', '021e03c6f811156c516a866a57a2c35a', 1, '2016-01-29 10:53:28'),
(2, 'mgerocs@email.hu', '::1', '2bfa28140fece3255fe3f5155c63d8bd', 0, '2016-01-29 11:03:29'),
(3, 'elektromagnes@email.com', '::1', 'f979d592b9672fdc7010d624dbdd8ba8', 0, '2016-01-29 11:05:51'),
(4, 'mgerocs@gmail.com', '::1', '223d30bcfbd4af302403a273dae949d3', 1, '2016-01-29 11:54:55');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `site_languages`
--

CREATE TABLE IF NOT EXISTS `site_languages` (
`id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `def` tinyint(1) NOT NULL,
  `weight` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `site_languages`
--

INSERT INTO `site_languages` (`id`, `lang_id`, `def`, `weight`) VALUES
(1, 1, 1, 3),
(5, 2, 0, 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `group_id` int(10) unsigned DEFAULT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `portrait` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `group_id`, `lastname`, `firstname`, `password`, `email`, `city`, `postal_code`, `address`, `phone1`, `phone2`, `portrait`) VALUES
(1, 1, '', 'Mátyás', '662c03c272ff006f12c9f16fc4992460c57585ee744bb093e0f7279ec7df5c01', 'mgerocs@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biography`
--
ALTER TABLE `biography`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_E3B3665C12469DE2` (`category_id`);

--
-- Indexes for table `bio_categories`
--
ALTER TABLE `bio_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bio_categories_tr`
--
ALTER TABLE `bio_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_4BFBC74082F1BAF4` (`language_id`), ADD KEY `IDX_4BFBC74012469DE2` (`category_id`);

--
-- Indexes for table `bio_tr`
--
ALTER TABLE `bio_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_912138F482F1BAF4` (`language_id`), ADD KEY `IDX_912138F49A99E1B9` (`bio_id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
 ADD PRIMARY KEY (`captcha_id`), ADD KEY `word` (`word`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_5387574A4E7AF8F` (`gallery_id`), ADD KEY `IDX_5387574A12469DE2` (`category_id`);

--
-- Indexes for table `events_tr`
--
ALTER TABLE `events_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_96D2C92882F1BAF4` (`language_id`), ADD KEY `IDX_96D2C928166D1F9C` (`project_id`);

--
-- Indexes for table `event_categories`
--
ALTER TABLE `event_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_categories_tr`
--
ALTER TABLE `event_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_8DF37D5682F1BAF4` (`language_id`), ADD KEY `IDX_8DF37D5612469DE2` (`category_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_F70E6EB7C54C8C93` (`type_id`), ADD KEY `IDX_F70E6EB712469DE2` (`category_id`);

--
-- Indexes for table `galleries_tr`
--
ALTER TABLE `galleries_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B12E9F3182F1BAF4` (`language_id`), ADD KEY `IDX_B12E9F314E7AF8F` (`gallery_id`);

--
-- Indexes for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_93CCF8882F1BAF4` (`language_id`), ADD KEY `IDX_93CCF8812469DE2` (`category_id`);

--
-- Indexes for table `gallery_types`
--
ALTER TABLE `gallery_types`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_BDAD59138CDE5729` (`type`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_E01FBE6A4E7AF8F` (`gallery_id`);

--
-- Indexes for table `images_tr`
--
ALTER TABLE `images_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_DFF7DCA882F1BAF4` (`language_id`), ADD KEY `IDX_DFF7DCA83DA5256D` (`image_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata`
--
ALTER TABLE `metadata`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metadata_tr`
--
ALTER TABLE `metadata_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D726301439FCA6F9` (`meta_id`), ADD KEY `IDX_D726301482F1BAF4` (`language_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_5C93B3A44E7AF8F` (`gallery_id`), ADD KEY `IDX_5C93B3A412469DE2` (`category_id`);

--
-- Indexes for table `projects_tr`
--
ALTER TABLE `projects_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_1566D61882F1BAF4` (`language_id`), ADD KEY `IDX_1566D618166D1F9C` (`project_id`);

--
-- Indexes for table `project_categories`
--
ALTER TABLE `project_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7D3B5AFC82F1BAF4` (`language_id`), ADD KEY `IDX_7D3B5AFC12469DE2` (`category_id`);

--
-- Indexes for table `recovery`
--
ALTER TABLE `recovery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_languages`
--
ALTER TABLE `site_languages`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_C6DB250CB213FA4` (`lang_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_1483A5E9E7927C74` (`email`), ADD KEY `IDX_1483A5E9FE54D947` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biography`
--
ALTER TABLE `biography`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bio_categories`
--
ALTER TABLE `bio_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bio_categories_tr`
--
ALTER TABLE `bio_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bio_tr`
--
ALTER TABLE `bio_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
MODIFY `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=471;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events_tr`
--
ALTER TABLE `events_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_categories`
--
ALTER TABLE `event_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_categories_tr`
--
ALTER TABLE `event_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `galleries_tr`
--
ALTER TABLE `galleries_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `gallery_categories`
--
ALTER TABLE `gallery_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallery_types`
--
ALTER TABLE `gallery_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `images_tr`
--
ALTER TABLE `images_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `metadata`
--
ALTER TABLE `metadata`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `metadata_tr`
--
ALTER TABLE `metadata_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `projects_tr`
--
ALTER TABLE `projects_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `project_categories`
--
ALTER TABLE `project_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `recovery`
--
ALTER TABLE `recovery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_languages`
--
ALTER TABLE `site_languages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `biography`
--
ALTER TABLE `biography`
ADD CONSTRAINT `FK_E3B3665C12469DE2` FOREIGN KEY (`category_id`) REFERENCES `bio_categories` (`id`);

--
-- Megkötések a táblához `bio_categories_tr`
--
ALTER TABLE `bio_categories_tr`
ADD CONSTRAINT `FK_4BFBC74012469DE2` FOREIGN KEY (`category_id`) REFERENCES `bio_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_4BFBC74082F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `bio_tr`
--
ALTER TABLE `bio_tr`
ADD CONSTRAINT `FK_912138F482F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_912138F49A99E1B9` FOREIGN KEY (`bio_id`) REFERENCES `biography` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `events`
--
ALTER TABLE `events`
ADD CONSTRAINT `FK_5387574A12469DE2` FOREIGN KEY (`category_id`) REFERENCES `event_categories` (`id`),
ADD CONSTRAINT `FK_5387574A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `events_tr`
--
ALTER TABLE `events_tr`
ADD CONSTRAINT `FK_96D2C928166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_96D2C92882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `event_categories_tr`
--
ALTER TABLE `event_categories_tr`
ADD CONSTRAINT `FK_8DF37D5612469DE2` FOREIGN KEY (`category_id`) REFERENCES `event_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_8DF37D5682F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `galleries`
--
ALTER TABLE `galleries`
ADD CONSTRAINT `FK_F70E6EB712469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`),
ADD CONSTRAINT `FK_F70E6EB7C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `gallery_types` (`id`);

--
-- Megkötések a táblához `galleries_tr`
--
ALTER TABLE `galleries_tr`
ADD CONSTRAINT `FK_B12E9F314E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_B12E9F3182F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `gallery_categories_tr`
--
ALTER TABLE `gallery_categories_tr`
ADD CONSTRAINT `FK_93CCF8812469DE2` FOREIGN KEY (`category_id`) REFERENCES `gallery_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_93CCF8882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images`
--
ALTER TABLE `images`
ADD CONSTRAINT `FK_E01FBE6A4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `images_tr`
--
ALTER TABLE `images_tr`
ADD CONSTRAINT `FK_DFF7DCA83DA5256D` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_DFF7DCA882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `metadata_tr`
--
ALTER TABLE `metadata_tr`
ADD CONSTRAINT `FK_D726301439FCA6F9` FOREIGN KEY (`meta_id`) REFERENCES `metadata` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_D726301482F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects`
--
ALTER TABLE `projects`
ADD CONSTRAINT `FK_5C93B3A412469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`),
ADD CONSTRAINT `FK_5C93B3A44E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `projects_tr`
--
ALTER TABLE `projects_tr`
ADD CONSTRAINT `FK_1566D618166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_1566D61882F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `project_categories_tr`
--
ALTER TABLE `project_categories_tr`
ADD CONSTRAINT `FK_7D3B5AFC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `project_categories` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_7D3B5AFC82F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `site_languages` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `site_languages`
--
ALTER TABLE `site_languages`
ADD CONSTRAINT `FK_C6DB250CB213FA4` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`);

--
-- Megkötések a táblához `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `FK_1483A5E9FE54D947` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
